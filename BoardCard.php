<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\CardIssueTypes;
use Auth;
use App\BoardCardFiles;
use App\Traits\slack; // <-- you'll need this line...


class BoardCard extends Model
{
	use slack; // <-- ...and also this line.
	
	public function traits(){
	 return $this->slack();
 }
 
 
    protected $table = "board_card";

    protected $fillable = [
        'project_id','working_hours','user_id', 'list_id', 'card_title','creator_id','parent_card_id','card_issue_type_id',
    ];

    public function createCard($input, $user_id)
    {
		
		// $this->send_slack_notification('D5PCJ4754','New Card is Created here');
		if( $input->get('parent_card_id') !== null) { $parent_card_id = $input->get('parent_card_id'); } else { $parent_card_id = '';}
    	$user = $this->create([
            'project_id' => $input->get('project_id'),
            'user_id' => '',
			'creator_id' => \Auth::id(),
            'list_id' => $input->get('list_id'),
			'parent_card_id' => $parent_card_id,
            'card_title' => $input->get('card-title'),  
			
        ]);
		@$user["project_title"] = @\App\Project::where("id",$input->get('project_id'))->first(['project_title'])->project_title;
		return @$user;
		
	
 
          

 

 








    }

    public function updateCardListid($input)
    {
        
		
		 $list_name = \App\BoardList::where("id",$input->get('listId'))->first(["list_name"])->list_name;
		
				$r = \App\BoardCard::where("id",$input->get("cardId"))->first(["user_id"])->user_id;
				$creator_id = \App\BoardCard::where("id",$input->get("cardId"))->first(["creator_id"])->creator_id;
		$users_array = explode(",",$r);
	 
	 
	  
  $creator_slack_id = \App\User::where("id",$creator_id)->first(["slack_id"])->slack_id;
		$creator_first_name = \App\User::where("id",Auth::id())->first(["first_name"])->first_name;
		$creator_last_name = \App\User::where("id",Auth::id())->first(["first_name"])->last_name;
			$assigner_first_name = \App\User::where("id",Auth::id())->first(["first_name"])->first_name;
		@$project_id = @\App\BoardCard::where("id",$input->get("cardId"))->first(["project_id"])->project_id;
		@$project_title = @\App\Project::where("id",$project_id)->first(["project_title"])->project_title;
		$card_title =  \App\BoardCard::where("id",$input->get("cardId"))->first(["card_title"])->card_title;
		
          for($t=0;$t<sizeof($users_array);$t++)
           {
			  //$count = \App\BoardCard::where("id",$input->get("cardId"))->whereRaw("FIND_IN_SET(".Auth::id().",board_card.user_id)")->get()->count();
			   
		 $message = '*'.@$project_title."* : *".$card_title."* _is moved to_ *".$list_name."* by ".$creator_first_name;
			  
			  
			//  http://192.168.0.116/workspace_web/public/project_details/realtimeboards/38
		
			  
			  
			 if(Auth::id() != $users_array[$t] ) 
			 {
			  
			  	
			  if($project_title =='') { $project_title ='General Task';}
			    

	        $slack_id = \App\User::where("id",$users_array[$t])->first(["slack_id"])->slack_id;
	        $first_name = \App\User::where("id",$users_array[$t])->first(["first_name"])->first_name;
			
						$data = '{"text": "*'.@$card_title.'* '.@$project_title.'\nTask Status has been  updated to *'.@$list_name.'* by _'.@$creator_first_name.' '.@$creator_last_name.'_", "channel": "'.@$slack_id.'"}';
            $this->send_slack_notification($slack_id , $data );
			 }
			
           }
		   
		 
			if($project_title =='') { $project_title ='General Task';}
		  
                    $data2 = '{"text": "*'.@$card_title.'* '.@$project_title.'\nTask Status has been updated to *'.@$list_name.'* by _'.@$creator_first_name.' '.@$creator_last_name.'_", "channel": "'.@$creator_slack_id.'"}';
                    $this->send_slack_notification($creator_slack_id , $data2);
		 
		 
			
			
			
			
		   return $this->where('id', $input->get('cardId'))->update(['list_id' => $input->get('listId')]);
		   
    }

    public function deleteCard($input)
    {
    	$card = $this->find($input->get("cardId"));
    	$this->find($input->get("cardId"))->delete();
    	return $card;
    }

    public function getCard($cardId)
    {
    	$card_detail = $this->findOrFail($cardId);
		if($card_detail["card_issue_type_id"] != '')
		{
		$card_detail["card_issue_type_image"] = \App\CardIssueTypes::where('id',$card_detail["card_issue_type_id"])->first(["image"])->image;
		}
		else
		{
			$card_detail["card_issue_type_image"] = '';
		}
		return $card_detail;
		
    }
	
	
	
	
	
	   public function card_files($cardId)
    {
		
	 
	 
	    $card_files = @\App\BoardCardFiles::where('card_id',$cardId)->get();
		
		for($y=0;$y<sizeof($card_files);$y++)
		{
			$card_files[$y]['size'] = '1273';
			$card_files[$y]['type'] ='image/jpeg';
			$card_files[$y]['name'] ='1491976695951.jpg';
			$card_files[$y]['file'] = 'http://192.168.0.106/workspace_web/public/upload/uploads/1491976695951.jpg';
		}
		
	
	    return $card_files;
	}
	
	
	
	
	
	
	
	

    public function updateCard($input)
    {
		$r= @$input->get("user_id");
		$users_array = explode(",",$r);
	if(@$input->get("user_id") == '')
	{
       $users_array = array();
	}
		$creator_id = \App\BoardCard::where("id",$input->get("cardId"))->first(["creator_id"])->creator_id;
		$assigner_first_name = \App\User::where("id",Auth::id())->first(["first_name"])->first_name;
		
		@$project_id = @\App\BoardCard::where("id",$input->get("cardId"))->first(["project_id"])->project_id;
		@$project_title = @\App\Project::where("id",$project_id)->first(["project_title"])->project_title;
       
 	    $creator_slack_id = \App\User::where("id",$creator_id)->first(["slack_id"])->slack_id;
		
		//array_push($users_array,$creator_id);
		if(sizeof($users_array) > 0)
		{
          for($t=0;$t<sizeof($users_array);$t++)
           {
			 $count = \App\BoardCard::where("id",$input->get("cardId"))->whereRaw("FIND_IN_SET(".$users_array[$t].",board_card.user_id)")->get()->count();
			   
			 if($count > 0) {   $pretext = 'Knock Knock, Task Updated:alarm_clock:';} else { $pretext = 'Knock Knock, new Task :alarm_clock:';}
			 
			 if(Auth::id() != $users_array[$t] && $creator_id !=  $users_array[$t] ) 
			 {
	        $slack_id = \App\User::where("id",$users_array[$t])->first(["slack_id"])->slack_id;
	        $first_name = \App\User::where("id",$users_array[$t])->first(["first_name"])->first_name;
			
			$due_date = '<!date^'.strtotime($input->get("cardDueDate")).'^Due by {date_short_pretty}| >' ;
			
			if($input->get("cardDueDate") == '')
			{
				$due_date = '';
			}
			
			$project_link = url('/').'/project_details/realtimeboards/'.$project_id;
			if($project_title =='') { $project_title ='General Task'; $project_link = '';}
			
			$description = $input->get("cardDescription");
			if($input->get("cardDescription") =='Empty') { $description ='';}
			
					$data = '{"text": "", "channel": "'.$slack_id.'", "attachments": [ { "color": "'.$input->get("cardColor").'","pretext": "'.$pretext.'","author_name": "'.$assigner_first_name.' | '.$project_title.'","author_link": "'.$project_link.'","title": "'.$input->get("cardName").'","text": "'.$description.'",
          "footer": "'.$due_date.'"}]}';
		  
		  
            $this->send_slack_notification($slack_id , $data);
			  
			
			
			
			
			 }
			 
		
           }
	}
		   
		    if(isset($count) && $count > 0)
			{
		   $data2 = '{"text": "", "channel": "'.$creator_slack_id.'", "attachments": [ { "color": "'.$input->get("cardColor").'","pretext": "'.$pretext.'","author_name": "'.$assigner_first_name.' | '.$project_title.'","author_link": "'.$project_link.'","title": "'.$input->get("cardName").'","text": "'.$description.'",
          "footer": "'.$due_date.'"}]}';
		  
 	 $this->send_slack_notification($creator_slack_id , $data2);
			}


    	$this->where('id', $input->get("cardId"))->update([
            "card_title" => $input->get("cardName"),
            "card_description" => ($input->get("cardDescription") != "Empty") ? $input->get("cardDescription") : '',
            "card_color" => $input->get("cardColor"),
			"working_hours" => $input->get("working_hours"),
			"user_id" => @$input->get("user_id"),//explode(" ",$str));
            "due_date" => date("Y-m-d H:i:s", strtotime($input->get("cardDueDate"))),
			'card_issue_type_id' => $input->get('card_issue_type_id'),
        ]);
		
		
		$files = $input->get("fileValue");
		$files_array = explode(",",$files );
		
		for($f=0;$f<sizeof($files_array);$f++)
		{
		  $board_card_files= new BoardCardFiles;
          $board_card_files->file_url=$files_array[$f];
		  $board_card_files->card_id= $input->get("cardId");
		  $board_card_files->type= 'image';
          $board_card_files->save();
		}
		
	
        return true;
    }

    public function getBoardCards($project_id)
    {
		
		if($project_id != '')
		{
		

	         $auth_role = Auth::user()->roles()->get()[0]->name;
		 if($auth_role == 'Admin')
		{
			        return $this->select([
                'board_card.*',
                DB::raw("COUNT(comment.id) as totalComments"),
            ])
            ->leftJoin('comment', 'board_card.id', '=', 'comment.card_id')->groupBy('board_card.id')
            ->where("project_id",$project_id)->get();
		}
		
		else
		{
			        return $this->select([
                'board_card.*',
                DB::raw("COUNT(comment.id) as totalComments"),
            ])
            ->leftJoin('comment', 'board_card.id', '=', 'comment.card_id')->whereRaw("FIND_IN_SET(".Auth::id().",board_card.user_id)")->orWhere("creator_id",Auth::id())
            ->groupBy('board_card.id')
            ->where("project_id",$project_id)->get();
			
		}
			

		}
		else
		{
			
			       $auth_role = Auth::user()->roles()->get()[0]->name;
		 if($auth_role == 'Admin')
		{
		   return $this->select([
                'board_card.*',
                DB::raw("COUNT(comment.id) as totalComments"),
            ])
            ->leftJoin('comment', 'board_card.id', '=', 'comment.card_id')->groupBy('board_card.id')->get();
		}
		else
		{
			  return $this->select([
                'board_card.*',
                DB::raw("COUNT(comment.id) as totalComments"),
            ])
            ->leftJoin('comment', 'board_card.id', '=', 'comment.card_id')->whereRaw("FIND_IN_SET(".Auth::id().",board_card.user_id)")->orWhere("creator_id",Auth::id())
            ->groupBy('board_card.id')->get();
		}
			
		}
    }
	
	
	
	
	    public function getBoardCardsFiltered($project_id , $filter)
    {
		 
			
            $query =$this->select([
                'board_card.*',
                DB::raw("COUNT(comment.id) as totalComments"),
            ])
            ->leftJoin('comment', 'board_card.id', '=', 'comment.card_id')
            ->groupBy('board_card.id');
 
 
             if($filter=='' && $project_id =='') { $query2 = $query->get();}
			 
			 if($filter != '' && $project_id =='') { 
			                                               if($filter == 'all')
												   {
			                                        $query2 = $query->get();
												   }
												   else if($filter == 'due_date')
												   {
													 
													 $query2 = $query->whereDate('due_date', DB::raw('CURDATE()'))->get(); 
												   }
												   else if($filter == 'added_yesterday')
												   {
													$query2 = $query->whereDate('due_date', DB::raw('CURDATE()-1'))->get(); 
												   }
												   else if($filter == 'delayed')
												   {
													 $query2 = $query->whereDate('due_date','<', DB::raw('CURDATE()'))->get(); 
												   }
												   else
												   {
													  $query2 = $query->get();
												   }
			                                       }
			 
			 if($filter=='' && $project_id !='') { 
			                                          $query2 = $query->where("project_id",$project_id)->get();
											     }
			 
			 if($filter != '' && $project_id !='') { 
			                                      if($filter == 'all')
												   {
			                                        $query2 = $query->where('project_id',$project_id)->get();
												   }
												   else if($filter == 'due_date')
												   {
													  $query2 = $query->where('project_id',$project_id)->whereDate('due_date', DB::raw('CURDATE()'))->get(); 
												   }
												   else if($filter == 'added_yesterday')
												   {
													 $query2 = $query->where('project_id',$project_id)->whereDate('due_date', DB::raw('CURDATE()-1'))->get(); 
												   }
												   else if($filter == 'delayed')
												   {
													 $query2 = $query->where('project_id',$project_id)->whereDate('due_date','<',DB::raw('CURDATE()'))->get();  
												   }
												   else
												   {
													  $query2 = $query->where('project_id',$project_id)->get();
												   }
			 
			                                          }
			 
			
			return $query2;
			
			
			//sort filter
			
			/**
			if($sortby =='')
			{
				
			}
			else if()
			{
				
			}
			else
			{
				
			}
			**/
			
			
			
			
 
    }

    public function cardTotalTask()
    {
        return $this->select([
            'board_card.*',
            DB::raw("COUNT(card_task.id) as totalTasks"),
        ])
        ->leftJoin('card_task', 'board_card.id', '=', 'card_task.card_id')
        ->groupBy('board_card.id')
        ->get();
    }
}
