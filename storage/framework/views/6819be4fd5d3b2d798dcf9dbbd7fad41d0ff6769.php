 
<?php $__env->startSection('title', 'Projects'); ?> 
<?php $__env->startSection('content'); ?> 
<?php $__env->startSection('header'); ?> 
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
<?php echo $__env->yieldSection(); ?>
    <div id="wrapper">
        <div id="layout-static">
            <!---------- Static Sidebar Starts--------->
            <?php $__env->startSection('sidebar'); ?> <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> <?php echo $__env->yieldSection(); ?>
            <!---------- Static Sidebar Ends----------->
            <div class="static-content-wrapper">

                <section id="main-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
								    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                                <div class="tab-content">

                                    <div class="row">
                                        <div class="col-sm-7 header-left">
                                            <h3 class="text-left">Projects List </h3>
                                        </div>
                                        <div class="col-sm-5 header-right">
                                            <div class="dropdown">
                                               
                                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_project')): ?>
                                                <button type="button" class="btn btn-trans" data-toggle="modal" data-target="#myModal">Add New</button>
												
                                                <?php endif; ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                          <?php $color_arr = array("0"=>"#ffe39a", "1"=>"#b6e0cd", "2"=>"#f3c6c3" ,"3"=>'#d8d8d8' , "4"=>'#f8ca9e' , "5"=>'#dd6668'); ?>
											<?php $status_name = array("0"=>"Work Pending", "1"=>"Team Working", "2"=>"Waiting For Client" ,"3"=>'Completed' , "4"=>'Cancelled' , "5"=>'Payment Pending'); ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel">

                                                <div class="row">

                                                    <div class="col-sm-7 ">
													<ul class="nav nav-tabs realTime">
								<li  class="<?php if(Request::path()=='projects_list/projects_by_status/all'  ) { echo 'active';} ?>" ><a href="<?php echo e(URL::to('projects_list/projects_by_status/all')); ?>">All</a></li>					
                              <li  class="<?php if(Request::path()=='projects_list/projects_by_status/active' || Request::path()=='projects') { echo 'active';} ?>" ><a href="<?php echo e(URL::to('projects_list/projects_by_status/active')); ?>">Active</a></li>
							<li  class="<?php if(Request::path()=='projects_list/projects_by_status/3') { echo 'active';} ?>" ><a href="<?php echo e(URL::to('projects_list/projects_by_status/3')); ?>"><?php echo e($status_name[3]); ?></a></li><li  class="<?php if(Request::path()=='projects_list/projects_by_status/4') { echo 'active';} ?>" ><a href="<?php echo e(URL::to('projects_list/projects_by_status/4')); ?>"><?php echo e($status_name[4]); ?></a></li>						
														
														
														
														
														
 </ul>
                                                     </div>

                                                    <div class="col-sm-5  actions-right">
													
				  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('export_project')): ?>
                                                        <a href="<?php echo e(URL::to('downloadExcelProject/xls')); ?>"><i class="fa fa-share-square-o" title="Export Excel"  ></i></a><?php endif; ?>									
													

													
													
													
 
  
    <?php
         echo Form::open(array('url' => '/projects_by_employee','files'=>'true','method'=>'POST'));
        
	?>
	
	<label for="">Project By Employee : &nbsp;</label>					
<select   class="form-control text-right"   id="user_id" name="user_id" style="width:40%;float:right" onchange="this.form.submit()"> 
    <option value="">Select Employee</option>
	<?php if(isset($employees_array) && sizeof($employees_array)>0) {?>
      <?php $__currentLoopData = $employees_array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	  
	  	<?php   $count1 = \App\ProjectEmployees::where('project_id',@$id)->where('employee_id',@$ct->id)->get();
		if(isset($selected_employee_id) && $selected_employee_id == $ct->id ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
<option value="<?php echo e($ct->id); ?>" <?php echo $s1;?> ><?php echo e($ct->first_name." ".$ct->last_name); ?></option>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<?php } ?>
</select> 
 
   <?php
      
         echo Form::close();
      ?>
	  
	  
	  
	  
	  										 


 





                                                       
                                                    
                                                    </div>
                                                </div>

                                             

                                                <div class="row">
												
											 
												 
                                                    <div class="col-md-5">
                                                        <h3 class="modal-title"><?php echo e($result->total()); ?> <?php echo e(str_plural('Projects', $result->count())); ?> </h3>
                                                    </div>
                                                    <div class="col-md-7 page-action text-right">

                                                    </div>
                                                </div>
                  
											
					 


                                                <div class="result-set" id="result-set">
                                                    <table class="table table-bordered table-striped table-hover" id="printTable">
                                                        <thead>
                                                            <tr>
                                                                <th>Sr</th>
                                                                <th>Project Title</th>
                                                                <th>Assigned Employees</th>
                                                                <th>Clients</th>
														        <th>Status</th>
                                                          
                                                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_project', 'delete_project')): ?>
                                                                <th class="text-center">Actions</th>
                                                                <?php endif; ?>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $x=0;?>
                                                                <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php $x++;?>
                                                                    <tr>
                                                                        <td><?php echo e($x); ?></td>
                                                                        <td><a href="<?php echo e(URL::to('project_details/realtimeboards/'.$item->id)); ?>" ><b> <?php echo e($item->project_title); ?></b></a></td>
                                                                     <td>
																	 <?php 
																	 
																 
																	   $project_employees = \App\ProjectEmployees::where("project_id",$item->id)->get();
																	   
																 
																	   $arr = array();
																	   foreach($project_employees as $e)
																	   {
																		  $user_id = $e->employee_id;
																		     @$name = @\App\User::where('id',$user_id)->first(["first_name"])->first_name;
																			 
																			 if($name != '' && $name !=null)
																			 {
																	 	       $arr[] = '<a href="#">'.$name.'</a> ';
																			 }
																			 
																	   }
																	   echo implode(",",$arr);
																	 ?>
																	 
																	 </td>
																	 
																	        <td>
																	 <?php 
																	 
																 
																	   $project_employees2 = \App\ProjectClients::where("project_id",$item->id)->get();
																	  $arr = array();
																	   foreach($project_employees2 as $e2)
																	   {
																		  $user_id = $e2->client_id;
																		     @$name = @\App\User::where('id',$user_id)->first(["first_name"])->first_name;
																			 
																			 if($name != '' && $name !=null)
																			 {
																	 	       $arr[] = '<a href="#">'.$name.'</a> ';
																			 }
																			 
																	   }
																	   echo implode(",",$arr);
																	 ?>
																	 
																	 </td>
																	 
																	 
																		<td style="background-color:<?php echo e($color_arr[$item->status]); ?>;color:#000"><?php echo e($status_name[$item->status]); ?></td>
                                                                      
                                                                        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_project')): ?>
                                                                        <td class="text-center">
                                                                            <?php echo $__env->make('shared._actions', [ 'entity' => 'projects', 'id' => $item->id ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                                        </td>
                                                                        <?php endif; ?>
                                                                    </tr>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </tbody>
                                                    </table>

                                                    <div class="text-right">
                                                        <?php echo e($result->links()); ?>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <!------------- Add Model ----->
    <div id="myModal" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Add New <span>Project</span></h4>
                </div>
                <div class="modal-body  ">
                    <div class="row">
                           <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                            <?php echo Form::open(['route' => ['projects.store'] ]); ?> <?php echo $__env->make('projects._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            <!-- Submit Form Button -->
                            <?php echo Form::submit('Create', ['class' => 'btn btn-create']); ?> <?php echo Form::close(); ?>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!---Add Model Ends--->

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>