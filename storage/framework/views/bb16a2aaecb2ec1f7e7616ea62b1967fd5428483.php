        <header id="topnav" class="navbar navbar-dark navbar-fixed-top clearfix" role="banner">

	 
	<span id="trigger-sidebar" class="toolbar-trigger" >
	                  	   <div class="navbar-header">
      <a class="navbar-brand" href="#">
	<img src="<?php echo e(env('APP_LOGO_WHITE')); ?>" class="" style="max-width:120px;"></a>
    </div>  
						 <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
					 
	                </span>
                                        
                    <ul class="nav navbar-nav toolbar pull-right">
                         <?php if(Auth::guest()): ?>
                        <li><a href="<?php echo e(url('/admin/login')); ?>">Login</a></li>
                        <li><a href="<?php echo e(url('/admin/register')); ?>">Register</a></li>
                    <?php else: ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle name" data-toggle="dropdown" role="button" aria-expanded="false">
                                <span class="name"><?php echo e(Auth::user()->first_name); ?> </span><span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                
                   
								  <li>
                                    <a href="<?php echo e(url('/logout')); ?>"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST" style="display: none;">
                                        <?php echo e(csrf_field()); ?>

                                    </form>
                                </li>
								
								
                            </ul>
                        </li>
                    <?php endif; ?>
					 
					 <!-- <li><span><i class="fa fa-plus-circle"></i></span></li>--->
					  
					  <li class="dropdown">
					         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               <span><i class="fa fa-cog"></i></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                               	   <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_contact_types')): ?><li><a href="<?php echo e(URL::to('contact_types')); ?>"><i class="fa fa-phone"></i><span> Contact Types</span></a></li><?php endif; ?>
					           	   <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_credentials_types')): ?><li><a href="<?php echo e(URL::to('credentials_types')); ?>"><i class="fa fa-key"></i><span> Credentials Types</span></a></li><?php endif; ?>
                               	   <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_payment_modes')): ?><li><a href="<?php echo e(URL::to('payment_modes')); ?>"><i class="fa fa-key"></i><span> Payment Modes</span></a></li><?php endif; ?>
                               	   <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_payment_fee_types')): ?><li><a href="<?php echo e(URL::to('payment_fee_types')); ?>"><i class="fa fa-key"></i><span> Payment Fee Types</span></a></li><?php endif; ?>
                               	   <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_designations')): ?><li><a href="<?php echo e(URL::to('designations')); ?>"><i class="fa fa-key"></i><span> Designations</span></a></li><?php endif; ?>	

 <li><a href="<?php echo e(URL::to('urls_types')); ?>"><i class="fa fa-key"></i><span> Url Types</span></a></li> 	

  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_payment_fee_types')): ?><li><a href="<?php echo e(URL::to('card_issue_types')); ?>"><i class="fa fa-key"></i><span> Card Issue Types</span></a></li><?php endif; ?>
                            </ul>
                        </li> 
					  
					<!--- <li><span><i class="fa fa-question-circle"></i></span></li>-->
                    </ul>   
   </header>
  