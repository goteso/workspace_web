<?php $__env->startSection('title', 'Roles & Permissions'); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
 
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!-- Modal -->
	 
      <div id="myModal" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
         <div class="modal-dialog" role="document">
            <?php echo Form::open(['method' => 'post']); ?>

            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title text-center" id="roleModalLabel">Role</h3>
               </div>
               <div class="modal-body">
                  <!-- name Form Input -->
				  <br><br>
				  <div class="row">
				  <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
                  <div class="form-group <?php if($errors->has('name')): ?> has-error <?php endif; ?>"  >
                     <?php echo Form::label('name', 'Name'); ?>

                     <?php echo Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name']); ?>

                     <?php if($errors->has('name')): ?> 
                     <p class="help-block"><?php echo e($errors->first('name')); ?></p>
                     <?php endif; ?>
                  </div>
				   <!-- Submit Form Button -->
                  <?php echo Form::submit('Submit', ['class' => 'btn btn-create']); ?>

				  </div>
				  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                 
               </div>
               <?php echo Form::close(); ?>

            </div>
         </div>
      </div>
	  
	  
	  
	  
	   

                   <div class="static-content-wrapper">
						
						<section id="main-header">	
						
 <div class="container-fluid">
 
      	    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       
	  <?php $x1 = 0; $x2=0;?>
      <ul class="nav nav-tabs">
         <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         <?php $x1++;  if($x1=='1') { $class1='active';} else { $class1='';}?>
         <li class="<?php echo e($class1); ?>" ><a data-toggle="tab" href="#<?php echo e($role1['id']); ?>"><?php echo e($role1->name); ?> </a></li>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </ul>
	  
	  
	  <div class="row roles-header" > 
		 <div class="col-sm-5 header-left">
       <h3 class="text-left">Roles</h3>
	  </div>
         <div class="col-md-7 page-action text-right header-right">
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_roles')): ?>
            <button type="button" class="btn btn-trans"  data-toggle="modal" data-target="#myModal">Add New</button>
            <?php endif; ?>
			
			 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_roles')): ?>
            <button type="button" class="btn btn-trans"  data-toggle="modal" data-target="#view">Manage</button>
            <?php endif; ?>
         </div>
      </div>
	  
      <div class="tab-content">
         <?php $__empty_1 = true; $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
         <?php $x2++;  if($x2=='1') { $class2='in active';} else { $class2='';}?>
         <div id="<?php echo e($role['id']); ?>" class="tab-pane fade <?php echo e($class2); ?>">  
            <?php echo Form::model($role, ['method' => 'PUT', 'route' => ['roles.update',  $role->id ], 'class' => 'm-b']); ?>

            <?php echo $__env->make('shared._permissions', [
            'title' => $role->name .' Permissions',
            'model' => $role ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_roles')): ?>
            <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

            <?php endif; ?>
            <?php echo Form::close(); ?>

         </div>
		 
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
         <p>No Roles defined, please run <code>php artisan db:seed</code> to seed some dummy data.</p>
         <?php endif; ?>
      </div>
   </div>
   </section>
</div>

</div>

</div>





<!----- Manage Modal Starts----->
<!------------- View Modal Starts ----->
<div id="view" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog text-center"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Manage Roles</h4>
         </div>
         <div class="modal-body "  >
         
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  <table class="table table-bordered table-striped table-hover" id="printTable">
                     <thead>
                        <tr>
                           <th>Sr</th>
                           <th>Role</th>
						   <th>Guard</th>
                           <th>Created At</th>
                           <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_roles', 'delete_roles')): ?>
                           <th class="text-center">Actions</th>
                           <?php endif; ?>
                        </tr>
                     </thead>
                     <tbody>
					     <?php $x=0;?>
                        <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php $x++;?>
                        <tr>
                           <td><?php echo e($x); ?></td>
                           <td><?php echo e($item['name']); ?></td>
						   <td><?php echo e($item['guard_name']); ?></td>
                           <td><?php echo e($item['created_at']); ?></td>
                           <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_users')): ?>
                           <td class="text-center">
						   <?php
                                       $model_contains_role = \DB::table('model_has_roles')->where('role_id', '=', $item['id'])->get()->count();
									   if($model_contains_role < 1)
									   {
                            ?>
                              <?php echo $__env->make('role._actions_roles', [
                              'entity' => 'roles',
                              'id' => $item['id']
                              ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
									   <?php } else { echo 'Users Exists';}?>
                           </td>
                           <?php endif; ?>
                        </tr>
                        <!-------inner model starts-------->		
                        <div id="edit<?php echo e($item['id']); ?>" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                           <div class="modal-dialog"  >
                              <!-- Modal content-->
                              <div class="modal-content"  >
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-center">Update Role</h4>
                                 </div>
                                 <div class="modal-body "  >
                                   
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                          <?php echo Form::model($item['id'], ['method' => 'PUT', 'route' => ['roles.update',  $item['id'] ] ]); ?>

                                          <?php echo $__env->make('role._form_edit_roles', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                          <!-- Submit Form Button -->
                                          <?php echo Form::submit('Update', ['class' => 'btn btn-primary']); ?>

                                          <?php echo Form::close(); ?>

                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <script>
                           function close_inner<?php echo $item['id']; ?>()
                           {
                           	$('#edit<?php echo $item['id']; ?>').modal('hide');
                           }
                           
                        </script>
                        <!-------inner model ends-------->
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>


<!----manage modal ends here--->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>