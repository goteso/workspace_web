<!-- First Name Form Input -->
 

<div class="form-group <?php if($errors->has('title')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('title', 'Board Title'); ?>

    <?php echo Form::text('title', @$title, ['class' => 'form-control ', 'placeholder' => 'Board Title' ] ); ?>

    <?php if($errors->has('title')): ?> <p class="help-block"><?php echo e($errors->first('title')); ?></p> <?php endif; ?>
</div>


  
    <?php echo Form::hidden('project_id', @$id, [] ); ?>

 


<div class="form-group <?php if($errors->has('realtime_board_id')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('realtime_board_id', 'RealTimeBoard Id'); ?>

    <?php echo Form::text('realtime_board_id', @$realtime_board_id, ['class' => 'form-control', 'placeholder' => 'RealTimeBoard Id' ] ); ?>

    <?php if($errors->has('realtime_board_id')): ?> <p class="help-block"><?php echo e($errors->first('realtime_board_id')); ?></p> <?php endif; ?>
</div>
