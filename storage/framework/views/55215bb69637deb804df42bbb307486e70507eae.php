<?php
   use App\Role;
   ?>

<?php $__env->startSection('title', 'Urls List' ); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				  <div class="text-right">
                     <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_urls')): ?>
                     <button type="button" class="btn btn-success text-right"  data-toggle="modal" data-target="#add">Add New</button>
                     <?php endif; ?>
					 </div>
                     <div class="tab-content">
                        <table class="table table-bordered table-striped table-hover" id="printTable">
                           <thead>
                              <tr>
                                 <th>Sr</th>
                                 <th>Title</th>
								 <th>Type</th>
								 <th>Full Url</th>
								 <th>Short Url</th>
								 <th>Analytics</th>
                                 <th>Created At</th>
                                 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_urls', 'delete_urls')): ?>
                                 <th class="text-center">Actions</th>
                                 <?php endif; ?>
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;?>
                              <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php $x++;?>
							  <?php @$url_type_title = @\App\UrlsTypes::where('id',$item['url_type_id'])->first(["title"])->title;?>
                              <tr>
                                 <td><?php echo e($x); ?></td>
                                 <td><?php echo e($item['title']); ?></td>
								 <td><?php echo e(@$url_type_title); ?></td>
								 <td><?php echo e($item['full_url']); ?></td>
								 <td><?php echo e($item['short_url']); ?></td>
								 <td><a href="<?php echo e(URL::to('urls/analytics/'.@$item['id'])); ?>">Analytics</a></td>
                                 <td><?php echo e($item['created_at']); ?></td>
                                 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_urls')): ?>
                                 <td class="text-center">
                                    <?php echo $__env->make($model_name.'._actions_with_modals', [
                                    'entity' => $model_name,
                                    'id' => $item['id']
                                    ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                 </td>
							
                                 <?php endif; ?>
                              </tr>
                              <div id="edit<?php echo e($item['id']); ?>" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                                 <div class="modal-dialog"  >
                                    <!-- Modal content-->
                                    <div class="modal-content"  >
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Update <?php echo e($display_modal_name); ?></h4>
                                       </div>
                                       <div class="modal-body "  >
                                          <div class="row">
                                             <div class="col-lg-10">
                                                <?php echo Form::model($item['id'], ['method' => 'PUT', 'route' => [$model_name.'.update',  $item['id'] ] ]); ?>

                                                <?php echo $__env->make($model_name.'._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                <!-- Submit Form Button -->
                                                <?php echo Form::submit('Update', ['class' => 'btn btn-primary']); ?>

                                                <?php echo Form::close(); ?>

                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <script>
                                 function close_inner<?php echo $item['id']; ?>()
                                 {
                                 	$('#edit<?php echo $item['id']; ?>').modal('hide');
                                 }
                                 
                              </script>
							  	 <?php $item=null;?>
                              <!-------inner model ends-------->
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </tbody>
                        </table>
						  <div class="text-right">
                                 <?php echo e($result->links()); ?>

                           </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<?php
   $roles = Role::pluck('name', 'id');
   ?>
<!--Add New Model Starts--> 
<style>

</style>
<!-- Modal -->
<!------------- Add New User ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog "  >
      <!-- Modal content-->
      <div class="modal-content "  >
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New <?php echo e($display_modal_name); ?></h4>
         </div>
         <div class="modal-body "  >
          
            <div class="row">
               <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  <?php echo Form::open(['route' => [ $model_name.'.store'] ]); ?>

                  <?php echo $__env->make($model_name.'._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  <!-- Submit Form Button -->
                  <?php echo Form::submit('Create', ['class' => 'btn btn-primary']); ?>

                  <?php echo Form::close(); ?>

               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>