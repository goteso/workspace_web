
<div class="form-group <?php if($errors->has('first_name')): ?> has-error <?php endif; ?>">
			<?php if(@$user->photo == '')
			{
				echo '<h4 class="text-center" style="height:160px;width:150px;border:1px solid #ccc;border-radius:5px;padding-top:50px;">Upload Profile pic</h4>';
			}
			else
			{
?>
			<img src="<?php echo url('/').'/'.@$user->photo;?>" height="200px" >
			<?php }
			?>
			
				
</div>

   <a href="<?php echo e(url('/users/show_edit_user_profile/'.@$user->id )); ?>"
                                         >
                                 Update Profile Picture
                                    </a>
<!-- First Name Form Input -->
<div class="form-group <?php if($errors->has('first_name')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('first_name', 'First Name'); ?>

    <?php echo Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']); ?>

    <?php if($errors->has('first_name')): ?> <p class="help-block"><?php echo e($errors->first('first_name')); ?></p> <?php endif; ?>
</div>

<!-- Last Name Form Input -->
<div class="form-group <?php if($errors->has('last_name')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('last_name', 'Last Name'); ?>

    <?php echo Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']); ?>

    <?php if($errors->has('last_name')): ?> <p class="help-block"><?php echo e($errors->first('last_name')); ?></p> <?php endif; ?>
</div>
<!-- email Form Input -->
<div class="form-group <?php if($errors->has('slack_id')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('slack_id', 'Slack Id'); ?>

    <?php echo Form::text('slack_id', null, ['class' => 'form-control', 'placeholder' => 'Slack Id']); ?>

    <?php if($errors->has('slack_id')): ?> <p class="help-block"><?php echo e($errors->first('slack_id')); ?></p> <?php endif; ?>
</div>
<!-- email Form Input -->
<div class="form-group <?php if($errors->has('email')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('email', 'Email'); ?>

    <?php echo Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']); ?>

    <?php if($errors->has('email')): ?> <p class="help-block"><?php echo e($errors->first('email')); ?></p> <?php endif; ?>
</div>





<!-- password Form Input -->
<div class="form-group <?php if($errors->has('password')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('password', 'Password'); ?>

    <?php echo Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']); ?>

    <?php if($errors->has('password')): ?> <p class="help-block"><?php echo e($errors->first('password')); ?></p> <?php endif; ?>
</div>

<!-- Roles Form Input -->
<div class="form-group <?php if($errors->has('roles')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('roles[]', 'Roles'); ?>

    <?php echo Form::select('roles[]', $roles, isset($user) ? $user->roles->pluck('id')->toArray() : null,  ['class' => 'form-control', 'multiple']); ?>

    <?php if($errors->has('roles')): ?> <p class="help-block"><?php echo e($errors->first('roles')); ?></p> <?php endif; ?>
</div>

 
					 
					 
<?php if(isset($designations_array) && sizeof($designations_array) > 0 )
{?>
<div class="form-group <?php if($errors->has('designations_array')): ?> has-error <?php endif; ?>">
<?php echo Form::label('designations_array', 'Designation'); ?><br>
   <select   class="form-control" id="example-getting-started" name="designations_array[]" multiple="multiple" style="width:100%;border:1px solid red">
      <?php $__currentLoopData = $designations_array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	    <?php   $count1 = \App\EmployeeDesignations::where('user_id',@$user['id'])->where('designation_id',@$ct->id)->get();
		        $count1 = intval($count1->count());
		 		if(isset($user['id']) && $count1 > 0 ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
         <option value="<?php echo e($ct->id); ?>" <?php echo $s1;?> ><?php echo e($ct->title); ?></option>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
</div>
<?php } ?>




<script type="text/javascript">
   $(document).ready(function() {
       $('#example-getting-started').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
 
 });
</script>



<!-- Permissions -->
<?php if(isset($user)): ?>

<?php endif; ?>
 