<div class="panel panel-default"  >
    <div class="panel-heading" role="tab" id="<?php echo e(isset($title) ? str_slug($title) :  'permissionHeading'); ?>">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#dd-<?php echo e(isset($title) ? str_slug($title) :  'permissionHeading'); ?>" aria-expanded="<?php echo e(isset($closed) ? $closed : 'true'); ?>" aria-controls="dd-<?php echo e(isset($title) ? str_slug($title) :  'permissionHeading'); ?>">
             <?php echo isset($user) ? '<span class="text-danger">(' . $user->getDirectPermissions()->count() . ')</span>' : ''; ?>

            </a>
        </h4>
    </div>
    <div id="dd-<?php echo e(isset($title) ? str_slug($title) :  'permissionHeading'); ?>" class="panel-collapse collapse <?php echo e(isset($closed) ? $closed : 'in'); ?>" role="tabpanel" aria-labelledby="dd-<?php echo e(isset($title) ? str_slug($title) :  'permissionHeading'); ?>">
        <div class="panel-body">
            <div class="row">
                <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $perm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				
			      <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 text-left">
			         <h4 style="color:#5CB75C"><?php echo e(title_case($perm[0]->category)); ?> </h4>
			      </div>
<div class="row">				  
				 <?php $__currentLoopData = $perm; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $perm_element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				         <?php
                        $per_found = null;

					 
                        if( isset($role) ) {
                            $per_found = $role->hasPermissionTo($perm_element->name);
                        }

                        if( isset($user)) {
                            $per_found = $user->hasDirectPermission($perm_element->name);
                        }   
                    ?>
				        <div class="col-md-3">
                        <div class="checkbox">
                            <label class="<?php echo e(str_contains($perm_element->name, 'delete') ? 'text-danger' : ''); ?>">
								<?php echo Form::checkbox("permissions[]", $perm_element->name, $per_found, isset($options) ? $options : []); ?> <?php echo e($perm_element->name); ?>

                            </label>
                        </div>
                    </div>
					
				  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				  </div>
				  <hr>
				  
				  
				 
            

             
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>