<?php
   use App\Role;
?>

<?php $__env->startSection('title', 'RealTimeBoard'); ?>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <?php $realtimeboard = $result[0]['project_real_time_board']; $x1=0;$x2=0;?>	  
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="row">
					 
                        <div class="col-sm-10">
					
					 
                          <?php echo $__env->make('projects._navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                        <div class="col-sm-2">
						<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_realtime_boards')): ?>
                           <div class="project-add-button">
                              <button type="button" class="btn btn-add " data-toggle="modal" data-target="#add">Add New</button>
                           </div>
						<?php endif; ?>
                        </div>
                     </div>
                     <div class="tab-content"  >
                        <ul class="nav nav-pills">
                           <?php $__currentLoopData = $realtimeboard; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php $x1++; if($x1==1) { $c1 = 'active';} else { $c1='';}
						    
							?>
							<?php if(sizeof($realtimeboard) > 1 )
							{
							?>
                           <li class="<?php echo e($c1); ?>" ><a  data-toggle="tab" href="#<?php echo e($data1['id']); ?>"  ><?php echo e($data1['title']); ?> </a></li>
							<?php } ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                        <div class="tab-content" id="myDiv" >
                           <?php $__currentLoopData = $realtimeboard; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php $x2++; if($x2==1) { $c2 = 'in active';} else { $c2='';}
						  
						   ?>
                           <div id="<?php echo e($data2['id']); ?>" class="tab-pane fade <?php echo e($c2); ?>">
						   
						   <button type='button' class="toggle-button" value='fullscreen'  ><i class="fa fa-arrow-left"></i></button>
                              <iframe src="https://realtimeboard.com/app/embed/<?php echo e($data2['realtime_board_id']); ?>" style="width:100%; height:800px; border:0;"></iframe>
                           </div>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------------- Add Modal Starts ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Add New <span>Board</span></h4>
         </div>
         <div class="modal-body "  >
            <br><br>
            <div class="row">
               <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  <?php echo Form::open(['route' => ['realtimeboards.store'] ]); ?>

                  <?php echo $__env->make('projects._form_realtime', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  <!-- Submit Form Button -->
                  <?php echo Form::submit('Create', ['class' => 'btn btn-create']); ?>

                  <?php echo Form::close(); ?>

               </div>
            </div>
            <br><br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>


<!------------- View Modal Starts ----->
<div id="view" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog text-center"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Manage RealTimeBoards</h4>
         </div>
         <div class="modal-body "  >
         
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  <table class="table table-bordered table-striped table-hover" id="printTable">
                     <thead>
                        <tr>
                           <th>Sr</th>
                           <th>Title</th>
                           <th>RealTimeBoard Id</th>
                           <th>Created At</th>
                           <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_realtime_boards', 'delete_realtime_boards')): ?>
                           <th class="text-center">Actions</th>
                           <?php endif; ?>
                        </tr>
                     </thead>
                     <tbody>
					     <?php $x=0;?>
                        <?php $__currentLoopData = $realtimeboard; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<?php $x++;?>
                        <tr>
                           <td><?php echo e($x); ?></td>
                           <td><?php echo e($item['title']); ?></td>
                           <td><?php echo e($item['realtime_board_id']); ?></td>
                           <td><?php echo e($item['created_at']); ?></td>
                           <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_realtime_boards')): ?>
                           <td class="text-center">
                              <?php echo $__env->make('projects._actions_realtime', [
                              'entity' => 'realtimeboards',
                              'id' => $item['id']
                              ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                           </td>
                           <?php endif; ?>
                        </tr>
                        <!-------inner model starts-------->		
                        <div id="edit<?php echo e($item['id']); ?>" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                           <div class="modal-dialog"  >
                              <!-- Modal content-->
                              <div class="modal-content"  >
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-center">Update Board</h4>
                                 </div>
                                 <div class="modal-body "  >
                                   
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                          <?php echo Form::model($id, ['method' => 'PUT', 'route' => ['realtimeboards.update',  $item['id'] ] ]); ?>

                                          <?php echo $__env->make('projects._form_edit_realtime', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                          <!-- Submit Form Button -->
                                          <?php echo Form::submit('Update', ['class' => 'btn btn-primary']); ?>

                                          <?php echo Form::close(); ?>

                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <script>
                           function close_inner<?php echo $item['id']; ?>()
                           {
                           	$('#edit<?php echo $item['id']; ?>').modal('hide');
                           }
                           
                        </script>
                        <!-------inner model ends-------->
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>