<?php
use App\Role;
?>

<?php $__env->startSection('title', 'Users'); ?>
<?php $__env->startSection('content'); ?>

   <?php $__env->startSection('header'); ?>
         <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->	
            <div class="static-content-wrapper">
			
			<section id="main-header">
			<div class="container-fluid">
			  <div class="row">
			  <div class="col-sm-12">
			  
			  	    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			    <ul class="nav nav-tabs">
			   <?php $records = App\Role::get();$x=0; //echo $records;?>
			  
			  
			   <?php    if(Request::path()=='users') { $class1 = 'active';} else { $class1='';} ?>
						    <li class="<?php echo $class1;?>"><a   href="<?php echo e(URL::to('users')); ?>">ALL</a></li>
			   <?php if(count($records)): ?>
					  <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					     <?php  $x++; if(Request::path()=='users/'.$record->name) { $class = 'active';} else { $class='';} ?>
						    <li class="<?php echo $class;?>"><a   href="<?php echo e(URL::to('users/'.$record->name)); ?>"><?php echo e($record->name); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
               <?php endif; ?>


                      
                   
                </ul>

				
 


<div class="tab-content">
 
   <div class="row">
     <div class="col-sm-7 header-left">
       <h3 class="text-left">Users List</h3>
	  </div>
	    <div class="col-sm-5 header-right">
		
             <!-- <button class="btn btn-trans dropdown-toggle" type="button" data-toggle="dropdown">New Transaction
              <span class="fa fa-caret-down"></span></button>-->
			   <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('add_users')): ?>
               <button type="button" class="btn btn-trans"  data-toggle="modal" data-target="#myModal">Add New</button>
            <?php endif; ?>
 

     </div>
    </div>
	
	
	<div class="row">
     <div class="col-sm-12">
	   <div class="panel">
	     
		 
		 	     <div class="row">
		 
		
 
 
        <div class="col-sm-7 ">
 
 </div> 
 
  
 
		
		
 
		
	      <div class="col-sm-5  actions-right">
	 
			<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('export_user')): ?>
			<?php if( !isset($role))
			{?>
			<a href="<?php echo e(URL::to('downloadExcel/xls')); ?>"><i class="fa fa-share-square-o" title="Export Excel"  ></i></a>
			<?php } else { ?>
			<a href="<?php echo e(URL::to('downloadExcel/'.@$role.'/xls')); ?>"><i class="fa fa-share-square-o" title="Export Excel"  ></i></a>
			<?php } ?>
			
			<?php endif; ?>
		 
		  </div>
		    </div>
			
			<script>
 function printData()
{
   var divToPrint=document.getElementById("result-set");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}

$('#print_btn').on('click',function(){
printData();
})
			 
			
			</script>
			
		 
	 
		 <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title"><?php echo e($result->total()); ?> <?php echo e(str_plural('Users', $result->count())); ?> </h3>
        </div>
        <div class="col-md-7 page-action text-right">
           
        </div>
    </div>

    <div class="result-set" id="result-set" >
        <table class="table table-bordered table-striped table-hover table-responsive" id="printTable">
            <thead>
            <tr>
                <th>Sr</th>
                <th>Name</th>
			 
                <th>Email</th>
                <th>Role</th>
				<th>Designations</th>
				 <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_contact')): ?><th>Contacts</th><?php endif; ?>
                <th>Created At</th>
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_users', 'delete_users')): ?>
                <th class="text-center">Actions</th>
                <?php endif; ?>
            </tr>
            </thead>
         	<?php $x=0;?>
            <?php $__currentLoopData = $result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			      <?php $x++;
		             $employee_designation_id = @\App\EmployeeDesignations::where('user_id',$item->id)->get();
				  ?>
					  
		 
                <tr>
                    <td><?php echo e($x); ?></td>
                    <td>							<?php if($item->photo == '')
			{
				echo '<h4 class="text-center" style="height:100px;width:100px;border:1px solid #ccc;border-radius:5px;padding-top:20px;">';
				?>
				
                         <a href="<?php echo e(url('/users/show_edit_user_profile/'.@$item->id )); ?>">Upload Image</a>				
				<?php
				echo '</h4>';
			}
			else
			{
?>
			<img src="<?php echo url('/').'/'.$item->photo;?>" height="100px" >
			<?php }
			?>
					<?php echo e($item->first_name); ?> <?php echo e($item->last_name); ?>


					
					</td>
				 
                    <td><?php echo e($item->email); ?></td>
                    <td><?php echo e($item->roles->implode('name', ', ')); ?></td>
					<td>
					 <?php $__currentLoopData = $employee_designation_id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<?php $title = @\App\Designations::where('id',$d->designation_id)->first(["title"])->title; ?>
					<?php echo e($title); ?><br>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</td>
					<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_contact')): ?><td><a   href="<?php echo e(URL::to('contacts/'.$item->roles->implode('name', ', ').'/'.$item->id)); ?>">Contacts</a></td><?php endif; ?>
                    <td><?php echo e(@$item->created_at->toFormattedDateString()); ?></td>

                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_users')): ?>
                    <td class="text-center">
                        <?php echo $__env->make('shared._actions', [
                            'entity' => 'users',
                            'id' => $item->id
                        ], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
		
		
		

        <div class="text-right">
            <?php echo e($result->links()); ?>

        </div>
    </div>
		  
		   
 

	   </div>
	  </div> 
    </div>
	
 
 
</div>
			
              </div> 
            </div>
			</div>
			</section>
			</div>
         </div>
      </div>
	  
<?php
      $roles = Role::pluck('name', 'id');
?>
	  
<!--Add New Model Starts-->
	  <style>
	  .modal-dialog {
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
}

.modal-content {
  height: auto;
  min-height: 100%;
  border-radius: 0;
}
</style>
<!-- Modal -->




<!------------- Add New User ----->
<div id="myModal" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
  <div class="modal-dialog"  >

    <!-- Modal content-->
    <div class="modal-content"  >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title  text-center">Add New <span>User</span></h3>
      </div>
      <div class="modal-body "  >
<br><br>

    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <?php echo Form::open(['route' => ['users.store'] ]); ?>

                <?php echo $__env->make('user._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- Submit Form Button -->
                <?php echo Form::submit('Create', ['class' => 'btn btn-create']); ?>

            <?php echo Form::close(); ?>

        </div>
    </div>
	<br><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!------>
	  
 
   <?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>