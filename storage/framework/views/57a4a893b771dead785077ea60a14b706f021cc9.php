<html>
   
   <head>
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title><?php echo $__env->yieldContent('title'); ?></title>
	        <!-- Font Awesome -->
	   <link href='http://fonts.googleapis.com/css?family=RobotoDraft:300,400,400italic,500,700' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700' rel='stylesheet' type='text/css'>
      <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	  <link rel="stylesheet" href="<?php echo e(URL::asset('admin/css/bootstrap.min.css')); ?>">
       <link href="<?php echo e(URL::asset('admin/css/styles.css')); ?>" type="text/css" rel="stylesheet">
	   <link href="<?php echo e(URL::asset('admin/css/main-style.css')); ?>" type="text/css" rel="stylesheet">
	   <link href="<?php echo e(URL::asset('admin/css/custom.css')); ?>" type="text/css" rel="stylesheet">
	   
	   
	   <!--------Assets For Image Uploading------------------->
	   <link href="<?php echo e(URL::asset('admin/img-uploads/css/style.css')); ?>" rel="stylesheet">
       <script src="<?php echo e(URL::asset('admin/img-uploads/js/jquery.min.js')); ?>"></script>
      <script src="<?php echo e(URL::asset('admin/img-uploads/js/jquery.form.js')); ?>"></script>
	  
	   <!--------Assets For Tags------------------->
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tagmanager/3.0.2/tagmanager.min.css"> 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tagmanager/3.0.2/tagmanager.min.js"></script>  

   
    <!--------Assets For Date Picker-------------------
   <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
   
   
   <!--------Assets For Time Picker and date&time picker------------------>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script> 
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet"> 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
   
   <!--------Assets For Date Range Picker------------------
   <script type="text/javascript" src="http://demo.itsolutionstuff.com/demoTheme/js/moment.min.js"></script>
   <script type="text/javascript" src="http://demo.itsolutionstuff.com/demoTheme/js/daterangepicker.js"></script>
   <link rel="stylesheet" type="text/css" href="http://demo.itsolutionstuff.com/demoTheme/css/daterangepicker.css" />

  <!--------Assets For Switches------------------
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.css" />
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.js"></script>
   
   <!--------Assets For DatePair ------------------
<script type="text/javascript" src="<?php echo e(URL::asset('admin/datepair/datepair.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/datepair/jquery.datepair.js')); ?>"></script>


   <!--------Assets For Imgae Uploading By Camera caPture ------------------ 
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" /> 
	
    <script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"></script>-->
  
    <script src="<?php echo e(URL::asset('admin/js/bootstrap-multiselect.js')); ?>"   ></script>
	  <link href="<?php echo e(URL::asset('admin/css/bootstrap-multiselect.css')); ?>" type="text/css" rel="stylesheet">
 
	       <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
   </head>
   
   <body>
 
	      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			

	
  
      
 
         <?php echo $__env->yieldContent('content'); ?>
       

	  
	  

      <script src="<?php echo e(URL::asset('admin//js/bootstrap.min.js')); ?>"></script> 								<!-- Load Bootstrap -->  
      <script src="<?php echo e(URL::asset('admin//js/enquire.min.js')); ?>"></script> 									<!-- Enquire for Responsiveness -->
      <script src="<?php echo e(URL::asset('admin//plugins/nanoScroller/js/jquery.nanoscroller.min.js')); ?>"></script> <!-- nano scroller --> 
      <script src="<?php echo e(URL::asset('admin//js/application.js')); ?>"></script> 
	  
	  
	   
	   <script>
  $('.toggle-button').click(function(e){
    $('#myDiv').toggleClass('fullscreen'); 
});
 </script>

	  <!---------script for checkbox table------------->
	  <script type='text/javascript'> 
        $(window).load(function(){
           $('#selectAll').click(function (e) {
               $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
            });
        }); 
     </script>
   	 
	 <script>
	 $(document).ready(function () {
        $('.checkAll').on('click', function () {
           $(this).closest('table').find('tbody :checkbox')
             .prop('checked', this.checked)
             .closest('tr').toggleClass('selected', this.checked);
        });

        $('tbody :checkbox').on('click', function () {
            $(this).closest('tr').toggleClass('selected', this.checked); //Classe de seleção na row
            $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
        });
      });  
	  </script>
	  
 
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

   </body>
</html>