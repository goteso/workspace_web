<!-- First Name Form Input -->
 

<div class="form-group <?php if($errors->has('title')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('title', 'Title'); ?>

    <?php echo Form::text('title', @$item['title'], ['class' => 'form-control '  ] ); ?>

    <?php if($errors->has('title')): ?> <p class="help-block"><?php echo e($errors->first('title')); ?></p> <?php endif; ?>
</div>

<div class="form-group <?php if($errors->has('url_type_id')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('url_type_id', 'Url Type'); ?>

    <?php $urls_types = \App\UrlsTypes::get(); ?>
       <select name="url_type_id" class="form-control">
           <option value="">Select Type</option>
               <?php $__currentLoopData = $urls_types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <option value="<?php echo e($ct->id); ?>"><?php echo e($ct->title); ?></option>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
       </select>
      <?php if($errors->has('url_type_id')): ?> <p class="help-block"><?php echo e($errors->first('url_type_id')); ?></p> <?php endif; ?>
</div>

<div class="form-group <?php if($errors->has('full_url')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('full_url', 'Full Url'); ?>

    <?php echo Form::text('full_url', @$item['full_url'], ['class' => 'form-control '  ] ); ?>

    <?php if($errors->has('full_url')): ?> <p class="help-block"><?php echo e($errors->first('full_url')); ?></p> <?php endif; ?>
</div>


 