<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_users')): ?>
    <a href="<?php echo e(route($entity.'.edit', [str_singular($entity) => $id])); ?>" title="Edit" class="action-edit" >
        <i class="fa fa-edit"></i> </a>
<?php endif; ?>

<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete_users')): ?>
    <?php echo Form::open( ['method' => 'delete', 'url' => route($entity.'.destroy', ['user' => $id]), 'style' => 'display: inline', 'onSubmit' => 'return confirm("Are yous sure wanted to delete it?")']); ?>

        <button type="submit" class="action-delete"  title="Delete">
            <i class="fa fa-trash"></i>
        </button>
    <?php echo Form::close(); ?>

<?php endif; ?>