<?php $__env->startSection('title', 'Goteso Workspace'); ?>
<?php $__env->startSection('content'); ?>

   <?php $__env->startSection('header'); ?>
         <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
           







		   <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->	
            <div class="static-content-wrapper">
			
			<section id="main-header">
			<div class="container-fluid">
			  <div class="row">
			  <div class="col-sm-12">
 
 
  <div id="expenses" class="tab-pane fade in active">
   <div class="row">
     <div class="col-sm-7 header-left">
       <h3 class="text-left">Dashboard</h3>
	  </div>
	    <div class="col-sm-5 header-right">
 
     </div>
    </div>
	
	
	<div class="row">
     <div class="col-sm-12">
	   <div class="">
 
 
      <div class="row">
	  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_users')): ?>
	           <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" >
			   <div class="panel" style="max-height:400px;overflow:auto;padding:2%">
			   <h3 class="text-center" ><b>Users</b> List</h3>
			       <table class="table" id="printTable">
                           <thead>
                              <tr>
                                 <th>Sr</th>
                                 <td>Name</td>
                                  
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;?>
                              <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php $x++;?>
                              <tr>
                                 <td><?php echo e($x); ?></td>
                                 <td><?php echo e($u['first_name']); ?> <?php echo e($u['last_name']); ?></td>
                   
                              </tr>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </tbody>
                        </table>
			     </div>
			   </div>
		<?php endif; ?>	  

<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('view_project')): ?>		
	           <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" >
			      <div class="panel" style="max-height:400px;overflow:auto;padding:2%;">
			   <h3 class="text-center" style="font-weight:bold;padding-bottom:10px;border-bottom:1px solid #ccc;">Projects</h3>
			   
			   <ol style="">
			   <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $u): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			   <li style="padding: 2px 5px;font-size: 16px;"><a href="<?php echo e(URL::to('project_details/realtimeboards/'.$u['id'])); ?>" target="_blank"><?php echo e($u['project_title']); ?></a></li>
			   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			   </ol>
 
			   </div>
			   </div>
			   <?php endif; ?>	
			   
			   
			   
			   
	  </div>
		  
		   
		  
 

	   </div>
	  </div> 
    </div>
	
  </div>
  
 
			
              </div> 
            </div>
			</div>
			</section>
			</div>
			
			
			
			
       </div>
      </div>
	  
 
 
 
 
 
 <script>
    $('#page').ready( function(){
		$("#chooseFile").click(function(e){
			e.preventDefault();
			$("input[type=file]").trigger("click");
		});
		$("input[type=file]").change(function(){
			var file = $("input[type=file]")[0].files[0];            
			$("#preview").empty();
			displayAsImage3(file, "preview");
			
			$info = $("#info");
			$info.empty();
			if (file && file.name) {
				$info.append("<li>name:<span>" + file.name + "</span></li>");
			}
			if (file && file.type) {
				$info.append("<li>size:<span>" + file.type + " bytes</span></li>");
			}
			if (file && file.size) {
				$info.append("<li>size:<span>" + file.size + " bytes</span></li>");
			}
			if (file && file.lastModifiedDate) {
				$info.append("<li>lastModifiedDate:<span>" + file.lastModifiedDate + " bytes</span></li>");
			}
			$info.listview("refresh");
		});
    });

    function displayAsImage3(file, containerid) {
		if (typeof FileReader !== "undefined") {
			var container = document.getElementById(containerid),
			    img = document.createElement("img"),
			    reader;
			container.appendChild(img);
			reader = new FileReader();
			reader.onload = (function (theImg) {
				return function (evt) {
					theImg.src = evt.target.result;
				};
			}(img));
			reader.readAsDataURL(file);
		}
	}
	
    </script>
	
   <?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>