<!-- First Name Form Input -->
 

<div class="form-group <?php if($errors->has('project_title')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('project_title', 'Project Title'); ?>

    <?php echo Form::text('project_title', @$project_title, ['class' => 'form-control ', 'placeholder' => 'Project Title' ] ); ?>

    <?php if($errors->has('project_title')): ?> <p class="help-block"><?php echo e($errors->first('project_title')); ?></p> <?php endif; ?>
</div>


<div class="form-group <?php if($errors->has('project_start_date')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('project_start_date', 'Project Start Date'); ?>

    <?php echo Form::text('project_start_date', @$project_start_date, ['class' => 'form-control datetimepicker', 'placeholder' => 'Project Start Date' ] ); ?>

    <?php if($errors->has('project_start_date')): ?> <p class="help-block"><?php echo e($errors->first('project_start_date')); ?></p> <?php endif; ?>
</div>


<div class="form-group <?php if($errors->has('slack_channel_id')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('slack_channel_id', 'Slack Channel Id'); ?>

    <?php echo Form::text('slack_channel_id', @$slack_channel_id, ['class' => 'form-control', 'placeholder' => 'Slack Channel Id' ] ); ?>

    <?php if($errors->has('slack_channel_id')): ?> <p class="help-block"><?php echo e($errors->first('slack_channel_id')); ?></p> <?php endif; ?>
</div>

<div class="form-group <?php if($errors->has('drive_admin_folder')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('drive_admin_folder', 'Drive Admin Folder'); ?>

    <?php echo Form::text('drive_admin_folder', @$drive_admin_folder, ['class' => 'form-control', 'placeholder' => 'Drive Admin Folder' ] ); ?>

    <?php if($errors->has('drive_admin_folder')): ?> <p class="help-block"><?php echo e($errors->first('drive_admin_folder')); ?></p> <?php endif; ?>
</div>

<div class="form-group <?php if($errors->has('drive_development_folder')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('drive_development_folder', 'Drive Development Folder'); ?>

    <?php echo Form::text('drive_development_folder', @$drive_development_folder, ['class' => 'form-control', 'placeholder' => 'Drive Development Folder' ] ); ?>

    <?php if($errors->has('drive_development_folder')): ?> <p class="help-block"><?php echo e($errors->first('drive_development_folder')); ?></p> <?php endif; ?>
</div>

 


<div class="form-group <?php if($errors->has('status')): ?> has-error <?php endif; ?>">
<?php echo Form::label('status', 'Project Status'); ?>

 <?php echo e($errors->has('status')); ?>

<select class="form-control" name="status" id="status" required >
<option value="">Select Project Status</option>      
<option value="0" <?php if(isset($id) && \App\Project::where('id',@$id)->first(["status"])->status=='0'){ echo 'SELECTED';} ?> >Work Pending</option>
<option value="1" <?php if(isset($id) && \App\Project::where('id',@$id)->first(["status"])->status=='1'){ echo 'SELECTED';} ?>>Team Working</option>
<option value="2" <?php if( isset($id) &&\App\Project::where('id',@$id)->first(["status"])->status=='2'){ echo 'SELECTED';} ?>>Waiting For Client</option>
<option value="3" <?php if( isset($id) &&\App\Project::where('id',@$id)->first(["status"])->status=='3'){ echo 'SELECTED';} ?>>Completed</option>
<option value="4" <?php if( isset($id) &&\App\Project::where('id',@$id)->first(["status"])->status=='4'){ echo 'SELECTED';} ?>>Cancelled</option>
<option value="4" <?php if( isset($id) &&\App\Project::where('id',@$id)->first(["status"])->status=='5'){ echo 'SELECTED';} ?>>Payment Pending</option>
 </select>
   <?php if($errors->has('status')): ?> <p class="help-block"><?php echo e($errors->first('status')); ?></p> <?php endif; ?>
</div>

 
 
 




 
<div class="form-group <?php if($errors->has('employees_array')): ?> has-error <?php endif; ?>">
<?php echo Form::label('employees_array', 'Employees'); ?>


 
   <select   class="form-control" id="example-getting-started" name="employees_array[]" multiple="multiple" style="width:100%">
    
      <?php $__currentLoopData = $employees_array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	  
	  	<?php   $count1 = \App\ProjectEmployees::where('project_id',@$id)->where('employee_id',@$ct->id)->get();
		if(isset($id) && $count1->count() > 0 ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
         <option value="<?php echo e($ct->id); ?>" <?php echo $s1;?> ><?php echo e($ct->first_name." ".$ct->last_name); ?></option>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
</div>




<div class="form-group <?php if($errors->has('clients_array')): ?> has-error <?php endif; ?>">
    <?php echo Form::label('employees', 'Clients'); ?>

   <select  class="form-control" id="example-getting-started2" name="clients_array[]"  multiple="multiple">
     <?php $__currentLoopData = $clients_array; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ct2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php   $count2 = \App\ProjectClients::where('project_id',@$id)->where('client_id',$ct2->id)->get();
		if(isset($id) && $count2->count() > 0 ) { $s2 = 'SELECTED';} else { $s2 = '';}
		?>
	     <option value="<?php echo e($ct2->id); ?>" <?php echo $s2;?>  ><?php echo e($ct2->first_name); ?></option>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
</div>
 

<script type="text/javascript">
   $(document).ready(function() {
       $('#example-getting-started').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
	   $('#example-getting-started2').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
 });
</script>

 

 
<script type='text/javascript'>
$(window).load(function(){
$('.datetimepicker').datetimepicker({  format:'DD/MM/YYYY hh:mm A', });
 
});
</script>