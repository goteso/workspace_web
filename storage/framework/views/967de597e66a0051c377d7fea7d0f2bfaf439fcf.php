<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit_payment_fee_types')): ?>
    <a    data-toggle="modal" data-target="#edit<?php echo $id;?>"  style="background:transparent;border:none;outline:none;color:grey">
        <i class="fa fa-edit"></i> </a>
		
		 
<?php endif; ?>

<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('delete_payment_fee_types')): ?>
    <?php echo Form::open( ['method' => 'delete', 'url' => route($entity.'.destroy', ['user' => $item['id']]), 'style' => 'display: inline', 'onSubmit' => 'return confirm("Are you sure want to delete it?")']); ?>

        <button type="submit"   style="background:transparent;border:none;outline:none;color:grey">
            <i class="fa fa-trash"></i>
        </button>
    <?php echo Form::close(); ?>

<?php endif; ?>