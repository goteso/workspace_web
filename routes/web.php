<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('ajax_test', 'HomeController@ajax_test')->name('ajax_test');

 //get tags via ajax
	Route::get('/get_tags', 'TutorialsController@get_tags')->name('get_tags');
	Route::get('/get_auth_skills', 'UserController@get_auth_skills')->name('get_auth_skills');
	
	
Route::get('/logout', 'HomeController@logout')->name('logout');


//Route::get('dashboard', ['middleware' => 'auth', 'uses' => 'UserController@getDashboard', 'as' => 'user.dashboard',]);
Route::group( ['middleware' => ['auth']], function() {
	Route::get('/users/{role}', 'UserController@users_by_role')->name('users_by_role');
    Route::resource('users', 'UserController');

    Route::resource('roles', 'RoleController');
    Route::resource('posts', 'PostController');
	Route::resource('contact_types', 'ContactTypeController');
	 
	Route::resource('credentials_types', 'CredentialsTypesController');
	Route::resource('urls_types', 'UrlsTypesController');
	Route::resource('urls', 'UrlsController');
	Route::get('/urls/analytics/{urlId}', 'UrlsController@get_analytics');
		
	Route::resource('payment_modes', 'PaymentModesController');
	Route::resource('payment_fee_types', 'PaymentFeeTypesController');
	
	Route::resource('designations', 'DesignationsController');
	Route::resource('card_issue_types', 'CardIssueTypesController');
	Route::resource('employee_designations', 'EmployeeDesignationsController');
	
	Route::post('add_issue_type', 'CardIssueTypesController@add_issue_type')->name('add_issue_type');
	
	Route::resource('tutorials', 'TutorialsController');
	//Route::get('add_tutorials', function () {    return view('tutorials.new');})->name("add_tutorials");
	Route::get('add_tutorials', 'TutorialsController@create')->name('add_tutorials');
	Route::post('get_tutorials', 'TutorialsController@get_tutorials')->name('get_tutorials');
	
	Route::get('get_tutorials_list', 'TutorialsController@get_tutorials_list')->name('get_tutorials_list');
	
	Route::resource('contacts', 'ContactController');
	Route::resource('projects', 'ProjectController');
	
	Route::resource('realtimeboards', 'ProjectRealTimeController');
	Route::resource('trelloboards', 'ProjectTrelloController');
	Route::resource('credentials', 'ProjectCredentialsController');
	Route::resource('notes', 'ProjectNotesController');
	
	Route::get('/get_auth_skills', 'UserController@get_auth_skills')->name('get_auth_skills');
	
   
	
	Route::get('/projects_list/projects_by_status/{status}', 'ProjectController@projects_by_status')->name('projects_by_status');
	Route::get('/project_details/realtimeboards/{id}', 'ProjectController@show_realtimeboard')->name('show_realtimeboard');
	Route::get('/project_details/trelloboards/{id}', 'ProjectController@show_trelloboard')->name('show_trelloboard');
	Route::get('/project_details/credentials/{id}', 'ProjectController@show_credentials')->name('show_credentials');
	Route::get('/project_details/notes/{id}', 'ProjectController@show_notes')->name('show_notes');
	Route::get('/project_details/drives/{id}', 'ProjectController@show_google_drives')->name('show_google_drives');
	Route::get('/project_details/cards/{id}', 'BoardController@show_cards')->name('show_cards');
	
	
	Route::get('/users/show_edit_user_profile/{id}', 'UserController@show_edit_user_profile')->name('show_edit_user_profile');
	Route::post('/users/update_edit_user_profile/', 'UserController@update_edit_user_profile')->name('update_edit_user_profile');
	
	Route::post('/projects_by_employee/', 'ProjectController@projects_by_employee')->name('projects_by_employee');
	
	//contacts starts
	Route::get('/contacts/{role}/{linked_id}', 'ContactController@index')->name('get_contacts');
	
	
});


 
Route::get('downloadExcel/{role}/xls', 'UserController@downloadExcelByRole');
Route::get('downloadExcel/xls', 'UserController@downloadExcelAll');
 
Route::get('downloadExcelProject/{role}/xls', 'ProjectController@downloadExcelByRole');
Route::get('downloadExcelProject/xls', 'ProjectController@downloadExcelAll');

Route::post('importExcel', 'ExportController@importExcel');




Route::get('register', ['middleware' => 'guest', 'uses' => 'UserController@getRegister', 'as' => 'auth.register',]);
Route::post('register', ['middleware' => 'guest', 'uses' => 'UserController@postRegister',]);
Route::get('dashboard', ['middleware' => 'auth', 'uses' => 'UserController@getDashboard', 'as' => 'user.dashboard',]);
Route::get('profile', ['middleware' => 'auth', 'uses' => 'UserController@getProfile', 'as' => 'user.profile',]);
Route::get('activity', ['middleware' => 'auth', 'uses' => 'UserActivityController@getUserActivity', 'as' => 'user.activity',]);

Route::post('postBoard', ['middleware' => 'auth', 'uses' => 'BoardController@postBoard',]);
Route::post('update-board-favourite', ['middleware' => 'auth', 'uses' => 'BoardController@updateBoardFavourite',]);

/**
 * Board
 */
Route::group(
    ['prefix' => 'board'],
    function () {
        
		Route::post('/postListName', ['uses' => 'ListController@postListName',]);
        
		Route::post('/delete-list', ['uses' => 'ListController@deleteList',]);
        
		Route::post('/update-list-name', ['uses' => 'ListController@updateListName',]);

        Route::post('/postCard', ['uses' => 'CardController@postCard',]);
        
		Route::post('/getPusherData', ['uses' => 'BoardController@getPusherData',]);
				
        Route::post('/changeCardList', ['uses' => 'CardController@changeCardList',]);
        
		Route::post('/deleteCard', ['uses' => 'CardController@deleteCard',]);
        
		Route::post('/getCardDetail', ['uses' => 'CardController@getCardDetail',]);
        
		Route::post('/update-card-data', ['uses' => 'CardController@updateCardData',]);

        Route::post('/save-comment', ['uses' => 'CommentController@saveComment',]);

        Route::post('/save-task', ['uses' => 'TaskController@saveTask',]);
        
		Route::post('/delete-task', ['uses' => 'TaskController@deleteTask',]);
        
		Route::post('/update-task-completed', ['uses' => 'TaskController@updateTaskCompleted',]);
		
		Route::post('/file_uploader', ['uses' => 'FileUploader@file_uploader',])->name('file_uploader');

        Route::get('/{id?}', ['middleware' => 'auth', 'uses' => 'BoardController@getBoardDetail', 'as' => 'user.boardDetail',]);
		
 
		
		Route::get('/project_details/cards/{id?}', ['middleware' => 'auth', 'uses' => 'BoardController@getBoardDetailProject', 'as' => 'user.boardDetail',]);
		
		Route::post('/cards-filter', ['middleware' => 'auth', 'uses' => 'BoardController@getBoardDetailFiltered', 'as' => 'user.cards-filter',]);
		
		Route::get('/duedate/{id?}', ['middleware' => 'auth', 'uses' => 'BoardController@getBoardDetail', 'as' => 'user.boardDetail',]);
        
		Route::post('create-user-activity', ['uses' => 'UserActivityController@createUserActivity']);
    }
);

/**
 * Password Reset
 */
Route::group(
    ['prefix' => 'password'],
    function () {
        // Password reset link request routes...
        Route::get('/email', 'Auth\PasswordController@getEmail');
        Route::post('/email', 'Auth\PasswordController@postEmail');

        // Password reset routes...
        Route::get('/reset/{token}', 'Auth\PasswordController@getReset');
        Route::post('/reset', 'Auth\PasswordController@postReset');
    }
);

Route::post('create-user-activity', ['uses' => 'UserActivityController@createUserActivity']);



Route::get('test/{cardId}', function ($cardId) {
	 
    event(new App\Events\StatusLiked($cardId));
    return "Event has been sent!";
});
 