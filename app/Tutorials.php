<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutorials extends Model
{
	
	
    protected $fillable = ['title','author_id','designation_id','content'];

     public function tutorials_tags()
    {
        return $this->hasMany(TutorialsTags::class,'tutorial_id','id');
    }
}
