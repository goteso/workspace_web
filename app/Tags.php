<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
	
  protected $fillable = ['title','designation_id'];

  public function TutorialsTags()
    {
        return $this->hasMany(TutorialsTags::class);
    }
	
  public function tutorials_tags()
    {
        return $this->hasMany(TutorialsTags::class,'tutorial_id','id');
    }
}
