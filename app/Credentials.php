<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credentials extends Model
{
  protected $fillable = ['project_id','credentials_type_id','host','user','password','type'];
 
 
     public function project()
    {
        return $this->belongsTo(Project::class);
    }
	
	
 
	
}
