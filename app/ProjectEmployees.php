<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectEmployees extends Model
{
   protected $fillable = ['project_id','employee_id'];
   
       public function project()
    {
        return $this->belongsTo(Project::class);
    }
	
	
}
