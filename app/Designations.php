<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designations extends Model
{
	
	
    protected $fillable = ['title'];

  public function credentials()
    {
        return $this->belongsTo(Credentials::class);
    }
}
