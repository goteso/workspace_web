<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectClients extends Model
{
   protected $fillable = ['project_id','client_id'];
}
