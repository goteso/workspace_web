<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\UrlsTypes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class UrlsTypesController extends Controller
{
    
  
    public function index()
    {
		//if(!Auth::user()->can('view_designations')){ return view('unauthorised');} 
		$display_modal_name = 'Url Types';
		$model_name = 'urls_types';
	    $modelName = "App\UrlsTypes";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
 
        return view('urls_types.index', compact('result','display_modal_name','model_name'));
    }
	
 
 
    public function store(Request $request)
    {
		//if(!Auth::user()->can('add_designations')){ return view('unauthorised');} 
        $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = UrlsTypes::create($request->toArray()))  {
 
           flash('Url Type has been created.');

        } else {
            flash()->error('Unable to create Url Type.');
        }

        return back();
    }

 
    public function update(Request $request, $id)
    {
		if(!Auth::user()->can('edit_designations')){ return view('unauthorised');} 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = UrlsTypes::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Url Type has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
		
		 if(!Auth::user()->can('delete_designations')){ return view('unauthorised');} 
	   if( \App\UrlsTypes::findOrFail($id)->delete() ) {
            flash()->success('Url Type has been deleted');
        } else {
            flash()->success('Url Type not deleted');
        }

        return redirect()->back();
    }

 
}
