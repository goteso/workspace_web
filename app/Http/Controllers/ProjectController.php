<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Project;
use App\ProjectEmployees;
use App\ProjectClients;
use App\Contact;
use App\Permission;
use App\ContactType;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Board;
use App\CardTag;
use App\BoardList;
use App\BoardCard;


use Excel;

class ProjectController extends Controller
{
    
 //use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
	public function index()
    {
		if(!Auth::user()->can('view_project')){ return view('unauthorised');} 
		
		$modelName = "App\Project";  
        $model = new $modelName();
		$auth_role = Auth::user()->roles()->get()[0]->name;
		
 
	   
	  		if($auth_role == 'Admin')
		{
			
		  $result = $model::latest();
		}
		else if($auth_role == 'Employee')
		{
			$result = $model::latest()->whereHas('project_employees', function($q){$q->whereIn('employee_id', [Auth::user()->id]);});
		}
		else if($auth_role == 'Client')
		{
			$result = $model::latest()->whereHas('project_employees', function($q){$q->whereIn('employee_id', [Auth::user()->id]);});
		}
		else
		{
			$result = $model::latest();
		}
	  
	  
	  
	  
		   $result2 = $result->where('status', '=', '0')->orWhere('status', '=', '1')->orWhere('status', '=', '2');
 
	 $result =$result2->paginate(50);
	  
	  
	  
	 
		
		$modelName1 = "App\User";  
        $model1 = new $modelName1();
		$employees_array = $model1::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Employee']);})->get();
		
		$modelName2 = "App\User";  
        $model2 = new $modelName2();
		$clients_array = $model2::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Client']);})->get();
         
        return view('projects.index', compact('result','employees_array','clients_array'));
    }
	
	
	
		public function projects_by_employee(Request $request)
    {
		$user_id = $request->user_id;
		if(!Auth::user()->can('view_project')){ return view('unauthorised');} 
		
		$modelName = "App\Project";  
        $model = new $modelName();
		$auth_role = Auth::user()->roles()->get()[0]->name;
		
 
	  
	  
	   
		 
			
		  $result3 = $model::latest()->whereHas('project_employees', function($q1) use ($user_id)  {$q1->where("employee_id",$user_id);});
 
	  

	 
	  
	 
 	
 
	 $result =$result3->paginate(50);
	  
	   
	  
 
	 
		
		$modelName1 = "App\User";  
        $model1 = new $modelName1();
		$employees_array = $model1::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Employee']);})->get();
		
		$modelName2 = "App\User";  
        $model2 = new $modelName2();
		$clients_array = $model2::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Client']);})->get();
		
		$selected_employee_id = $user_id;
         
        return view('projects.index', compact('result','employees_array','clients_array','selected_employee_id'));
    }
	
	
	
	
	
	
	
	
		public function projects_by_status($status)
    {
		if(!Auth::user()->can('view_project')){ return view('unauthorised');} 
		
		$modelName = "App\Project";  
        $model = new $modelName();
		$auth_role = Auth::user()->roles()->get()[0]->name;
		
		if($auth_role == 'Admin')
		{
			
		 
		  
			$result = $model::latest();
		}
		else if($auth_role == 'Employee')
		{
			$result = $model::latest()->whereHas('project_employees', function($q){$q->whereIn('employee_id', [Auth::user()->id]);});
		}
		else if($auth_role == 'Client')
		{
			$result = $model::latest()->whereHas('project_employees', function($q){$q->whereIn('employee_id', [Auth::user()->id]);});
		}
		else
		{
			$result = $model::latest();
		}
	  
	  if($status == 'active')
	  {
		   $result2 = $result->where('status', '=', '0')->orWhere('status', '=', '1')->orWhere('status', '=', '2');
	  }
	  else if($status == 'all')
	  {
		  $result2 = $result;
	  }
	  else
	  {
		   $result2 = $result->where('status', '=', $status); 
	  }
	 
	  
	  
	  $result =$result2->paginate(50);
	 
		
		$modelName1 = "App\User";  
        $model1 = new $modelName1();
		$employees_array = $model1::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Employee']);})->get();
		
		$modelName2 = "App\User";  
        $model2 = new $modelName2();
		$clients_array = $model2::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Client']);})->get();
         
        return view('projects.index', compact('result','employees_array','clients_array'));
    }
	
	
	
	
	
	

	
	
	public function project_access($id)
	{
	   $auth_role = Auth::user()->roles()->get()[0]->name;
       if($auth_role == 'Admin' )
        {}
        else if($auth_role == 'Employee')
        {
           @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
        else
        {
            @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
		 
	}
 
	
	
		public function downloadExcelAll()
	{
		
		$data = Project::get()->toArray();
		return Excel::create('goteso_projects', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xls');
	}
	
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id');

        return view('user.new', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if(!Auth::user()->can('add_project')){ return view('unauthorised');} 
		$clients_array= $request->clients_array;
		$employees_array= $request->employees_array;
 
		 
		 $this->validate($request, [
            'project_title' => 'bail|required',
			'project_start_date' => 'bail|required',
			'slack_channel_id' => 'bail|required',
		     
        ]);
		
       // Create the project
        if ( $user = \App\Project::create($request->toArray()) ) {
 
 
        if(sizeof($employees_array) > 0)
		{
         foreach($employees_array as $emp)
		{
			  $count = \App\ProjectEmployees::where('project_id',$user->id)->where('employee_id',$emp)->get();
			  if($count->count() < 1)
			  {
			        $model = new ProjectEmployees();
                    $model->employee_id = $emp;
					$model->project_id = $user->id;
					$model->save();
			  }
		}
		}
		
		 if(sizeof($clients_array) > 0)
		{
		 foreach($clients_array as $abc )
		{
			$count2 = \App\ProjectClients::where('project_id',$user->id)->where('client_id', $abc )->get();
			  if($count2->count() < 1)
			  {
					$model2 = new ProjectClients();
                    $model2->client_id = $abc;
					$model2->project_id = $user->id;
					$model2->save();
			  }
		 }
		}
		    flash('Project has been created.');

        } else {
		 
            flash()->error('Unable to create Project.');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_realtimeboard($id)
    {
		
		
   $auth_role = Auth::user()->roles()->get()[0]->name;
       if($auth_role == 'Admin' )
        {}
        else if($auth_role == 'Employee')
        {
           @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
        else
        {
            @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
		
		
		 
	    // if(!Auth::user()->can('view_realtime_boards')){ return view('unauthorised');} 
	    $modelName = "App\Project";  
        $model = new $modelName();
	    //$result = $model::latest()->where("role",$role)->where('linked_id',$linked_id)->paginate(50);
		$result = $model::where("id",$id)->with("ProjectRealTimeBoard")->get()->toArray();
	    //return view('projects.index', compact('result','role','linked_id'));
		return view('projects.show_realtimeboard', compact('result','id'));
    }
	
	
	
	
	    public function show_trelloboard($id)
    {
		   $auth_role = Auth::user()->roles()->get()[0]->name;
       if($auth_role == 'Admin' )
        {}
        else if($auth_role == 'Employee')
        {
           @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
        else
        {
            @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
		
		
		 
	     if(!Auth::user()->can('view_trello_boards')){ return view('unauthorised');} 
	    $modelName = "App\Project";  
        $model = new $modelName();
	    //$result = $model::latest()->where("role",$role)->where('linked_id',$linked_id)->paginate(50);
		$result = $model::where("id",$id)->with("ProjectTrelloBoard")->get()->toArray();
	    //return view('projects.index', compact('result','role','linked_id'));
		return view('projects.show_trelloboard', compact('result','id'));
    }
	
	
	
		    public function show_credentials($id)
    {
	   $auth_role = Auth::user()->roles()->get()[0]->name;
       if($auth_role == 'Admin' )
        {}
        else if($auth_role == 'Employee')
        {
           @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
        else
        {
            @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
	     if(!Auth::user()->can('view_credentials')){ return view('unauthorised');} 
	    $modelName = "App\Project";  
        $model = new $modelName();
	    //$result = $model::latest()->where("role",$role)->where('linked_id',$linked_id)->paginate(50);
		$result = $model::where("id",$id)->with("Credentials")->get()->toArray();
	    //return view('projects.index', compact('result','role','linked_id'));
		return view('projects.show_credentials', compact('result','id','employees_array'));
    }
	
	
	
	
	
	

	
	
	
	
	
	
			    public function show_notes($id)
    {
		
		   $auth_role = Auth::user()->roles()->get()[0]->name;
       if($auth_role == 'Admin' )
        {}
        else if($auth_role == 'Employee')
        {
           @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
        else
        {
            @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
		
		
	 
	   if(!Auth::user()->can('view_notes')){ return view('unauthorised');} 
	    $modelName = "App\Project";  
        $model = new $modelName();
	    //$result = $model::latest()->where("role",$role)->where('linked_id',$linked_id)->paginate(50);
		
		 if($auth_role == 'Admin' )
        { $notes = \App\Notes::where("linked_id",$id)->orderby("pinned",'DESC')->orderBy('id', 'DESC')->get()->toArray(); }
	else{
		$notes = \App\Notes::where("linked_id",$id)->where("notes_type",'project_development')->orderby("pinned",'DESC')->orderBy('id', 'DESC')->get()->toArray();
	}
		
		
		 
	 
	    //return view('projects.index', compact('result','role','linked_id'));
		 
		return view('projects.show_notes', compact('notes','id','employees_array'));
    }
	
	
	public function show_google_drives($id)
    {
		
		   $auth_role = Auth::user()->roles()->get()[0]->name;
       if($auth_role == 'Admin' )
        {}
        else if($auth_role == 'Employee')
        {
           @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
        else
        {
            @$count = @\App\ProjectEmployees::where('employee_id',Auth::user()->id)->where('project_id',$id)->get()->count();
		    if($count == 0) { return back();}
        }
		
		
	 
		$auth_role = Auth::user()->roles()->get()[0]->name;
		 
 
	    $modelName = "App\Project";  
        $model = new $modelName();
	    //$result = $model::latest()->where("role",$role)->where('linked_id',$linked_id)->paginate(50);
		$result = $model::where("id",$id)->get()->toArray();
	    //return view('projects.index', compact('result','role','linked_id'));
		return view('projects.show_google_drives', compact('result','id','auth_role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	   if(!Auth::user()->can('edit_project')){ return view('unauthorised');} 
		$project = Project::find($id);
        $roles = Role::pluck('name', 'id');
        $permissions = Permission::all('name', 'id');
		
		$modelName1 = "App\User";  
        $model1 = new $modelName1();
		$employees_array = $model1::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Employee']);})->get();
		
		$modelName2 = "App\User";  
        $model2 = new $modelName2();
		$clients_array = $model2::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Client']);})->get();
        return view('projects.edit', compact('project', 'roles', 'permissions' , 'id','employees_array','clients_array'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if(!Auth::user()->can('edit_project')){ return view('unauthorised');} 
		
		$clients_array= $request->clients_array;
		$employees_array= $request->employees_array;
		
		
        $this->validate($request, [
            'project_title' => 'bail|required',
			'project_start_date' => 'bail|required',
			'slack_channel_id' => 'bail|required',
        ]);

        // Get the user
        $user = Project::findOrFail($id);

        // Update user
        $user->fill($request->toArray());
 
        $user->save();
		
		
		$project_emp = \App\ProjectEmployees::where('project_id',$id)->delete();
		
	    if(sizeof($employees_array) > 0)
		{
         foreach($employees_array as $emp)
		{
			  $count = \App\ProjectEmployees::where('project_id',$user->id)->where('employee_id',$emp)->get();
			  if($count->count() < 1)
			  {
			        $model = new ProjectEmployees();
                    $model->employee_id = $emp;
					$model->project_id = $user->id;
					$model->save();
			  }
		}
		}
			
		$project_clt = \App\ProjectClients::where('project_id',$id)->delete();
		if(sizeof($clients_array) > 0)
		{
		 foreach($clients_array as $abc )
		{
			 
			 $count2 = \App\ProjectClients::where('project_id',$user->id)->where('client_id', $abc )->get();
			  if($count2->count() < 1)
			  {
					$model2 = new ProjectClients();
                    $model2->client_id = $abc;
					$model2->project_id = $user->id;
					$model2->save();
			  }
		 }
		}
		flash()->success('Project has been updated.');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy($id)
    {
		if(!Auth::user()->can('delete_project')){ return view('unauthorised');} 
       if( Project::findOrFail($id)->delete() ) {
            flash()->success('Project has been deleted');
        } else {
            flash()->success('Project not deleted');
        }
        return redirect()->back();
    }

    /**
     * Sync roles and permissions
     *
     * @param Request $request
     * @param $user
     * @return string
     */
    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);
        return $user;
    }
}
