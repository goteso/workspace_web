<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

use Auth;
use App\CardIssueTypes;
use \App\BoardCard;
use \App\CardTag;
use \App\CardTask;
use \App\Comment;

class CardController extends Controller
{
    protected $boardCard;
    protected $cardTag;
    protected $cardTask;
    protected $comment;

    public function __construct(BoardCard $boardCard, CardTag $cardTag, CardTask $cardTask, Comment $comment)
    {
        $this->boardCard = $boardCard;
        $this->cardTag = $cardTag;
        $this->cardTask = $cardTask;
        $this->comment = $comment;
    }

    /**
     * Creates a new card in database 
     * @param  Request $request have the input data
     * @return object created card
     */
    public function postCard(Request $request)
    {
	 
		$this->validate($request, [
            'card-title' => 'required',
        ]);
		
		$arr = '';
        return $this->boardCard->createCard($request, $arr);
    }

    /**
     * Change the card list. For example if the card is in list x then you drag it to the
     * other list named y. So, now using this function card list has been updaed to list_id 
     * of y in database.
     * @param  Request $request has input data for the function
     * @return object updated data
     */
    public function changeCardList(Request $request)
    {
        return $this->boardCard->updateCardListId($request);
    }

    /**
     * Delete a card from a list.
     * @param  Request $request has the id of the card
     * @return boolean if the card is deleted or not
     */
    public function deleteCard(Request $request)
    {
        return $this->boardCard->deleteCard($request);
    }

    /**
     * Get a card detail from database.
     * @param  Request $request has the data that is being used in this function
     * @return object card detail
     */
    public function getCardDetail(Request $request)
    {
		 
		$cards = \App\BoardCard::where("parent_card_id",$request->cardId)->get();
		
			 
		 for($r=0;$r<sizeof($cards);$r++)
		 {
			 
			
			 @$cards[$r]["project_title"] = @\App\Project::where('id',$cards[$r]["project_id"])->first(["project_title"])->project_title;
			 
			  @$cards[$r]["due_date"] =  @\Carbon\Carbon::parse($cards[$r]['due_date'])->format("j M");
			  @$cards[$r]["card_issue_type_image"] =  @\App\CardIssueTypes::where('id',$cards[$r]["card_issue_type_id"])->first(["image"])->image;
		 }
		 
		 
 
        return [
            "card" => $this->boardCard->getCard($request->get("cardId")),
            "label" => $this->cardTag->getCardTag($request->get("cardId")),
            "task" => $this->cardTask->getCardTasks($request->get("cardId")),
            "comment" => $this->comment->getCardComment($request->get("cardId")),
			 "board_card_files" => $this->boardCard->card_files($request->get("cardId")),
			"subCardsList" => $cards,
        ];     
    }

    /**
     * Update card date in database.
     * @param  Request $request has the input data for this function
     * @return object updated data
     */
    public function updateCardData(Request $request)
    {
        $this->cardTag->deleteCardTag($request->get("cardId"));
        $this->cardTag->createCardTag($request);
        $this->boardCard->updateCard($request);

        return [
            "cardTitle" => $request->get("cardName"),
            "cardId" => $request->get("cardId"),
        ];
    }
}
