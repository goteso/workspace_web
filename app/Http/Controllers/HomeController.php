<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use Illuminate\Http\Request;
 

use App\Traits\slack; // <-- you'll need this line...

class HomeController extends Controller
{
	
		use slack; // <-- ...and also this line.
	
	public function traits(){
	 return $this->slack();
 }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

 
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
   {
	//   $this->send_slack_notification('D5NM44UMT','Hi Deepu Singla');
	   
	   // Send a message to the default channel
//\Slack::send('Hello world!');
 
 
 
 
 



 

// Send a message to a different channel
//Slack::to('#accounting')->send('Are we rich yet?');

// Send a private message
//Slack::to('@username')->send('psst!');
                $modelName = "App\Project";  
       $model = new $modelName();
        $auth_role = Auth::user()->roles()->get()[0]->name;
        
        if($auth_role == 'Admin')
        {
            $result = $model::latest()->get();
        }
        else if($auth_role == 'Employee')
        {
            $result = $model::latest()->whereHas('project_employees', function($q){$q->whereIn('employee_id', [Auth::user()->id]);})->get();
        }
        else if($auth_role == 'Client')
        {
            $result = $model::latest()->whereHas('project_employees', function($q){$q->whereIn('employee_id', [Auth::user()->id]);})->get();
        }
        else
        {
            $result = $model::latest()->get();
        }
        
        $projects = $result;

        $users = \App\User::get(["id","first_name","last_name"]);

       return view('home',compact("projects","users"));
   }
	
	
	  public function ajax_test()
    {
        echo 'Home';
    }
	
	
	
}
