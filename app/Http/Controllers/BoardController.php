<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use DB;

use Auth;
 
use App\Board;
use App\CardTag;
use App\BoardList;
use App\BoardCard;

class BoardController extends Controller
{
    protected $board;
    protected $user;
    protected $cardTag;
    protected $boardList;
    protected $boardCard;

    public function __construct(Board $board, User $user, CardTag $cardTag, BoardList $boardList, BoardCard $boardCard)
    {
        $this->board = $board;
        $this->user = $user;
        $this->cardTag = $cardTag;
        $this->boardList = $boardList;
        $this->boardCard = $boardCard;
    }

    /**
     * Creates a new Board
     * @param  Request $request have the input data
     * @return object return the newly created board            
     */
    public function postBoard(Request $request)
    {
        $this->validate($request, [
            'boardTitle'        => 'required|unique:board,boardTitle',
            'boardPrivacyType'  => 'required',   
        ]);
        
        return $this->board->createBoard($request, Auth::id());
    }

    /**
     * Get the Board details
     * @param  Request $request have the input data
     * @return view board page or view
     */
    public function getBoardDetail(Request $request)
    {
		 
	 
	 
		//$boardDetail = $this->board->getBoard($request->id);
		$lists = $this->boardList->getBoardList($request->id);
	   
	   $cards = json_decode(json_encode($this->boardCard->getBoardCards($request->id)), True);
	 
	   
	  // return $cards;
        $cardTaskCount = json_decode(json_encode($this->boardCard->cardTotalTask()), True);
        $boards = $this->board->getUserBoards(Auth::id());
		$recentBoards = $this->board->getUserRecentBoards(Auth::id());
		
		$modelName1 = "App\User";  
        $model1 = new $modelName1();
		$employees_array = $model1::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Employee','Admin']);})->get();
		 
		 
		  
		 
		 
		 for($r=0;$r<sizeof($cards);$r++)
		 {
			 @$cards[$r]["project_title"] = @\App\Project::where('id',$cards[$r]["project_id"])->first(["project_title"])->project_title;
			 if($cards[$r]["card_issue_type_id"] != '')
			 {
			   @$cards[$r]["card_issue_type_image"] = \App\CardIssueTypes::where('id',$cards[$r]["card_issue_type_id"])->first(["image"])->image;
			   @$cards[$r]["card_issue_type_title"] = \App\CardIssueTypes::where('id',$cards[$r]["card_issue_type_id"])->first(["title"])->title;
			 }
			 else
			 {
			   @$cards[$r]["card_issue_type_image"] = '';
			   @$cards[$r]["card_issue_type_title"] = '';
			 }
			 			
						
					 $date = new \Carbon\Carbon;
                     $d = new \Carbon\Carbon( $cards[$r]['due_date'] );   
				     @$d =$d->format('Y');
	

			 if($cards[$r]['due_date'] == null || $cards[$r]['due_date'] == 'null' || $cards[$r]['due_date'] == '' || $d < 200   )
			 {
				 $cards[$r]['due_date'] = '';
				    
			 }
else
{
	  @$cards[$r]["due_date"] =  @\Carbon\Carbon::parse($cards[$r]['due_date'])->format("j M");
}
			    @$cards[$r]["created_at"] =  @\Carbon\Carbon::parse($cards[$r]['created_at'])->format("Y-m-d");
				
				$users_array = explode(',',$cards[$r]["user_id"]);
				 
				 $users_images = '';
				  for($r2=0;$r2<sizeof($users_array);$r2++)
		        {
					
			
				
				 
					
						//@$users_images .=    '<span style="float:right;background:#f5f5f5;padding:1%">'.$first_name[0]. $last_name[0].'</span>';
					 
				 $img = url('/').'/'.@\App\User::where('id',$users_array[$r2])->first(["photo_thumb"])->photo_thumb;
				 
				 
				 if(sizeof($users_array) > 0)
				 {
					 
				 if(@\App\User::where('id',$users_array[$r2])->first(["photo_thumb"])->photo_thumb == ''   )
				 {
					 
					 $first_name = 	@\App\User::where('id',$users_array[$r2])->first(["first_name"])->first_name;
				        $last_name = 	@\App\User::where('id',$users_array[$r2])->first(["last_name"])->last_name;
					  $img = str_replace("\\",'"\"',$img);
						 @$users_images .=    '<span style="float:right;background:#d5d9db;height:20px;width:20px;color:#000; border-radius:2px;text-align:center;margin:1px 2px 0px;text-transform:uppercase;" title="'.$first_name.'">'.$first_name[0].$last_name[0].'</span></span>';
				 }
				 else
				 {
					  $first_name = 	@\App\User::where('id',$users_array[$r2])->first(["first_name"])->first_name;
				        $last_name = 	@\App\User::where('id',$users_array[$r2])->first(["last_name"])->last_name;
					 	  $img = str_replace("\\",'"\"',$img);
						 @$users_images .=    '<span style="float:right;background:#ccc; border-radius:2px;border:1px solid #d5d9db;margin:1px 2px 0px" ><img style="height:18px;width:18px;display:block;" title="'.$first_name.'" alt="AF" src="'.$img.'"></span>';
				 }
				 }
						
				 
				if($cards[$r]["user_id"] == '')
	{
		@$users_images='';
	}					
					
			
					
				//	return  '<img style="max-height:20px" src="'.$img.'">';
				 
					
					 
			 
		        }
				
				 @$cards[$r]["users_images"] =  @$users_images;
			 
		 }
		
	 
	
	 $card_issue_types = \App\CardIssueTypes::get(['image','id','title']);
	 
	 
	
 if(isset($request->card_id) && $request->card_id != '')
 {
	 $card_id = $request->card_id;
 }
 else
 {
	  $card_id = '';
 }
		if($request->id !='') { $project_id = $request->id;}
	    return view('user.board_angular', compact('lists', 'cards', 'cardTaskCount', 'boards', 'recentBoards','project_id','employees_array','card_issue_types','card_id'));
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
 

    /**
     * Get the Board details
     * @param  Request $request have the input data
     * @return view board page or view
     */
    public function getPusherData(Request $request)
    {
		 
	 
		//$boardDetail = $this->board->getBoard($request->id);
		$lists = $this->boardList->getBoardList($request->id);
	   
	   $cards = json_decode(json_encode($this->boardCard->getBoardCards($request->id)), True);
	   
	   
	  // return $cards;
        $cardTaskCount = json_decode(json_encode($this->boardCard->cardTotalTask()), True);
        $boards = $this->board->getUserBoards(Auth::id());
		$recentBoards = $this->board->getUserRecentBoards(Auth::id());
		
		$modelName1 = "App\User";  
        $model1 = new $modelName1();
		$employees_array = $model1::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Employee','Admin']);})->get();
		 
		 
		 
		 
		 
		 for($r=0;$r<sizeof($cards);$r++)
		 {
			 @$cards[$r]["project_title"] = @\App\Project::where('id',$cards[$r]["project_id"])->first(["project_title"])->project_title;
			 if($cards[$r]["card_issue_type_id"] != '')
			 {
			   @$cards[$r]["card_issue_type_image"] = \App\CardIssueTypes::where('id',$cards[$r]["card_issue_type_id"])->first(["image"])->image;
			 }
			 else
			 {
			   @$cards[$r]["card_issue_type_image"] = '';
			 }
			 			
								 

					 $date = new \Carbon\Carbon;
                     $d = new \Carbon\Carbon( $cards[$r]['due_date'] );   
				     @$d =$d->format('Y');
	

			 if($cards[$r]['due_date'] == null || $cards[$r]['due_date'] == 'null' || $cards[$r]['due_date'] == '' || $d < 200   )
			 {
				 $cards[$r]['due_date'] = '';
				    
			 }
else
{
	  @$cards[$r]["due_date"] =  @\Carbon\Carbon::parse($cards[$r]['due_date'])->format("j M");
}
			    @$cards[$r]["created_at"] =  @\Carbon\Carbon::parse($cards[$r]['created_at'])->format("Y-m-d");
				
				$users_array = explode(',',$cards[$r]["user_id"]);
				 
				 $users_images = '';
				  for($r2=0;$r2<sizeof($users_array);$r2++)
		        {
					
			
				
				 
					
						//@$users_images .=    '<span style="float:right;background:#f5f5f5;padding:1%">'.$first_name[0]. $last_name[0].'</span>';
					 
				 $img = url('/').'/'.@\App\User::where('id',$users_array[$r2])->first(["photo_thumb"])->photo_thumb;
				 
				 
				 if(sizeof($users_array) > 0)
				 {
					 
				 if(@\App\User::where('id',$users_array[$r2])->first(["photo_thumb"])->photo_thumb == ''   )
				 {
					 
					 $first_name = 	@\App\User::where('id',$users_array[$r2])->first(["first_name"])->first_name;
				        $last_name = 	@\App\User::where('id',$users_array[$r2])->first(["last_name"])->last_name;
					  $img = str_replace("\\",'"\"',$img);
						 @$users_images .=    '<span style="float:right;background:#d5d9db;height:20px;width:20px;color:#000; border-radius:2px;text-align:center;margin:1px 2px 0px;text-transform:uppercase;">'.$first_name[0].$last_name[0].'</span></span>';
				 }
				 else
				 {
					 	  $img = str_replace("\\",'"\"',$img);
						 @$users_images .=    '<span style="float:right;background:#ccc; border-radius:2px;border:1px solid #d5d9db;margin:1px 2px 0px"><img style="height:18px;width:18px;display:block;" alt="AF" src="'.$img.'"></span>';
				 }
				 }
						
				 
				if($cards[$r]["user_id"] == '')
	{
		@$users_images='';
	}					
					
			
					
				//	return  '<img style="max-height:20px" src="'.$img.'">';
				 
					
					 
			 
		        }
				
				 @$cards[$r]["users_images"] =  @$users_images;
			 
		 }
		
	 
	
	 $card_issue_types = \App\CardIssueTypes::get(['image','id','title']);
	 
 
		if($request->id !='') { $project_id = $request->id;}
		
		$main=array();
		$main["lists"]=$lists;
		$main["cards"]=$cards;
		$main["cardTaskCount"]=$cardTaskCount;
		$main["boards"]=$boards;
		$main["project_id"]=@$request->id;
		$main["employees_array"]=$employees_array;
		$main["card_issue_types"]=$card_issue_types;
		return $main;
		
		
	    return view('user.board_angular', compact('lists', 'cards', 'cardTaskCount', 'boards', 'recentBoards','project_id','employees_array','card_issue_types'));
    }
	
	
	
	
	
	
	
	
	    public function getBoardDetailProject(Request $request)
    {
		 
	 
		//$boardDetail = $this->board->getBoard($request->id);
		$lists = $this->boardList->getBoardList($request->id);
	   
	   $cards = json_decode(json_encode($this->boardCard->getBoardCards($request->id)), True);
	   
	   
	  // return $cards;
        $cardTaskCount = json_decode(json_encode($this->boardCard->cardTotalTask()), True);
        $boards = $this->board->getUserBoards(Auth::id());
		$recentBoards = $this->board->getUserRecentBoards(Auth::id());
		
		$modelName1 = "App\User";  
        $model1 = new $modelName1();
		$employees_array = $model1::latest()->whereHas('roles', function($q){$q->whereIn('name', ['Employee']);})->get();
		 
		 
		 
		 
		 for($r=0;$r<sizeof($cards);$r++)
		 {
			 @$cards[$r]["project_title"] = @\App\Project::where('id',$cards[$r]["project_id"])->first(["project_title"])->project_title;
			 if($cards[$r]["card_issue_type_id"] != '')
			 {
			   @$cards[$r]["card_issue_type_image"] = \App\CardIssueTypes::where('id',$cards[$r]["card_issue_type_id"])->first(["image"])->image;
			 }
			 else
			 {
			   @$cards[$r]["card_issue_type_image"] = '';
			 }
			  @$cards[$r]["due_date"] =  @\Carbon\Carbon::parse($cards[$r]['due_date'])->format("j M");
			  
			  
			  
			  
		 }
		
	
	
	
	
 
	 $card_issue_types = \App\CardIssueTypes::get(['image','id','title']);
 
		if($request->id !='') { $project_id = $request->id;}
	    return view('projects.show_cards', compact('lists', 'cards', 'cardTaskCount', 'boards', 'recentBoards','project_id','employees_array','card_issue_types'));
    }
	
	
	
	
	
	
 
	
	
	
	
	    public function getBoardDetailFiltered(Request $request)
    {
 
 return $request->filter;
       $lists = $this->boardList->getBoardList($request->filter_project_id);
		
		
	    $cards = json_decode(json_encode($this->boardCard->getBoardCardsFiltered($request->filter_project_id , $request->filter)), True);
        $cardTaskCount = json_decode(json_encode($this->boardCard->cardTotalTask()), True);
              $boards = $this->board->getUserBoards(Auth::id());
		$recentBoards = $this->board->getUserRecentBoards(Auth::id());
		
		
	 
		$filter = $request->filter;
		
		if($request->filter_project_id !='') { $project_id = $request->filter_project_id;}
	    return view('user.board', compact('lists', 'cards', 'cardTaskCount', 'boards', 'recentBoards','project_id','filter'));
    }
	
	
	
	    public function getBoardDetailAjax(Request $request)
    {
		//$boardDetail = $this->board->getBoard($request->id);
		
        $lists = $this->boardList->getBoardList($request->id);
		
		
		
        $cards = json_decode(json_encode($this->boardCard->getBoardCards()), True);
	 
		 
        $cardTaskCount = json_decode(json_encode($this->boardCard->cardTotalTask()), True);
        $boards = $this->board->getUserBoards(Auth::id());
		
        $recentBoards = $this->board->getUserRecentBoards(Auth::id());
		
	 
	
 
        return view('user.board_ajax', compact('lists', 'cards', 'cardTaskCount', 'boards', 'recentBoards'));
    }
	
	
	

    /**
     * Update the board is_favourite attribute in the database. 
     * @param  Request $request have the input data
     * @return object the updated data           
     */
    public function updateBoardFavourite(Request $request)
    {
        return $this->board->updateBoardFavourite($request);
    }

}