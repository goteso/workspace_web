<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\Designations;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class DesignationsController extends Controller
{
    
  
    public function index()
    {
		if(!Auth::user()->can('view_designations')){ return view('unauthorised');} 
		$display_modal_name = 'Designations';
		$model_name = 'designations';
	    $modelName = "App\Designations";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
 
        return view('designations.index', compact('result','display_modal_name','model_name'));
    }
	
 
 
    public function store(Request $request)
    {
		if(!Auth::user()->can('add_designations')){ return view('unauthorised');} 
        $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = Designations::create($request->toArray()))  {
 
           flash('Designation has been created.');

        } else {
            flash()->error('Unable to create Designation.');
        }

        return back();
    }

 
    public function update(Request $request, $id)
    {
		if(!Auth::user()->can('edit_designations')){ return view('unauthorised');} 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = Designations::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Designation has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
		
		 if(!Auth::user()->can('delete_designations')){ return view('unauthorised');} 
	   if( \App\Designations::findOrFail($id)->delete() ) {
            flash()->success('Designation has been deleted');
        } else {
            flash()->success('Designation not deleted');
        }

        return redirect()->back();
    }

 
}
