<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Project;
use App\ProjectRealTimeBoard;
use App\Contact;
use App\Permission;
use App\ContactType;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class ProjectRealTimeController extends Controller
{
    
 //use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
	public function index()
    {
		if(!Auth::user()->can('view_project')){ return view('unauthorised');} 
	    $modelName = "App\Project";  
        $model = new $modelName();
		//$result = $model::latest()->where("role",$role)->where('linked_id',$linked_id)->paginate(50);
		$result = $model::latest()->paginate(50);
        //return view('projects.index', compact('result','role','linked_id'));
		return view('projects.index', compact('result'));
    }
	
	
	
 
	
	
	
	
		public function downloadExcelByRole($role)
	{
		    if(!Auth::user()->permission=='export_user'){ return  back();}
		$data = User::whereHas('roles', function($q) use ($role){$q->whereIn('name', [$role]);})->get()->toArray();
		return Excel::create('goteso_users', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xls');
			
			
	}
	
	
	
		
		public function downloadExcelAll()
	{
		
		$data = User::get()->toArray();
		return Excel::create('goteso_userssss', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xls');
	}
	
	
	
	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id');

        return view('user.new', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if(!Auth::user()->can('add_realtime_boards')){ return view('unauthorised');} 
		 $this->validate($request, [
       ]);
		 if ( $user = \App\ProjectRealTimeBoard::create($request->toArray()) ) {
               flash('Board has been created.');
         } else {
		    flash()->error('Unable to create Board.');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if(!Auth::user()->can('edit_realtime_boards')){ return view('unauthorised');} 
		$project = ProjectRealTimeBoard::find($id);
        $roles = Role::pluck('name', 'id');
        $permissions = Permission::all('name', 'id');

        return view('contacts.edit', compact('project', 'roles', 'permissions' , 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
		 if(!Auth::user()->can('edit_realtime_boards')){ return view('unauthorised');} 
        $this->validate($request, [
            
        ]);

		
        // Get the user
        $user = ProjectRealTimeBoard::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Board has been updated.');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy($id)
    {
	 if(!Auth::user()->can('delete_realtime_boards')){ return view('unauthorised');} 
       if( ProjectRealTimeBoard::findOrFail($id)->delete() ) {
            flash()->success('Board has been deleted');
        } else {
            flash()->success('Board not deleted');
        }

        return redirect()->back();
    }

    /**
     * Sync roles and permissions
     *
     * @param Request $request
     * @param $user
     * @return string
     */
    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }
}
