<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\Tutorials;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class TutorialsController extends Controller
{
    
  
    public function index()
    {
		$display_modal_name = 'Tutorials List';
		$model_name = 'tutorials';
	    $modelName = "App\Tutorials"; 
        $model = new $modelName();		
		
		$skills =  $this->get_auth_skills();
		$result = array();
		foreach($skills as $s)
		{
			 
			$result[] = $model::latest()->with('tutorials_tags')->where( "designation_id",$s )->first();
		}
		
		foreach($result as $t)
		{
			@$tutorials_tags = $t->tutorials_tags;
			@$designation_id = $t->designation_id;
			@$designation_title = \App\Designations::where( 'id',$designation_id )->first(["title"])->title;
			@$t->designation_title = $designation_title;
			
			$tags_array = array();
			
			if(sizeof($tags_array) > 0)
			{
			foreach($tutorials_tags as $tt)
			{
				$tag_id = $tt->tag_id;
				$tag_title = \App\Tags::where( 'id',$tag_id )->first(["title"])->title;
				$tags_array[] = $tag_title;
				
			}
			$t->tags_array = implode (", ", $tags_array);
			$t->tags = $tags_array;
			}
			else
			{
					$t->tags_array = '';
			$t->tags = [];
			}
			unset($t->tutorials_tags);
		}
		 
  
 
 
        return view('tutorials.index', compact('result','display_modal_name','model_name'));
    }
	
 public function get_tutorials_list()
 {
	   $display_modal_name = 'Tutorials List';
		$model_name = 'tutorials';
	    $modelName = "App\Tutorials";  
        $model = new $modelName();
		$result = $model::latest()->where( "author_id",Auth::user()->id )->paginate(25);
 
 
        return view('tutorials.index_list', compact('result','display_modal_name','model_name'));
 }
 
 
 
  public function get_tutorials(request $request)
 {
	 
	    $display_modal_name = 'Tutorials List';
		$model_name = 'tutorials';
	    $modelName = "App\Tutorials";  
        $model = new $modelName();
		$result = $model::where( "id",$request->id )->get();
		
		$designation_title = \App\Designations::where('id',$result[0]["designation_id"])->first(["title"])->title;
		$result[0]["designation_title"] = $designation_title;
		
		$first_name = \App\User::where('id',$result[0]["author_id"])->first(["first_name"])->first_name;
		$last_name = \App\User::where('id',$result[0]["author_id"])->first(["last_name"])->last_name;
		$result[0]["author_name"] = $first_name." ".$last_name;
 
 
 $result[0]["t"] = \Carbon\Carbon::parse($result[0]["created_at"])->format(" F jS ,Y , h:i A");
 return $result;
 
        return view('tutorials.index_list', compact('result','display_modal_name','model_name'));
 }
 
 
 
    public function store(Request $request)
    {
		
 
  
 
 $tags_array = explode(",",$request->tags);
		 
        $this->validate($request, [
          
        ]);

		            $tutorials = new Tutorials();
                    $tutorials->title = $request->title;
					$tutorials->author_id = Auth::user()->id;
					$tutorials->designation_id = $request->designation_id;
					$tutorials->content = $request->content;
					$tutorials->save();
					
					 foreach($tags_array as $tag)
		               {
						  $count = \App\Tags::where('title',$tag)->where('designation_id',$request->designation_id)->get();
		                  if(sizeof($count) < 1)
						  {
						    $tags = new \App\Tags();
                            $tags->title = $tag;
						    $tags->designation_id = $request->designation_id;
					        $tags->save();
							
							$tutorials_tag = new \App\TutorialsTags();
                            $tutorials_tag->tutorial_id = $tutorials->id;
						    $tutorials_tag->tag_id = $tags->id;
					        $tutorials_tag->save();
						  }
			           }
					   
		$tutorial_id = $tutorials->id;
					
					
 
					
      // Create the user
        if ( $tutorial_id !='' && $tutorial_id != null)  {
 
           flash('Tutorial has been Added.');

        } else {
            flash()->error('Unable to Add Tutorial.');
        }

        return back();
    }

 
 
    public function update(Request $request, $id)
    {
		$tags_array = explode(",",$request->tags);
		
		 $this->validate($request, [
            
        ]);
		
	 

		// Get the user
        $tutorials = Tutorials::findOrFail($id);
        $tutorials->title = $request->title;
		$tutorials->author_id = Auth::user()->id;
		$tutorials->designation_id = $request->designation_id;
		$tutorials->content = $request->content;
		$tutorials->save();
	 
		
		 
	 
		 
		 $delete = \App\TutorialsTags::where('tutorial_id',$id)->delete();
					
					 foreach($tags_array as $tag)
		               {
						  $count = \App\Tags::where('title',$tag)->where('designation_id',$request->designation_id)->get();
		                  if(sizeof($count) < 1)
						  {
						    $tags = new \App\Tags();
                            $tags->title = $tag;
						    $tags->designation_id = $request->designation_id;
					        $tags->save();
							
							$tutorials_tag = new \App\TutorialsTags();
                            $tutorials_tag->tutorial_id = $tutorials->id;
						    $tutorials_tag->tag_id = $tags->id;
					        $tutorials_tag->save();
						  }
						  else
						  {
						    $tag_id = \App\Tags::where('title',$tag)->where('designation_id',$request->designation_id)->first(["id"])->id;
							$tutorials_tag = new \App\TutorialsTags();
                            $tutorials_tag->tutorial_id = $tutorials->id;
						    $tutorials_tag->tag_id = $tag_id;
					        $tutorials_tag->save();
						  }
			           }
					   
		$tutorial_id = $tutorials->id;
		
		
		

        flash()->success('Tutorials has been updated.');

        return back();
    }
 
 
     public function edit($id)
    {
		 
        $display_modal_name = 'Tutorials List';
		$model_name = 'tutorials';
	    $modelName = "App\Tutorials";  
        $model = new $modelName();
		
		$result = $model::latest()->where( "id",$id )->get();
		$added_tags = \App\TutorialsTags::latest()->where( "tutorial_id",$id )->get();
	 
	    $tags = \App\Tags::pluck('title')->toArray(); 
		
		$added_tags_array= array();
		foreach($added_tags as $t)
		{
			 
			$added_tags_array[] = \App\Tags::where('id',$t->tag_id)->first(["title"])->title;
			 
		}
		
		$employee_designation_id = @\App\EmployeeDesignations::where('user_id', Auth::user()->id)->get();
		 
		 //return $added_tags;
		 //return $tags;
 
        return view('tutorials.edit', compact('result','display_modal_name','model_name','tags','added_tags','employee_designation_id','added_tags_array'));
    }
 
 
    public function destroy($id)
    {
		if( \App\Tutorials::findOrFail($id)->delete() ) {
            flash()->success('Tutorial has been deleted');
        } else {
            flash()->success('Tutorial not deleted');
        }

        return redirect()->back();
    }
	
	
	
	public function create()
	{
 
		
		$employee_designation_id = @\App\EmployeeDesignations::where('user_id', Auth::user()->id)->get();
	    $tags = \App\Tags::pluck('title')->toArray(); 
	    
		return view('tutorials.new', compact('employee_designation_id','tags'));
	}

	
	
		
	public function get_tags()
	{
		$tags = \App\Tags::pluck('title')->toArray(); 
	    return $tags;
	}

	
		public function get_auth_skills()
	{
		$designations_array = array();
		$employee_designation_id = @\App\EmployeeDesignations::where('user_id',Auth::user()->id)->get();
		foreach($employee_designation_id as $d)
		{
			  $designations_array[] = \App\Designations::where('id',$d->designation_id)->first(["id"])->id;
		}
		return $designations_array;
	}
 
}
