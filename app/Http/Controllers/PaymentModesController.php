<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\PaymentModes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class PaymentModesController extends Controller
{
    
  
    public function index()
    {
		if(!Auth::user()->can('view_payment_modes')){ return view('unauthorised');} 
		$display_modal_name = 'Payment Modes';
		$model_name = 'payment_modes';
	    $modelName = "App\PaymentModes";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
 
        return view('payment_modes.index', compact('result','display_modal_name','model_name'));
    }
	
 
 
    public function store(Request $request)
    {
		if(!Auth::user()->can('add_payment_modes')){ return view('unauthorised');} 
        $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = PaymentModes::create($request->toArray()))  {
 
           flash('Payment Mode has been created.');

        } else {
            flash()->error('Unable to create Payment Mode.');
        }

        return back();
    }

 
    public function update(Request $request, $id)
    {
		if(!Auth::user()->can('edit_payment_modes')){ return view('unauthorised');} 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = PaymentModes::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Payment Mode has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
		if(!Auth::user()->can('delete_payment_modes')){ return view('unauthorised');} 
		 
	   if( \App\PaymentModes::findOrFail($id)->delete() ) {
            flash()->success('Payment Mode has been deleted');
        } else {
            flash()->success('Payment Mode not deleted');
        }

        return redirect()->back();
    }

 
}
