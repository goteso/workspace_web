<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\PaymentFeeTypes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class PaymentFeeTypesController extends Controller
{
    
  
    public function index()
    {
		if(!Auth::user()->can('view_payment_fee_types')){ return view('unauthorised');} 
		$display_modal_name = 'Payment Fee Type';
		$model_name = 'payment_fee_types';
	    $modelName = "App\PaymentFeeTypes";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
 
        return view('payment_modes.index', compact('result','display_modal_name','model_name'));
    }
	
 
 
    public function store(Request $request)
    {
		if(!Auth::user()->can('add_payment_fee_types')){ return view('unauthorised');} 
        $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = PaymentFeeTypes::create($request->toArray()))  {
 
           flash('Payment Fee Type has been created.');

        } else {
            flash()->error('Unable to create Payment Fee Type.');
        }

        return back();
    }

 
    public function update(Request $request, $id)
    {
		if(!Auth::user()->can('edit_payment_fee_types')){ return view('unauthorised');} 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = PaymentFeeTypes::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Payment Fee Type has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
		
		 if(!Auth::user()->can('delete_payment_fee_types')){ return view('unauthorised');} 
	   if( \App\PaymentFeeTypes::findOrFail($id)->delete() ) {
            flash()->success('Payment Fee Type has been deleted');
        } else {
            flash()->success('Payment Fee Type not deleted');
        }

        return redirect()->back();
    }

 
}
