<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Contact;
use App\Permission;
use App\ContactType;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class ContactController extends Controller
{
    
  //  use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($role,$linked_id)
    {
		if(!Auth::user()->can('view_contact')){ return view('unauthorised');} 
	    $modelName = "App\Contact";  
        $model = new $modelName();
		$result = $model::latest()->where("role",$role)->where('linked_id',$linked_id)->paginate(50);
        return view('contacts.index', compact('result','role','linked_id'));
    }
	
	
	
 
	
	
	
	
		public function downloadExcelByRole($role)
	{
		    if(!Auth::user()->can('export_contact')){ return view('unauthorised');} 
		$data = User::whereHas('roles', function($q) use ($role){$q->whereIn('name', [$role]);})->get()->toArray();
		return Excel::create('goteso_users', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xls');
			
			
	}
	
	
	
		
		public function downloadExcelAll()
	{
		 if(!Auth::user()->can('export_contact')){ return view('unauthorised');} 
		$data = User::get()->toArray();
		return Excel::create('goteso_userssss', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xls');
	}
	
	
	
	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		 if(!Auth::user()->can('add_contact')){ return view('unauthorised');} 
        $roles = Role::pluck('name', 'id');

        return view('user.new', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
		 if(!Auth::user()->can('add_contact')){ return view('unauthorised');}  
        $this->validate($request, [
            'role' => 'bail|required',
			'linked_id' => 'bail|required',
			'contact_type_id' => 'bail|required',
			'contact_value' => 'bail|required',
            'contact_title' => 'bail|required'
        ]);

       // Create the contact
        if ( $user = \App\Contact::create($request->toArray()) ) {
 
            flash('Contact has been created.');

        } else {
            flash()->error('Unable to create contact.');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id )
    {
 
 
		
		
		 if(!Auth::user()->can('edit_contact')){ return view('unauthorised');} 
		$contact = Contact::find($id);
        $roles = Role::pluck('name', 'id');
        $permissions = Permission::all('name', 'id');

        return view('contacts.edit', compact('contact', 'roles', 'permissions' , 'id','linked_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		 if(!Auth::user()->can('edit_contact')){ return view('unauthorised');} 
        $this->validate($request, [
            'contact_type_id' => 'bail|required',
			'contact_value' => 'bail|required',
            'contact_title' => 'bail|required'
        ]);

        // Get the user
        $user = Contact::findOrFail($id);

        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Contact has been updated.');
		
	 

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy($id)
    {
		 if(!Auth::user()->can('delete_contact')){ return view('unauthorised');} 
        if ( Auth::user()->id == $id ) {
            flash()->warning('Deletion of currently logged in user is not allowed :(')->important();
            return redirect()->back();
        }

        if( User::findOrFail($id)->delete() ) {
            flash()->success('Contact Type has been deleted');
        } else {
            flash()->success('Unable to delete Contact Type');
        }

        return redirect()->back();
    }

    /**
     * Sync roles and permissions
     *
     * @param Request $request
     * @param $user
     * @return string
     */
    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }
}
