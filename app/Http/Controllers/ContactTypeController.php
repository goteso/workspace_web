<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\ContactType;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class ContactTypeController extends Controller
{
    
 use Authorizable;

 
    public function index()
    {
		if(!Auth::user()->can('view_contact_types')){ return view('unauthorised');} 
		$display_modal_name = 'Contact Types';
		$model_name = 'contact_types';
	    $modelName = "App\ContactType";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
        return view('contact_types.index', compact('result','display_modal_name','model_name'));
    }
	
 
 
    public function store(Request $request)
    {
		if(!Auth::user()->can('add_contact_types')){ return view('unauthorised');} 
        $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = ContactType::create($request->toArray()))  {
 
           flash('Contact Type has been created.');

        } else {
            flash()->error('Unable to create Contact Type.');
        }

        return back();
    }

 
    public function update(Request $request, $id)
    {
		if(!Auth::user()->can('edit_contact_types')){ return view('unauthorised');} 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = ContactType::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Contact Type has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
		if(!Auth::user()->can('delete_contact_types')){ return view('unauthorised');} 
	   if( ContactType::findOrFail($id)->delete() ) {
            flash()->success('Contact Type has been deleted');
        } else {
            flash()->success('Contact Type not deleted');
        }

        return redirect()->back();
    }

 
}
