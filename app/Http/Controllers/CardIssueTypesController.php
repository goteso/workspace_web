<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\CardIssueTypes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class CardIssueTypesController extends Controller
{
    
    public function index()
    {
		if(!Auth::user()->can('view_card_issue_types')){ return view('unauthorised');} 
		$display_modal_name = 'Card Issue Types';
		$model_name = 'card_issue_types';
	    $modelName = "App\CardIssueTypes";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
        return view('card_issue_types.index', compact('result','display_modal_name','model_name'));
    }
	
    public function add_issue_type(Request $request)
    {
		if(!Auth::user()->can('add_card_issue_types')){ return view('unauthorised');} 
	  
	  
	  	if($request->file('image') !='')
	{
	  $file = $request->file('image');
      $file_name =$file->getClientOriginalName();
      $destinationPath = 'admin/issue_types/';
      $file->move($destinationPath,$file->getClientOriginalName());
	}
	else
	{
		$file_name = '';
	}
	
	
	 
	  
	  $ci = new CardIssueTypes;
      $ci->title = $request->title;
	  $ci->image = $file_name;
      $ci->save();
 
 
		//if(!Auth::user()->can('add_designations')){ return view('unauthorised');} 
         
        // Create the user
        if ($ci!='')  {
 
           flash('Issue Type has been created.');

        } else {
            flash()->error('Unable to create Issue Type.');
        }

        return back();
    }

	
	
    public function update(Request $request, $id)
    {
		 if(!Auth::user()->can('edit_card_issue_types')){ return view('unauthorised');} 
		 $this->validate($request, []);
        // Get the user
	  	if($request->file('image') !='')
	{
	  $file = $request->file('image');
      $file_name =$file->getClientOriginalName();
      $destinationPath = 'admin/issue_types/';
      $file->move($destinationPath,$file->getClientOriginalName());
	}
	else
	{
		$file_name = '';
	}
	  
	  
        $user = CardIssueTypes::findOrFail($id);
	    $ci->title = $request->title;
	    $ci->image = $file_name;
        $ci->save();
 
        flash()->success('Issue Type has been updated.');
        return back();
    }
 
    public function destroy($id)
    {
	   if(!Auth::user()->can('delete_card_issue_types')){ return view('unauthorised');} 
	   if( \App\CardIssueTypes::findOrFail($id)->delete() ) {
            flash()->success('Issue Type has been deleted');
        } else {
            flash()->success('Issue Type not deleted');
        }

        return redirect()->back();
    }

 
}
