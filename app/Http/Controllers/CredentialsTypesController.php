<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\CredentialsTypes;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class CredentialsTypesController extends Controller
{
    
  
    public function index()
    {
		if(!Auth::user()->can('view_credentials_types')){ return view('unauthorised');} 
		$display_modal_name = 'Credentials Types';
		$model_name = 'credentials_types';
	    $modelName = "App\CredentialsTypes";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
 
        return view('credentials_types.index', compact('result','display_modal_name','model_name'));
    }
	
 
 
    public function store(Request $request)
    {
		if(!Auth::user()->can('add_credentials_types')){ return view('unauthorised');} 
        $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = CredentialsTypes::create($request->toArray()))  {
 
           flash('Credentials Type has been created.');

        } else {
            flash()->error('Unable to create Credentials Type.');
        }

        return back();
    }

 
    public function update(Request $request, $id)
    {
		if(!Auth::user()->can('edit_credentials_types')){ return view('unauthorised');} 
		 $this->validate($request, [
            
        ]);

		// Get the user
        $user = CredentialsTypes::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Credentials Type has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
		
		 if(!Auth::user()->can('delete_credentials_types')){ return view('unauthorised');} 
	   if( \App\CredentialsTypes::findOrFail($id)->delete() ) {
            flash()->success('Credentials Type has been deleted');
        } else {
            flash()->success('Credentials Type not deleted');
        }

        return redirect()->back();
    }

 
}
