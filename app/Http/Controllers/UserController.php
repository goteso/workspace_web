<?php

namespace App\Http\Controllers;

use App\User;
use App\EmployeeDesignations;
use App\Role;
use \App\Board;
 
use App\Permission;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class UserController extends Controller
{
	
	    protected $board;
    protected $user;

    public function __construct(Board $board, User $user)
    {
        $this->board = $board;
        $this->user = $user;
    }
	
	
 //   use Authorizable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	 
	     public function getDashboard()
    {
		 
        $boards = $this->board->getUserBoards(Auth::id());
		$starredBoards = $this->board->getUserStarredBoards(Auth::id());
        return view('user.home', compact('boards', 'starredBoards'));
    }
	
	
	
    public function index()
    {
		if(!Auth::user()->can('view_users')){ return view('unauthorised');}  
	    $modelName = "App\User";  
        $model = new $modelName();
		$result = $model::latest()->with("EmployeeDesignations")->paginate(50);
		
		$modelName1 = "App\Designations";  
        $model1 = new $modelName1();
		$designations_array = $model1::latest()->get(["id","title"]);
		 
		 
		//return $result;
        return view('user.index', compact('result','designations_array'));
    }
	
 
	
	
	public function users_by_role($role)
    {	
        if(!Auth::user()->can('view_users')){ return view('unauthorised');}  	
		$modelName = "App\User";  
        $model = new $modelName();
		$result = $model::latest()->whereHas('roles', function($q) use ($role){$q->whereIn('name', [$role]);})->paginate(50);
		
		$modelName1 = "App\Designations";  
        $model1 = new $modelName1();
			$designations_array = $model1::latest()->get(["id","title"]);
        return view('user.index', compact('result','role','designations_array'));
    }
	
	
	
	
	public function downloadExcelByRole($role)
	{
		
		 
		if(!Auth::user()->can('export_user')){ return view('unauthorised');}  
		$data = User::whereHas('roles', function($q) use ($role){$q->whereIn('name', [$role]);})->get()->toArray();
		return Excel::create('goteso_users', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xls');
    }
	
	
	
		
	public function downloadExcelAll()
	{
		//if(!Auth::user()->can('export_user')){ return view('unauthorised');}  
		$data = User::get()->toArray();
		return Excel::create('goteso_users', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xls');
	}
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		if(!Auth::user()->can('add_users')){ return view('unauthorised');}  
        $roles = Role::pluck('name', 'id');

        return view('user.new', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
		
 
		if(!Auth::user()->can('add_users')){ return view('unauthorised');}  
		$designations_array = $request->designations_array;
        $this->validate($request, [
            'first_name' => 'bail|required|min:2',
			'last_name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users',
           // 'password' => 'required|min:6',
            'roles' => 'required|min:1'
        ]);

		
		 
        // hash password
        $request->merge(['password' => bcrypt($request->get('password'))]);

        // Create the user
        if ( $user = User::create($request->except('roles', 'permissions')) ) {
		
        //create employee designations		
		if(sizeof($designations_array) > 0)
		{
         foreach($designations_array as $emp)
		{
			  $count = \App\EmployeeDesignations::where('user_id',$user->id)->where('designation_id',$emp)->get();
			  if($count->count() < 1)
			  {
			        $model = new EmployeeDesignations();
                    $model->designation_id = $emp;
					$model->user_id = $user->id;
					$model->save();
			  }
		}
		}
		
		return $request."------".$user;

            $this->syncPermissions($request, $user);

            flash('User has been created.');

        } else {
            flash()->error('Unable to create user.');
        }

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		if(!Auth::user()->can('edit_users')){ return view('unauthorised');}  
        $user = User::find($id);
        $roles = Role::pluck('name', 'id');
        $permissions = Permission::all('name', 'id');
		
		$modelName1 = "App\Designations";  
        $model1 = new $modelName1();
		$designations_array = $model1::latest()->get(["id","title"]);

		   $employee_designation_id = @\App\EmployeeDesignations::where('user_id',$id)->get();

        return view('user.edit', compact('user', 'roles', 'permissions','designations_array','employee_designation_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		if(!Auth::user()->can('edit_users')){ return view('unauthorised');}  
		$designations_array = $request->designations_array;
        $this->validate($request, [
            'first_name' => 'bail|required|min:2',
			'last_name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required|min:1'
        ]);

        // Get the user
        $user = User::findOrFail($id);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password'));

        // check for password change
        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }
		
		
		

        // Handle the user roles
        $this->syncPermissions($request, $user);

        $user->save();
		
			   
          $project_emp = \App\EmployeeDesignations::where('user_id',$id)->delete();
			   if(sizeof($designations_array) > 0)
		{
         foreach($designations_array as $emp)
		{
			  $count = \App\EmployeeDesignations::where('user_id',$id)->where('designation_id',$emp)->get();
			  if($count->count() < 1)
			  {
			        $model = new EmployeeDesignations();
                    $model->user_id = $id;
					$model->designation_id = $emp;
					$model->save();
			  }
		}
		}
		
		

        flash()->success('User has been updated.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy($id)
    {
		if(!Auth::user()->can('delete_users')){ return view('unauthorised');}  
        if ( Auth::user()->id == $id ) {
            flash()->warning('Deletion of currently logged in user is not allowed :(')->important();
            return redirect()->back();
        }

        if( User::findOrFail($id)->delete() ) {
            flash()->success('User has been deleted');
        } else {
            flash()->success('User not deleted');
        }

        return redirect()->back();
    }

	
	public function get_auth_skills()
	{
		$designations_array = array();
		$employee_designation_id = @\App\EmployeeDesignations::where('user_id',Auth::user()->id)->get();
		foreach($employee_designation_id as $d)
		{
			  $designations_array[] = \App\Designations::where('id',$d->designation_id)->first(["title"])->title;
		}
		return $designations_array;
	}
    /**
     * Sync roles and permissions
     *
     * @param Request $request
     * @param $user
     * @return string
     */
    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	    public function show_edit_user_profile($id)
    {
       $user = \App\User::where('id',$id)->get()[0];
	 
	   return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
   public function update_edit_user_profile(Request $request)
    {
	   
	   
	  
	   
	   if($request->hasFile('image'))
	   {
		     $file = $request->file('image');
       //Move Uploaded File
       $destinationPath = 'admin/users/';
       $file->move($destinationPath,$file->getClientOriginalName());
	   $file_name = $destinationPath.$file->getClientOriginalName(); 
	    
	    $this->make_thumb(public_path().'/'.$file_name,public_path('/').'/admin/users/profile_'.$request->user_id.'.jpg','32');
	   $flight = \App\User::where('id',$request->user_id)->update(['photo' => $file_name,'photo_thumb'=>'admin/users/profile_'.$request->user_id.'.jpg' ]);
	   }
 
	
	  
    
     $user = \App\User::where('id',$request->user_id)->get()[0];
	 return view('user.show', compact('user'));
 
    }
		
	
	
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }
	
	
	
	
	
	
	
	
}
