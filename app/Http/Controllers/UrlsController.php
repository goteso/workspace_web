<?php
namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Permission;
use App\Urls;
use App\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;
use HttpRequest;
use Mbarwick83\Shorty\Facades\Shorty;

class UrlsController extends Controller
{
    
  
    public function index()
    {
		if(!Auth::user()->can('view_urls')){ return view('unauthorised');} 
		$display_modal_name = 'Urls List';
		$model_name = 'urls';
	    $modelName = "App\Urls";  
        $model = new $modelName();
		$result = $model::latest()->paginate(25);
 
        return view('urls.index', compact('result','display_modal_name','model_name'));
    }
	
 
 
    public function store(Request $request)
    {
		if(!Auth::user()->can('add_urls')){ return view('unauthorised');} 
     $full_url = $request->full_url;
     $request["short_url"]= Shorty::shorten($full_url);
	// $request["short_url"]= Shorty::stats($full_url);
 



		//if(!Auth::user()->can('add_designations')){ return view('unauthorised');} 
        $this->validate($request, [
          
        ]);

      // Create the user
        if ( $user = Urls::create($request->toArray()))  {
 
           flash('Url has been created.');

        } else {
            flash()->error('Unable to create Url.');
        }

        return back();
    }

	
	
	
	
	    public function get_analytics($urlId)
    {
		
		 if(!Auth::user()->can('view_analytics')){ return view('unauthorised');} 
        $url = Urls::findOrFail($urlId);
		$short_url = $url->short_url;
		$analytics= Shorty::stats($short_url);
		 

        return view('urls.analytics', compact('analytics'));
    }
 
	
	
	
 
    public function update(Request $request, $id)
    {
		
 
		if(!Auth::user()->can('edit_urls')){ return view('unauthorised');} 
		 $this->validate($request, [
            
        ]);

		
	 $full_url = $request->full_url;
     $request["short_url"]= Shorty::shorten($full_url);
		// Get the user
        $user = Urls::findOrFail($id);
        // Update user
        $user->fill($request->toArray());
 
        $user->save();

        flash()->success('Url Type has been updated.');

        return back();
    }
 
    public function destroy($id)
    {
		
		 if(!Auth::user()->can('delete_urls')){ return view('unauthorised');} 
	   if( \App\Urls::findOrFail($id)->delete() ) {
            flash()->success('Url has been deleted');
        } else {
            flash()->success('Url not deleted');
        }

        return redirect()->back();
    }

 
}
