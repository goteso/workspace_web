<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardIssueTypes extends Model
{
    protected $table = "card_issue_types";

    protected $fillable = [
        'image', 'title',
    ];
}
