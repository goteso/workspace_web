<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTrelloBoard extends Model
{
   protected $fillable = ['title','project_id','trello_board_id'];
 
 
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
	
}
