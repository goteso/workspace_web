<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectRealTimeBoard extends Model
{
  protected $fillable = ['title','project_id','realtime_board_id'];
 
 
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
	
}
