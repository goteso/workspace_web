<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Urls extends Model
{
  protected $fillable = ['title','full_url','short_url','url_type_id'];
}
