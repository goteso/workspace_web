<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentFeeTypes extends Model
{
	
	
    protected $fillable = ['payment_mode_id','title','fee_value'];

  public function credentials()
    {
        return $this->belongsTo(Credentials::class);
    }
}
