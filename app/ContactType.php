<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactType extends Model
{
	
	
    protected $fillable = ['title'];

  public function user()
    {
        return $this->belongsTo(User::class);
    }
}
