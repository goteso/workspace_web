<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	
	
    protected $fillable = ['role','linked_id','contact_type_id','contact_value','contact_title'];

 
}
