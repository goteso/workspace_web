<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\CardIssueTypes;
use Auth;
use App\Traits\slack; // <-- you'll need this line...


class BoardCardFiles extends Model
{
	use slack; // <-- ...and also this line.
	
	public function traits(){
	 return $this->slack();
 }
 
 
    protected $table = "board_card_files";

    protected $fillable = [
        'card_id','file_url','type',
    ];

     
}
