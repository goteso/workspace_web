<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
  protected $fillable = ['project_title','project_start_date','slack_channel_id','status','drive_admin_folder','drive_development_folder'];
 
 
    public function ProjectRealTimeBoard()
    {
        return $this->hasMany(ProjectRealTimeBoard::class);
    }
	
	
	public function ProjectTrelloBoard()
    {
        return $this->hasMany(ProjectTrelloBoard::class);
    }
	
		public function Credentials()
    {
        return $this->hasMany(Credentials::class);
    }
	
	
			public function Notes()
    {
        return $this->hasMany(Notes::class , 'linked_id','id');
    }
	
		    public function project_employees()
    {
        return $this->hasMany(ProjectEmployees::class);
    }
	
}
