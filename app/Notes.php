<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
   protected $fillable = ['notes_type','notes','linked_id','pinned','creator_id'];
   
       public function notes()
    {
        return $this->belongsTo(Notes::class);
    }
	
}
