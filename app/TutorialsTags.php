<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TutorialsTags extends Model
{
	
	
    protected $fillable = ['tutorial_id','tag_id'];

     public function tutorials()
    {
        return $this->belongsTo(Tutorials::class,'id','tutorial_id');
    }
	
	
	    public function tags()
    {
        return $this->belongsTo(Tags::class,'id','tag_id');
    }
}
