@can('edit_users')
    <a href="{{ route($entity.'.edit', [str_singular($entity) => $id])  }}" title="Edit" class="action-edit" >
        <i class="fa fa-edit"></i> </a>
@endcan

@can('delete_users')
    {!! Form::open( ['method' => 'delete', 'url' => route($entity.'.destroy', ['user' => $id]), 'style' => 'display: inline', 'onSubmit' => 'return confirm("Are yous sure wanted to delete it?")']) !!}
        <button type="submit" class="action-delete"  title="Delete">
            <i class="fa fa-trash"></i>
        </button>
    {!! Form::close() !!}
@endcan