<div class="panel panel-default"  >
    <div class="panel-heading" role="tab" id="{{ isset($title) ? str_slug($title) :  'permissionHeading' }}">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#dd-{{ isset($title) ? str_slug($title) :  'permissionHeading' }}" aria-expanded="{{ $closed or 'true' }}" aria-controls="dd-{{ isset($title) ? str_slug($title) :  'permissionHeading' }}">
             {!! isset($user) ? '<span class="text-danger">(' . $user->getDirectPermissions()->count() . ')</span>' : '' !!}
            </a>
        </h4>
    </div>
    <div id="dd-{{ isset($title) ? str_slug($title) :  'permissionHeading' }}" class="panel-collapse collapse {{ $closed or 'in' }}" role="tabpanel" aria-labelledby="dd-{{ isset($title) ? str_slug($title) :  'permissionHeading' }}">
        <div class="panel-body">
            <div class="row">
                @foreach($permissions as $perm)
				
			      <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 text-left">
			         <h4 style="color:#5CB75C">{{ title_case($perm[0]->category) }} </h4>
			      </div>
<div class="row">				  
				 @foreach($perm as $perm_element)
				         <?php
                        $per_found = null;

					 
                        if( isset($role) ) {
                            $per_found = $role->hasPermissionTo($perm_element->name);
                        }

                        if( isset($user)) {
                            $per_found = $user->hasDirectPermission($perm_element->name);
                        }   
                    ?>
				        <div class="col-md-3">
                        <div class="checkbox">
                            <label class="{{ str_contains($perm_element->name, 'delete') ? 'text-danger' : '' }}">
								{!! Form::checkbox("permissions[]", $perm_element->name, $per_found, isset($options) ? $options : []) !!} {{ $perm_element->name }}
                            </label>
                        </div>
                    </div>
					
				  @endforeach
				  </div>
				  <hr>
				  
				  
				 
            

             
                @endforeach
            </div>
        </div>
    </div>
</div>