
<?php
   use App\Role;
   ?>
@extends('layout.auth')
@section('title', 'Contact Types' )
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
			   
                  <div class="col-sm-12">
				 

 
					    @include('flash::message')
				 
				  
				  
				  <div class="tab-content">
 
   <div class="row">
     <div class="col-sm-7 header-left">
       <h3 class="text-left">Contact Types</h3>
	  </div>
	    <div class="col-sm-5 header-right">
		
           @can('add_contact_types')
                     <button type="button" class="btn btn-trans"  data-toggle="modal" data-target="#add">Add New</button>
                     @endcan

     </div>
    </div>
	
			<div class="row">
     <div class="col-sm-12">
	   <div class="panel">	  
                    
                     <div class="tab-content">
                        <table class="table table-bordered table-striped table-hover" id="printTable">
                           <thead>
                              <tr>
                                 <th>Sr</th>
                                 <th>Title</th>
                                 <th>Created At</th>
                                 @can('edit_contact_types', 'delete_contact_types')
                                 <th class="text-center">Actions</th>
                                 @endcan
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;?>
                              @foreach($result as $item)
                              <?php $x++;?>
                              <tr>
                                 <td>{{ $x }}</td>
                                 <td>{{ $item['title'] }}</td>
                                 <td>{{ $item['created_at'] }}</td>
                                 @can('edit_contact_types')
                                 <td class="text-center">
                                    @include($model_name.'._actions_with_modals', [
                                    'entity' => $model_name,
                                    'id' => $item['id']
                                    ])
                                 </td>
							
                                 @endcan
                              </tr>
                              <div id="edit{{$item['id']}}" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                                 <div class="modal-dialog"  >
                                    <!-- Modal content-->
                                    <div class="modal-content"  >
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Update {{ $display_modal_name }}</h4>
                                       </div>
                                       <div class="modal-body "  >
                                          <div class="row">
                                             <div class="col-lg-10">
                                                {!! Form::model($item['id'], ['method' => 'PUT', 'route' => [$model_name.'.update',  $item['id'] ] ]) !!}
                                                @include($model_name.'._form')
                                                <!-- Submit Form Button -->
                                                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                                {!! Form::close() !!}
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <script>
                                 function close_inner<?php echo $item['id']; ?>()
                                 {
                                 	$('#edit<?php echo $item['id']; ?>').modal('hide');
                                 }
                                 
                              </script>
							  	 <?php $item=null;?>
                              <!-------inner model ends-------->
                              @endforeach
                           </tbody>
                        </table>
						  <div class="text-right">
                                 {{ $result->links() }}
                           </div>
                     </div>
                  </div>
               </div>
			    </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<?php
   $roles = Role::pluck('name', 'id');
   ?>
<!--Add New Model Starts--> 
<style>
   .modal-dialog {
   width: 100%;
   height: 100%;
   margin: 0;
   padding: 0;
   }
   .modal-content {
   height: auto;
   min-height: 100%;
   border-radius: 0;
   }
</style>
<!-- Modal -->
<!------------- Add New User ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Add New {{ $display_modal_name }}</h4>
         </div>
         <div class="modal-body "  >
          <br>
            <div class="row">
               <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
                  {!! Form::open(['route' => [ $model_name.'.store'] ]) !!}
                  @include($model_name.'._form')
                  <!-- Submit Form Button -->
                  {!! Form::submit('Create', ['class' => 'btn btn-create']) !!}
                  {!! Form::close() !!}
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>
@endsection