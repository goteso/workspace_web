<!-- First Name Form Input -->
 

<div class="form-group @if ($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Role Name') !!}
    {!! Form::text('name', @$name, ['class' => 'form-control ', 'placeholder' => 'Role Name' ] ) !!}
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
</div>


  
    {!! Form::hidden('project_id', @$item['id'], [] ) !!}
 
 