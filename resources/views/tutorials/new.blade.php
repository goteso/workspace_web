@extends('layout.auth')

@section('title', 'Add Tutorial')

@section('content')
   @section('header')
         @include('includes.header')
      @show
	   
         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    @section('sidebar')
         @include('includes.sidebar')
      @show

     <div class="static-content-wrapper">
						
						<section id="main-header">	
						
 <div class="container-fluid">
 
<!--- form section  starts here-->
 <div class="row">
            <div class="col-xs-12 " >
			 
			 
			  <div class="row">
        <div class="col-md-12">
            <h3 class="edit-title">Add <span>Tutorials</span> </h3> 
        </div>
         
    </div>
	
	
	<div class="row">
            <div class="col-xs-12 " > 
			
			<div class="tab-content panel">
				    @include('flash::message')
              <div class="box ">
             
                @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif

<br><br> 
<div class="row">
 
 
 {!! Form::open(['route' => ['tutorials.store'] ]) !!}
 <div class="col-md-10 col-md-offset-1">
  <div class="row"> 
		<div class="form-group @if ($errors->has('title')) has-error @endif">
    <div class="col-sm-3 col-md-2 "  > {!! Form::label('title', 'Tutorial Title') !!}</div>
     <div class="col-sm-9 col-md-10"> {!! Form::text('title', @$item['title'], ['class' => 'form-control '  ] ) !!}
    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
	</div>
     </div>
</div>
</div>
<br class="hidden-xs"><br class="hidden-xs"><br class="hidden-xs  hidden-sm">
 <div class="col-md-10 col-md-offset-1">
 <div class="row"> 

		<div class="form-group @if ($errors->has('title')) has-error @endif">
   <div class="col-sm-3 col-md-2"  >  {!! Form::label('title', 'Skills') !!}</div>
    <div class="col-sm-9 col-md-10"  >
	
	
	@foreach($employee_designation_id as $d)
	<?php $title = @\App\Designations::where('id',$d->designation_id)->first(["title"])->title; ?>
	 
	  
		<div class="radio-inline">			 
	 <input type="radio" name="designation_id" value="<?php echo $d->designation_id;?>"  >
	{!! Form::label('title', $title) !!}
	</div>
	@endforeach
   </div>
   </div>
</div>
</div>

<link rel="stylesheet" href="http://twitter.github.io/typeahead.js/css/examples.css">
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
     <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<br class="hidden-xs"><br class="hidden-xs  hidden-sm">
 <div class="col-md-10 col-md-offset-1 "  >
 <div class="row"> 
 <div class="col-sm-3 col-md-2" style="padding-top:8px;" >{!! Form::label('title', 'Tags') !!}</div>
 <div class="col-sm-9 col-md-10 "  >
 <input type="text" class="tm-input" value="" data-role="tagsinput" name="tags">
				 
	    <!-- <input class="typeahead tm-input tm-input-info form-control" type="text" placeholder=" Search Text"  id="the-basics" name="tags"  >-->
		     @if ($errors->has('tags')) <p class="help-block">{{ $errors->first('tags') }}</p> @endif
 </div>
</div>
</div>
<script> 
var citynames = new Bloodhound({
 datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
 queryTokenizer: Bloodhound.tokenizers.whitespace,
 prefetch: {
   url: '{{ URL::to("get_tags") }}',
   filter: function(list) {
     return $.map(list, function(cityname) {
       return { name: cityname }; });
   }
 }
});
citynames.initialize();

$('.tm-input').tagsinput({
 typeaheadjs: {
 hint: false,
   name: 'citynames',
   displayKey: 'name',
   valueKey: 'name',
   source: citynames.ttAdapter()
 }
});
</script>
  
 
 
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({
  selector: "textarea",
  theme: "modern",
 plugins: "codesample,code,autolink,autoresize",
  toolbar: "codesample,code" // change this value according to your HTML
  
 
});</script>

<br class="hidden-xs"><br class="hidden-xs  hidden-sm"><br class="hidden-xs hidden-sm">
<div class="col-md-10 col-md-offset-1"  >
            <div class="panel panel-default" >

                <div class="panel-heading">Tutorial Content</div>

                <div class="panel-body" >
    
                    <textarea class="description" rows="10" name="content"></textarea>
         <div class="box-footer">
                  
                  </div>

                </div>

            </div>
			 <button type="submit" class="btn btn-create">Submit</button>
</div>

</div>
  {!! Form::close() !!}
               </div><!-- /.box -->
             </div><!-- /.col -->
           
		  
		  
		   </div><!-- /.col -->
          </div>
		    </div>
		    </div>
			</section>
    </div>
<!---- form ends here--->		  
	
	       </div>
    </div>
 

	  
	  
@endsection