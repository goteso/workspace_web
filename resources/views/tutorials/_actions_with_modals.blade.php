@can('edit_users')
    <a    data-toggle="modal" data-target="#edit<?php echo $id;?>"  style="background:transparent;border:none;outline:none;color:grey">
        <i class="fa fa-edit"></i> </a>
		
		 
@endcan

@can('delete_users')
    {!! Form::open( ['method' => 'delete', 'url' => route($entity.'.destroy', ['user' => $item['id']]), 'style' => 'display: inline', 'onSubmit' => 'return confirm("Are you sure want to delete it?")']) !!}
        <button type="submit"   style="background:transparent;border:none;outline:none;color:grey">
            <i class="fa fa-trash"></i>
        </button>
    {!! Form::close() !!}
@endcan