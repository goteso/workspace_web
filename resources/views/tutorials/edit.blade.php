@extends('layout.auth')

@section('title', 'Edit Tutorial')

@section('content')
   @section('header')
         @include('includes.header')
      @show
	  

         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    @section('sidebar')
         @include('includes.sidebar')
      @show

 
 <div class="static-content-wrapper">
						
						<section id="main-header">	
						
 <div class="container-fluid">
 
<!--- form section  starts here-->
 <div class="row">
            <div class="col-xs-12 " >
			 	    @include('flash::message')
			 
			  <div class="row">
        <div class="col-md-12">
            <h3 class="edit-title">Edit <span>Tutorials</span> </h3> 
        </div>
         
    </div>
	
	
	<div class="row">
            <div class="col-xs-12 " > 
			
			<div class="tab-content panel">
              <div class="box">
             
                @if(Session::has('message'))
                <p class="alert alert-success">{{ Session::get('message') }}</p>
               @endif


<div class="row">
  
 
 {!! Form::model($result, ['method' => 'PUT', 'route' => ['tutorials.update',  $result[0]["id"] ] ]) !!}
 
 <div class="col-md-10 col-md-offset-1">
		<div class="form-group @if ($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Tutorial Title') !!}
    {!! Form::text('title', $result[0]['title'], ['class' => 'form-control '  ] ) !!}
    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
     </div>
</div>


 <div class="col-md-10 col-md-offset-1">
		<div class="form-group @if ($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Skills') !!}<br>
	
	
	@foreach($employee_designation_id as $d)
	<?php $ds =''; $title = @\App\Designations::where('id',$d->designation_id)->first(["title"])->title; 
	if($result[0]["designation_id"] == $d->designation_id) { $ds = 'checked="checked"';} else { $ds = '';}  
	  
	?>
	 <div class="radio-inline">	
	 <input type="radio" name="designation_id" value="<?php echo $d->designation_id;?>" <?php echo $ds;?> >
 
	{!! Form::label('title', $title) !!}
	</div>
	@endforeach
   </div>
</div>







<link rel="stylesheet" href="http://twitter.github.io/typeahead.js/css/examples.css">
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
     <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


 <div class="col-md-10 col-md-offset-1 "  >
 
 <input type="text" class="tm-input" value="<?php echo implode(',', $added_tags_array);?>" data-role="tagsinput" name="tags">
				 
	    <!-- <input class="typeahead tm-input tm-input-info form-control" type="text" placeholder=" Search Text"  id="the-basics" name="tags"  >-->
		     @if ($errors->has('tags')) <p class="help-block">{{ $errors->first('tags') }}</p> @endif
 </div>

<script> 
var citynames = new Bloodhound({
 datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
 queryTokenizer: Bloodhound.tokenizers.whitespace,
 prefetch: {
   url: '{{ URL::to("get_tags") }}',
   filter: function(list) {
     return $.map(list, function(cityname) {
       return { name: cityname }; });
   }
 }
});
citynames.initialize();

$('.tm-input').tagsinput({
 typeaheadjs: {
 hint: false,
   name: 'citynames',
   displayKey: 'name',
   valueKey: 'name',
   source: citynames.ttAdapter()
 }
});
</script>
  
 
 
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({
  selector: "textarea",
  theme: "modern",
 plugins: "codesample,code,autolink,autoresize",
  toolbar: "codesample,code" // change this value according to your HTML
  
 
});</script>
<div class="col-md-10 col-md-offset-1"  >
            <div class="panel panel-default" >

                <div class="panel-heading">Tutorial Content</div>

                <div class="panel-body" >
    
                    <textarea class="description" rows="10" name="content"><?php echo $result[0]['content'];?></textarea>
         <div class="box-footer">
                  <br>
                    <button type="submit" class="btn btn-save">Submit</button>
                 
                  </div>

                </div>

            </div>
</div>


  {!! Form::close() !!}
  </div>
               </div><!-- /.box -->
             </div><!-- /.col -->
          </div><!-- /.row -->
		  
<!---- form ends here--->		  
	
	       </div>
    </div>
 </div>
    </div>
	</section>
	
	</div>

	  </div>
	  </div>
	  
@endsection