<?php
   use App\Role;
   ?>
@extends('layout.auth')
@section('title', 'Tutorials')
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->	
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    @include('flash::message')
                    
                     <div class="tab-content">
                        <div class="row">
                           <div class="col-sm-7 header-left">
                              <h3 class="text-left">Tutorials List</h3>
                           </div>
                           <div class="col-sm-5 header-right">
                              <div class="dropdown">
                            
                                 <a href="{{ route('add_tutorials') }}"><button type="button" class="btn btn-trans"   >Add New</button></a>
                                  
                              </div>
							  
						 
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="panel">
                                 <div class="row">
                                    <div class="col-sm-7 ">
                                    </div>
                                    <div class="col-sm-5  actions-right">
                                       <a><i class="fa fa-print" title="Print" class="btn" type="button" ></i></a>
                                 
                                    </div>
                                 </div>
                                 <script>
                                    function printData()
                                    {
                                      var divToPrint=document.getElementById("result-set");
                                      newWin= window.open("");
                                      newWin.document.write(divToPrint.outerHTML);
                                      newWin.print();
                                      newWin.close();
                                    }
                                    
                                    $('#print_btn').on('click',function(){
                                    printData();
                                    })
                                    		 
                                    		
                                    		
                                 </script>
                                 <div class="row">
                                    <div class="col-md-5">
                                       <h3 class="modal-title">{{ $result->total() }} {{ str_plural('Tutorials', $result->count()) }} </h3>
                                    </div>
                                    <div class="col-md-7 page-action text-right">
                                    </div>
                                 </div>
                                 <div class="result-set" id="result-set" >
                                    <table class="table table-bordered table-striped table-hover table-responsive" id="printTable">
                                       <thead>
                                          <tr>
                                             <th>Sr</th>
                                             <th>Title</th>
                                             <th>Skills</th>
                                             <th>Created At</th>
                                           
                                             <th class="text-center">Actions</th>
                                          
                                          </tr>
                                       </thead>
                                       <?php $x=0;?>
                                       @foreach($result as $item)
                                       <?php $x++;
                                          $employee_designation_id = @\App\EmployeeDesignations::where('user_id',$item->id)->get();
                                          ?>
                                       <tr>
                                          <td>{{ $x }}</td>
                                          <td>{{ $item->title }}</td>
                                       
                                      
                                          <td>
                                          
                                             <?php $title = @\App\Designations::where('id',$item->designation_id)->first(["title"])->title; ?>
                                             {{ $title }}<br>
                                             
                                          </td>
                                         
                                          <td>{{ $item->created_at->toFormattedDateString() }}</td>
                                         
                                          <td class="text-center">
                                             @include('shared._actions', [
                                             'entity' => 'tutorials',
                                             'id' => $item->id
                                             ])
                                          </td>
                                          
                                       </tr>
                                       @endforeach
                                       </tbody>
                                    </table>
                                    <div class="text-right">
                                       {{ $result->links() }}
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
 
 
 
@endsection