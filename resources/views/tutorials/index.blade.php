<?php
   use App\Role;
   ?>
@extends('layout.auth')
@section('title', 'Tutorials' )
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
                  	    @include('flash::message')
                   
                     <link rel="stylesheet" href="http://twitter.github.io/typeahead.js/css/examples.css">
                     <link rel="stylesheet" href="{{ URL::asset('admin/tag/bootstrap-tagsinput.css')}}">
                     <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
                     <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
                     <div class="text-right">
                        @can('add_users')
                        <a type="button" class="btn btn-success text-right"  href="{{ route('add_tutorials') }}">Add New</a>
                        <a type="button" class="btn btn-trans"  href="{{ route('get_tutorials_list') }}">My Tutorials</a>
                        @endcan
                     </div>
					 </div>
                     <!----- main container starts----->
                     <div class="tab-content">
                        <!----- side tutorials menu starts----->
                        <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12" style="border:1px solid g">
						<div class="panel">
						<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
						   {!! Form::label('title', 'Category') !!}<br>
							<input type="text" class="skill-input form-control" value="" data-role="tagsinput" name="skills" id="skills" style="width:100%"  onchange="search()"  >
                           <script> 
                              var skills = new Bloodhound({
                               datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                               queryTokenizer: Bloodhound.tokenizers.whitespace,
                               prefetch: {
                                 url: '{{ URL::to("get_auth_skills") }}',
                                 filter: function(list) {
                                   return $.map(list, function(cityname) {
                                     return { name: cityname }; });
                                 }
                               }
                              });
                              skills.initialize();
                              
                              $('.skill-input').tagsinput({
                               typeaheadjs: {
                               hint: false,
                                 name: 'skills',
                                 displayKey: 'name',
                                 valueKey: 'name',
                                 source: skills.ttAdapter()
                               }
                              });
                           </script>
						   </div>
						   <hr>
						   
						   <style>.tag{border-top:1px solid #f5f5f5;border-bottom:1px solid #f5f5f5;.form-control:border:0px;}</style>
						   <div class="col-lg-12 col-md-12 col-sm-12  tag">
						   {!! Form::label('title', 'Tags') !!}<br>
                           <input type="text" class="tm-input form" value="" data-role="tagsinput" name="tags" id="tags" onchange="search()"   >
                           <script> 
                              var citynames = new Bloodhound({
                               datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                               queryTokenizer: Bloodhound.tokenizers.whitespace,
                               prefetch: {
                                 url: '{{ URL::to("get_tags") }}',
								 // url: 'https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/assets/citynames.json',
                                 filter: function(list) {
                                   return $.map(list, function(cityname) {
                                     return { name: cityname }; });
                                 }
                               }
                              });
                              citynames.initialize();
                              
                              $('.tm-input').tagsinput({
								    freeInput:false,
								   allowDuplicates:false,
                               typeaheadjs: { 
							     hint: false,
                                 name: 'citynames',
                                 displayKey: 'name',
                                 valueKey: 'name',
                                 source: citynames.ttAdapter()
                               }
							  
                              });
                           </script>
						   </div>
						   <input type="hidden" value="" name="abc" id="abc">
						   
						    <form class="form-horizontal"   method="POST" action="{{ route('ajax_test') }}">
                                               {{ csrf_field() }}
											    </form>

                                                    <input type="hidden"   value="{{ Session::token() }}" name="_token" >
						   <script>
						   function abc()
						   {
							 //  alert('hi');
						   }
						   </script>
						 <br>
				          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top:2%;border-bottom:1px solid #f5f5f5;">
                           <input type="input" class="form-control input-lg" id="txt-search" placeholder="Search Here..." onkeyup="search()" style="border:0px; box-shadow:inset 0 0px 0px rgba(0,0,0,.075);">
						     </div>
							   </div>
                           <div id="filter-records"> 
						   </div>
                        </div>
                        <!----- side tutorials menu ends------->
					  </div>
					  
                        <!----- tutorial body starts----->
                        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12"   >
						<div class=" row panel">
						    <!-- content title starts--->
                            <div class="col-md-12 col-sm-12 col-lg-12" >
							    <h2 id="content_title" class="text-center"></h2><br>
								<ul class="list-inline text-center">
								<li id="content_designation_title" class="text-center" style="padding:0;margin-top:0"></li>
								<li> <span id="calendaricon"></span> <span id="content_created_at" class="text-center" style="padding:0;margin-top:0"></span></li>
								<li> <span id="usericon"></span> <span id="content_author_name" class="text-center" style="padding:0;margin-top:0"></span><li>
								</ul>
								<span id="orangeline"></span>
								<div class="col-md-12 col-sm-12 col-lg-12" id="content">
								</div>
							</div>
							<!----- content title ends---->
						  </div>
                        </div>
                        <!----- tutorial body ends------->
                     </div>
                     <!----- main container ends here----->
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<?php
   $roles = Role::pluck('name', 'id');
   ?>
<!--Add New Model Starts--> 
<style>
</style>
<!-- Modal -->
<!------------- Add New User ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog "  >
      <!-- Modal content-->
      <div class="modal-content "  >
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New {{ $display_modal_name }}</h4>
         </div>
         <div class="modal-body "  >
            <div class="row">
               <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  {!! Form::open(['route' => [ $model_name.'.store'] ]) !!}
                  @include($model_name.'._form')
                  <!-- Submit Form Button -->
                  {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                  {!! Form::close() !!}
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<style>
h5 a { color:black;text-decoration:none;cursor:pointer}

</style>
<!------>
<!------------------------- Tutorials function starts---------------------------->
<script>
   var data =  <?php echo json_encode($result);?>
;
    



   function show_onload(data){
		
       
										 
					         var output = '<div class="row">';
      var i=0;
	               $.each(data, function(key, val){
					   i++;
					   if(i==1) { get_tutorial_data(val["id"]); }
                
				 
				          var title = val["title"];
						  var id = val["id"];
						  
					 
						                                 output += '<div class="col-md-12">';
                                                          output += '<h5>'+i+'. <a onclick="get_tutorial_data('+id+')">'+title+'</a></h5>';
                                                          output += '</div>';	
						   });
                             output += '</div>';
                             $('#filter-records').html(output);
							 
							 
	}


show_onload(data);

	
 
		
		
   function search(){
		
                    var searchField = $('#txt-search').val();
                    var regex = new RegExp(searchField, "i");
                    var output = '<div class="row">';
                    var count = 1;
					
					var tagsField = $('#tags').val();
                    var regex_tags = new RegExp(tagsField, "i");
                    var tags_field_array = tagsField.split(",");
					var tags_field_array_length = tags_field_array.length;
										
					var skillsField = $('#skills').val();
                    var regex_skills = new RegExp(skillsField, "i");
                    var skills_field_array = skillsField.split(",");
					var skills_field_array_length = skills_field_array.length;
										 
					 
      var i=0;
	               $.each(data, function(key, val){
					     i++;
                 
				          var title = val["title"];
						  
						  
						  
						//tag input starts---------
                        var tags_array = val["tags_array"];						
						if(tags_array != '')
						{
						
						var tags_array_length = tags_array.length;
					    var exist = 0; 
                        for(var x=0;x<tags_field_array_length;x++)
						{
							var field_tag = tags_field_array[x];
							 var field_tag_index = tags_array.indexOf(field_tag);
							 
							 if(field_tag_index != -1)
						  {
                                       exist = 1;
								 
                          }
						  else
						  {
							  exist = 0;
							 
                            break;
				          }

						}
						}
						else
						{
							var exist =0;
						}
						//tag input ends---------
						
						
						
					 
							//skills input starts---------
                        var designation_title = val["designation_title"];
                        var designation_id = val["designation_id"];						
						
						if(skillsField != '')
						{
						    var skills_exist = 0; 
                            var skills_exist_index = skills_field_array.indexOf(designation_title);
							 
							if(skills_exist_index != -1)
						  {
							  
                                       skills_exist = 1;
								 
                          }
						  else
						  {
							  skills_exist = 0;
							  
							  
				          }
                        }
						else
						{
							 
							var skills_exist =0;
						}
						//tag input ends---------
						
						
						
						  

                           
						  if(skillsField != '')
						  {
						    if (((title.search(regex) != -1 && searchField !='') || exist == '1') && skills_exist =='1' ) 
							 {
								                        output += '<div class="col-md-12">';
                                                          output += '<h5>'+i+'. <a onclick="get_tutorial_data('+val["id"]+')">'+val.title+'</a></h5>';
                                                          output += '</div>';	

                                                     if(count%2 == 0)
													     {
                                                           output += '</div><div class="row">'
                                                         }
                                                     count++;														  
							 }
						  }
						  else
						  {
							      if ((title.search(regex) != -1 && searchField !='') || exist == '1' ) 
							 {
								                        output += '<div class="col-md-12">';
                                                          output += '<h5>'+i+'. <a onclick="get_tutorial_data('+val["id"]+')">'+val.title+'</a></h5>';
                                                          output += '</div>';	

                                                     if(count%2 == 0)
													     {
                                                           output += '</div><div class="row">'
                                                         }
                                                     count++;														  
							 }
						  }
							 
						 
				 
		 					 
													 
													 
													 
                                                 });
                             output += '</div>';
                             $('#filter-records').html(output);
	}
   
   
   
 

function get_tutorial_data(id)
{
	
 
//event.preventDefault();
 
$.ajax({

type:"POST",
url:"{{ route('get_tutorials') }}",
data:{model:$("#abc").val(),_token:"{{ Session::token() }}",id:id},
 success:function(data){
                 
				 
			 
			 
                    var content =data[0]['content'];
					var title =data[0]['title'];
					var created_at =data[0]['created_at'];
					var designation_title =data[0]['designation_title'];
					var author_name =data[0]['author_name'];
					
					 $("#content").html(content);
					 $("#content_title").html(title);
					 $("#content_created_at").html(data[0]['t']);
					 $("#content_designation_title").html(designation_title);
					 $("#content_author_name").html(author_name);
					 
					 $("#calendaricon").html('<i class="fa fa-calendar"></i>');
					  $("#usericon").html('<i class="fa fa-user"></i>');
					  $("#orangeline").html('<hr style="width:10%;border:2px solid orange; ">');
					 
               

               }

});
}

 
  
   
</script>
<!------------------------- Tutorials Functions ends here ----------------------->
@endsection