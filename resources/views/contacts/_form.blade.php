<!-- First Name Form Input -->
 

<div class="form-group @if ($errors->has('role')) has-error @endif">
    {!! Form::label('role', 'Role') !!}
    {!! Form::text('role', @$role, ['class' => 'form-control', 'placeholder' => 'Role' , 'readonly'=>'readonly'] ) !!}
    @if ($errors->has('role')) <p class="help-block">{{ $errors->first('role') }}</p> @endif
</div>



<div class="form-group @if ($errors->has('linked_id')) has-error @endif">
 
    {!! Form::hidden('linked_id', $linked_id, ['class' => 'form-control', 'placeholder' => 'Role'    ] ) !!}
    @if ($errors->has('linked_id')) <p class="help-block">{{ $errors->first('linked_id') }}</p> @endif
</div>


<div class="form-group @if ($errors->has('last_name')) has-error @endif">
{!! Form::label('contact_type_id', 'Contact Type') !!}
<?php $contact_types = \App\ContactType::get();?>
<select class="form-control" name="contact_type_id" id="contact_type_id" >
        @foreach($contact_types as $cat)
        <option value="{{$cat->id}}" >{{ $cat->title }}</option>
        @endforeach
      </select>
</div>



<!-- Contact Value Form Input -->
<div class="form-group @if ($errors->has('contact_value')) has-error @endif">
    {!! Form::label('contact_value', 'Contact Value') !!}
    {!! Form::text('contact_value', null, ['class' => 'form-control', 'placeholder' => 'Contact Value']) !!}
    @if ($errors->has('contact_value')) <p class="help-block">{{ $errors->first('contact_value') }}</p> @endif
</div>


<!-- Contact Value Form Input -->
<div class="form-group @if ($errors->has('contact_title')) has-error @endif">
    {!! Form::label('contact_title', 'Contact Title (optional)') !!}
    {!! Form::text('contact_title', null, ['class' => 'form-control', 'placeholder' => 'Contact Title']) !!}
    @if ($errors->has('contact_title')) <p class="help-block">{{ $errors->first('contact_title') }}</p> @endif
</div>

 
 

<!-- Permissions -->
@if(isset($user))
    @include('shared._permissions', ['closed' => 'true', 'model' => $user ])
@endif