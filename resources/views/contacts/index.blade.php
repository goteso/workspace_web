<?php
use App\Role;?>

    @extends('layout.auth') @section('title', 'Contacts') @section('content') @section('header') @include('includes.header') @show
    <div id="wrapper">
        <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->
            @section('sidebar') @include('includes.sidebar') @show
            <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
            <div class="static-content-wrapper">

                <section id="main-header">
                    <div class="container-fluid">
                        <div class="row">
						
						<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					     <h2> <u><?php echo \App\User::where('id',$linked_id)->first(["first_name"])->first_name." ".\App\User::where('id',$linked_id)->first(["last_name"])->last_name;?></u></h2>
					 </div>
					 
					 
                            <div class="col-sm-12">
	    @include('flash::message')
                                <div class="tab-content">

                                    <div class="row">
                                        <div class="col-sm-7 header-left">
                                            <h3 class="text-left">Contacts List</h3>
                                        </div>
                                        <div class="col-sm-5 header-right">
                                            <div class="dropdown">
                                                <!-- <button class="btn btn-trans dropdown-toggle" type="button" data-toggle="dropdown">New Transaction
                                                <span class="fa fa-caret-down"></span></button>-->
                                                @can('add_contact')
                                                <button type="button" class="btn btn-trans" data-toggle="modal" data-target="#myModal">Add New</button>
                                                @endcan

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel">

                                                <div class="row">

                                                    <div class="col-sm-7 ">

                                                    </div>

                                                    <div class="col-sm-7 ">

                                                    </div>

                                                    <div class="col-sm-5  actions-right">
                                                        <a><i class="fa fa-print" title="Print" class="btn" type="button" ></i></a> @can('export_contact')
                                                    <!--    <a href="{{ URL::to('downloadExcel/'.@$role.'/xls') }}"><i class="fa fa-share-square-o" title="Export Excel"  ></i></a>@endcan-->
                                                         
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <h3 class="modal-title">{{ $result->total() }} {{ str_plural('Contacts', $result->count()) }} </h3>
                                                    </div>
                                                    <div class="col-md-7 page-action text-right">

                                                    </div>
                                                </div>

                                                <div class="result-set" id="result-set">
                                                    <table class="table table-bordered table-striped table-hover" id="printTable">
                                                        <thead>
                                                            <tr>
                                                                <th>Id</th>
                                                                <th>User Role</th>
                                                                <th>User</th>
                                                                <th>Contact Type</th>
                                                                <th>Contact Value</th>
                                                                <th>Contact Title</th>
                                                                <th>Created At</th>
                                                                @can('edit_contact', 'delete_contact')
                                                                <th class="text-center">Actions</th>
                                                                @endcan
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($result as $item)
                                                            <tr>
                                                                <td>{{ $item->id }}</td>
                                                                <td>{{ $item->role }}</td>

                                                                <?php $user_first_name= \App\User::where('id',$item->linked_id)->first(["first_name"])->first_name;?>
                                                                    <td>{{ $user_first_name }}</td>
                                                                    <?php $contact_type_title= \App\ContactType::where('id',$item->contact_type_id)->first(["title"])->title;?>
                                                                        <td>{{ $contact_type_title }}</td>

                                                                        <td>{{ $item->contact_value }} </td>
                                                                        <td>{{ $item->contact_title }}</td>
                                                                        <td>{{ $item->created_at->toFormattedDateString() }}</td>

                                                                        @can('edit_contact')
                                                                        <td class="text-center">
                                                                            @include('shared._actions', [ 'entity' => 'contacts', 'id' => $item->id ])
                                                                        </td>
                                                                        @endcan
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>

                                                    <div class="text-right">
                                                        {{ $result->links() }}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <!------------- Add New User ----->
    <div id="myModal" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Contact</h4>
                </div>
                <div class="modal-body ">

                    <div class="row">
                        <div class="col-lg-10">
                            <p>Enter Below from to Add New Contact.</p>
                            <h3>Create</h3>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-10">
                            {!! Form::open(['route' => ['contacts.store'] ]) !!} @include('contacts._form')
                            <!-- Submit Form Button -->
                            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!} {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!------>

    @endsection