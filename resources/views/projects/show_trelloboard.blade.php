@extends('layout.auth')
@section('title', 'Trello Boards')
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <?php $trelloboard = $result[0]['project_trello_board']; $x1=0;$x2=0;?>	  
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    @include('flash::message')
                     <div class="row">
                        <div class="col-sm-10">
                           @include('projects._navigation')
                        </div>
                        <div class="col-sm-2">
						@can('add_trello_boards')
                           <div class="project-add-button">
                              <button type="button" class="btn btn-add " data-toggle="modal" data-target="#add">Add New</button>
                           </div>
						@endcan
                        </div>
                     </div>
                     <div class="tab-content" id="myDiv">
					  <button type='button' class="toggle-button" value='fullscreen'  ><i class="fa fa-arrow-left"></i></button>
                        @foreach( $trelloboard as $data1 )
                        <blockquote class="trello-board-compact">
                           <a href="https://trello.com/b/{{ $data1['trello_board_id'] }}" target="_blank">{{ $data1['title'] }}</a>
                        </blockquote>
                        @endforeach
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
 
<!------------- Add Modal Starts ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Add New Board</h4>
         </div>
         <div class="modal-body "  >
         
            <div class="row">
               <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  {!! Form::open(['route' => ['trelloboards.store'] ]) !!}
                  @include('projects._form_trello')
                  <!-- Submit Form Button -->
                  {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                  {!! Form::close() !!}
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>


<!-------------View Modal Starts----->
<div id="view" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Manage Trello Boards</h4>
         </div>
         <div class="modal-body "  >
           
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  <table class="table table-bordered table-striped table-hover" id="printTable">
                     <thead>
                        <tr>
                           <th>Id</th>
                           <td>Title</td>
                           <td>Trello Id</td>
                           <th>Created At</th>
                           @can('edit_trello_boards', 'delete_trello_boards')
                           <th class="text-center">Actions</th>
                           @endcan
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($trelloboard as $item)
                        <tr>
                           <td>{{ $item['id'] }}</td>
                           <td>{{ $item['title'] }}</td>
                           <td>{{ $item['trello_board_id'] }}</td>
                           <td>{{ $item['created_at'] }}</td>
                           @can('edit_trello_boards')
                           <td class="text-center">
                              @include('projects._actions_realtime', [
                              'entity' => 'trelloboards',
                              'id' => $item['id']
                              ])
                           </td>
                           @endcan
                        </tr>
                        <!-------inner model starts-------->		
                        <div id="edit{{$item['id']}}" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                           <div class="modal-dialog"  >
                              <!-- Modal content-->
                              <div class="modal-content"  >
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-center">Update Board</h4>
                                 </div>
                                 <div class="modal-body "  >
                                  
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                          {!! Form::model($id, ['method' => 'PUT', 'route' => ['trelloboards.update',  $item['id'] ] ]) !!}
                                          @include('projects._form_edit_trello')
                                          <!-- Submit Form Button -->
                                          {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                          {!! Form::close() !!}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <script>
                           function close_inner<?php echo $item['id']; ?>()
                           {
                           	$('#edit<?php echo $item['id']; ?>').modal('hide');
                           }
                           
                        </script>
                        <!-------inner model ends-------->
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>


<script src="https://p.trellocdn.com/embed.min.js"></script>
@endsection