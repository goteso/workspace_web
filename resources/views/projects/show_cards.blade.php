@extends('layouts.app_task_angular')
@section('content')


@section('header')
@include('includes.header')
   
  @show
    <div id="wrapper"  >
      <div id="layout-static">
           
         <!---------- Static Sidebar Starts--------------------------------------------------------->			
         @section('sidebar')
             @include('includes.sidebar')
         @show
	     <!---------- Static Sidebar Ends---------------------------------------------------------------------------->	
            
		  <div class="static-content-wrapper">
			@include('layouts.partials.modal')
			<section id="main-header">
			  <div class="container-fluid">
			    <div class="row">
			      <div class="col-sm-12">
			        <div class="horizontal-container frame">

		             <?php $projects = \App\Project::get(); ?>

                     <div ng-app = "myApp" >
                       <div ng-controller="myCtrl">
                    
					    <?php //echo $projects; ?>
				         
						<div class="row ">
						  <div class="col-lg-6 filter-btn ">
							<button class="btn btn-md" style="background-color:#16AE54;">ðŸ‘» All</button>
			                <button class="btn btn-md" ng-model="dueDatevalue" ng-click="checkDueDate()" style="background-color:#CD021B;">ðŸ‘©â€ðŸ’» Due Date  </button>
			                <button class="btn btn-md" ng-model="addedYesterdayvalue" ng-click="checkAddedYesterday()" style="background-color:#E85B47;">âŒ›ï¸ Delayed</button>
			                <button class="btn btn-md"  style="background-color:#59B4E0;">ðŸ“Œ Added Yesterday</button> 
						  </div>
						   
						  <div class="col-lg-6 header-fliters">
							<div class="row">
							   <div class="col-sm-4 header-fliters-data" >
						          <select  class="form-control"  ng-model="projectFilter" ng-options="project as project.project_title for project in projects">
						             <option value="">Select Project </option> 
                                  </select>
						       </div>
							    
                               <div class="col-sm-4 header-fliters-data">
                          		 <div class="dropdown" >
                                    <button class="btn btn-defalut dropdown-toggle" type="button" data-toggle="dropdown">Sort By
                                       <span class="caret"></span>
									</button>
                                    <ul class="dropdown-menu">
                                       <li><a href=""ng-click="sortBy='card_title'; reverse=!reverse;">Alphabetically Method1</a></li>
                                       <li><a href=""ng-click="sort('id')">Alphabetically Method2</a></li> 
                                    </ul>
                                 </div>
                               </div>
                            </div>	
                          </div>
                        </div>						
				 
		 
				        <br><br>
				 
				 <div class="row">
				 
                <div ng-repeat="list in lists " class="deepu col-md-4   col-sm-6 col-xs-12"  style="padding-right: 8px; padding-left: 8px;" >
				 
				
				<div class="bcategory-list" data-list-id="@{{list.id}}" class="col-md-12" >
				<div class="panel panel-default">
				<div class="panel-heading" style="border-bottom: 0px;padding:10px 10px 20px; "> 
				<div class="row">
				<div class="col-lg-10">
                       <!-- <h3 class="panel-title"  >@{{list.list_name}}  <span ng-show="list.list_name = 'To Do'">ðŸ“…</span> <span ng-show="list.list_name = 'In Development'">ðŸ‘§ðŸ»</span>  <span>@{{list.list_name}}</span></h3>-->
					   <h3 class="panel-title"  ><span>
					   <span ng-hide="list.list_name != 'To Do'">ðŸ“…</span>  
					   <span ng-hide="list.list_name != 'In Development'">ðŸ‘©ðŸ»â€ðŸ«â€</span>
					   <span ng-hide="list.list_name != 'In Tester Review'">ðŸ‘©ðŸ»â€ðŸ’»</span>
					   <span ng-hide="list.list_name != 'Need Client Input'">ðŸ¤µðŸ»</span>
					   <span ng-hide="list.list_name != 'Completed'">ðŸŽ‰</span>
					   @{{list.list_name}}  </span></h3>
	    	    </div>
		        <div class="col-lg-2">
				</div> 
				</div>
				</div>
				<div class="panel-body card-list-con frame" id="add_card_form_container" style="padding:0px;">
				
				
				<ul class="list-group" >
				
				
				<div class="card-con" data-listid="@{{list.id}}" id="abc@{{list.id}}"  > 
				  
				  
                 <li  ng-repeat="card in cards | filter:{project_id:projectFilter.id, due_date:dueDate , addedYesterdayvalue} | orderBy : sortBy : reverse | orderBy : sortKey : reverse  "  ng-if="list.id == card.list_id"  class="list-group-item board-list-items" id="card_@{{card.id}}"   data-cardid="@{{card.id}}"  href="#card-detail"  style="border-left: 5px solid #@{{card.card_color}};padding-top:0"   >
				 
				 
				 
				 
				 
				 
				 
				 
				 
				 
				 
				 
				 
				 
				 
				 
				 <!----- CARD STARTS----->
				<a href="#"><span style="color:#5BBFDD;margin-top:0;padding-top:0;font-size:14px;"> @{{card.project_title}} <span ng-show="card.parent_card_id != ''"> <span style="color:black">-</span> #@{{card.parent_card_id}}</span></span> </a>
                    <div class="row">  
					<div class="col-lg-12" style="padding:0px;"> 
					   <p style="margin-bottom: 0px;font-size:14px;" class="" >@{{card.card_title}} </p>  
                      <ul class="card-description-intro list-inline " style="margin-left:0px;">

					  <li>
                         <i class="fa fa-clock-o"></i> @{{card.due_date}}
						 </li>
					 
					  <li id="card_description"  ng-hide="card.card_description == ''">
					   <a href="#" data-placement="bottom" data-toggle="tooltip" title="This card has a description." style="display:block;opacity:1;"><span class="fa fa-align-left" aria-hidden="true"></span></a>
					  </li>
					  
					  <li id="totalTasks" ng-show="card.totalTasks > 0">
					  <a  href="#" data-placement="bottom" data-toggle="tooltip" title="This card have @{{totalTasks}} tasks." data-totaltask="@{{totalTasks}}"><span class="fa fa-check" aria-hidden="true"></span></a>@{{totalTasks}}
					  </li>
					  
					  <li id="totalComments" ng-show="card.totalComments > 0">
					  <a href="#" data-placement="bottom" data-toggle="tooltip" title="This card have @{{totalComments}} comments." data-totalComments="@{{totalComments}}"><span class="fa fa-comment-o" aria-hidden="true"></span></a>@{{totalComments}}
					  </li>
					  
					  
					   <li id="workingHours" ng-show="card.workingHours > 0">
					  <a href="#" data-placement="bottom" data-toggle="tooltip" title="This card have @{{workingHours}} comments." data-workingHours="@{{workingHours}}"><span class="fa fa-comment-o" aria-hidden="true"></span></a>@{{workingHours}}
					  </li>
					  
</ul></div>
            </div>
			</li>
 
			</div>
			</ul>
			
 
 
 
 <!--<div style="position:sticky;bottom:0px;background:#fff;">-->
 <input type="hidden" value="" name="abc" id="abc">
<a href="#" class="show-input-field">Add a card...</a>
<form action="/postCard" method="POST" role="form" style="display: none;">
<input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
<div class="form-group" id="dynamic-board-input-con" style="margin-bottom: 8px;">
<textarea name="card-title" class="form-control" rows="3" placeholder="Card Title Here"></textarea>
<input type="hidden" name="parent_card_id">

<select name="project_id" class="form-control">
<option value="">Select Projects</option>
                                           @foreach($projects as $ct)
                                             <option value="{{ $ct->id }}">{{ $ct->project_title }}</option>
                                           @endforeach
</select>
								   
<input type="hidden" name="list_id" value="@{{list.id}}">
</div>
<div class="form-group" style="margin-bottom: 0px;">
<button type="submit" class="btn btn-primary" id="saveCard">Save</button> <span class="fa fa-remove close-input-field" aria-hidden="true"></span>
</div>
</form>
<!--</div>-->





 






 
      
      </div>
	  
	    </div>
      </div>
	    </div>
		 
		
		</div>
		
 
		
		
      </div>
 </div>
 
 
  </div>
  
  
         </div> 
            </div>
			</div>
			</section>
			</div>
         </div>
      </div>

<script>
         var app = angular.module('myApp', []);
		 
        app.controller('myCtrl',function($scope, $http, $filter) { 
           
 
var cardTaskCount = '<?php echo json_encode($cardTaskCount)?>';
          var data = {
"lists":<?php echo json_encode($lists);?>,
"cards":<?php echo json_encode($cards);?>,
"employees":<?php echo json_encode($employees_array);?>
};

  
            $scope.lists = data.lists;//the data is what you got from api;
			$scope.cards = data.cards;
			$scope.employees = data.employees; 
            console.log(JSON.stringify($scope.lists));
			 console.log(JSON.stringify($scope.cards));
			  console.log(JSON.stringify($scope.employees)); 
			 
			 $scope.sortAlphabetically = function (card) {
				 
				$scope.sortAlphabet = card.id; 
            }
			
			
var project = <?php echo $projects ?>;
		  $scope.projects = project;
		  
		    $scope.checkDueDate = function () { 
				$scope.$watch('dueDatevalue', function (val) {
                   $scope.dueDate = $filter('date')(new Date(), 'd MMM');
                }, true);
			}
			
			  $scope.checkAddedYesterday = function () { 
				$scope.$watch('addedYesterdayvalue', function (val) {
                   $scope.addedYesterday = $filter('date')(new Date().getDate() - 1, 'd MMM');
                }, true);
			}
			
			
			$scope.sort = function(id){
        $scope.sortKey = id;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
			
		
        });
              </script>
              
			  

 
	
   </body>
@endsection