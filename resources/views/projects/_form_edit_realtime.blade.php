<!-- First Name Form Input -->
 

<div class="form-group @if ($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Board Title') !!}
    {!! Form::text('title', $item['title'], ['class' => 'form-control ', 'placeholder' => 'Board Title' ] ) !!}
    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
</div>


 
    {!! Form::hidden('id', @$id, [] ) !!}
 


<div class="form-group @if ($errors->has('realtime_board_id')) has-error @endif">
    {!! Form::label('realtime_board_id', 'RealTimeBoard Id') !!}
    {!! Form::text('realtime_board_id', $item['realtime_board_id'], ['class' => 'form-control', 'placeholder' => 'RealTimeBoard Id' ] ) !!}
    @if ($errors->has('realtime_board_id')) <p class="help-block">{{ $errors->first('realtime_board_id') }}</p> @endif
</div>
