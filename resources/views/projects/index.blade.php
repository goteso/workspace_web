@extends('layout.auth') 
@section('title', 'Projects') 
@section('content') 
@section('header') 
@include('includes.header') 
@show
    <div id="wrapper">
        <div id="layout-static">
            <!---------- Static Sidebar Starts--------->
            @section('sidebar') @include('includes.sidebar') @show
            <!---------- Static Sidebar Ends----------->
            <div class="static-content-wrapper">

                <section id="main-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
								    @include('flash::message')

                                <div class="tab-content">

                                    <div class="row">
                                        <div class="col-sm-7 header-left">
                                            <h3 class="text-left">Projects List </h3>
                                        </div>
                                        <div class="col-sm-5 header-right">
                                            <div class="dropdown">
                                               
                                                @can('add_project')
                                                <button type="button" class="btn btn-trans" data-toggle="modal" data-target="#myModal">Add New</button>
												
                                                @endcan
                                                
                                            </div>
                                        </div>
                                    </div>
                          <?php $color_arr = array("0"=>"#ffe39a", "1"=>"#b6e0cd", "2"=>"#f3c6c3" ,"3"=>'#d8d8d8' , "4"=>'#f8ca9e' , "5"=>'#dd6668'); ?>
											<?php $status_name = array("0"=>"Work Pending", "1"=>"Team Working", "2"=>"Waiting For Client" ,"3"=>'Completed' , "4"=>'Cancelled' , "5"=>'Payment Pending'); ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel">

                                                <div class="row">

                                                    <div class="col-sm-7 ">
													<ul class="nav nav-tabs realTime">
								<li  class="<?php if(Request::path()=='projects_list/projects_by_status/all'  ) { echo 'active';} ?>" ><a href="{{ URL::to('projects_list/projects_by_status/all') }}">All</a></li>					
                              <li  class="<?php if(Request::path()=='projects_list/projects_by_status/active' || Request::path()=='projects') { echo 'active';} ?>" ><a href="{{ URL::to('projects_list/projects_by_status/active') }}">Active</a></li>
							<li  class="<?php if(Request::path()=='projects_list/projects_by_status/3') { echo 'active';} ?>" ><a href="{{ URL::to('projects_list/projects_by_status/3') }}">{{ $status_name[3] }}</a></li><li  class="<?php if(Request::path()=='projects_list/projects_by_status/4') { echo 'active';} ?>" ><a href="{{ URL::to('projects_list/projects_by_status/4') }}">{{ $status_name[4] }}</a></li>						
														
														
														
														
														
 </ul>
                                                     </div>

                                                    <div class="col-sm-5  actions-right">
													
				  @can('export_project')
                                                        <a href="{{ URL::to('downloadExcelProject/xls') }}"><i class="fa fa-share-square-o" title="Export Excel"  ></i></a>@endcan									
													

													
													
													
 
  
    <?php
         echo Form::open(array('url' => '/projects_by_employee','files'=>'true','method'=>'POST'));
        
	?>
	
	<label for="">Project By Employee : &nbsp;</label>					
<select   class="form-control text-right"   id="user_id" name="user_id" style="width:40%;float:right" onchange="this.form.submit()"> 
    <option value="">Select Employee</option>
	<?php if(isset($employees_array) && sizeof($employees_array)>0) {?>
      @foreach($employees_array as $ct)
	  
	  	<?php   $count1 = \App\ProjectEmployees::where('project_id',@$id)->where('employee_id',@$ct->id)->get();
		if(isset($selected_employee_id) && $selected_employee_id == $ct->id ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
<option value="{{ $ct->id }}" <?php echo $s1;?> >{{ $ct->first_name." ".$ct->last_name  }}</option>
     @endforeach
	<?php } ?>
</select> 
 
   <?php
      
         echo Form::close();
      ?>
	  
	  
	  
	  
	  										 


 





                                                       
                                                    
                                                    </div>
                                                </div>

                                             

                                                <div class="row">
												
											 
												 
                                                    <div class="col-md-5">
                                                        <h3 class="modal-title">{{ $result->total() }} {{ str_plural('Projects', $result->count()) }} </h3>
                                                    </div>
                                                    <div class="col-md-7 page-action text-right">

                                                    </div>
                                                </div>
                  
											
					 


                                                <div class="result-set" id="result-set">
                                                    <table class="table table-bordered table-striped table-hover" id="printTable">
                                                        <thead>
                                                            <tr>
                                                                <th>Sr</th>
                                                                <th>Project Title</th>
                                                                <th>Assigned Employees</th>
                                                                <th>Clients</th>
														        <th>Status</th>
                                                          
                                                                @can('edit_project', 'delete_project')
                                                                <th class="text-center">Actions</th>
                                                                @endcan
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $x=0;?>
                                                                @foreach($result as $item)
                                                                <?php $x++;?>
                                                                    <tr>
                                                                        <td>{{ $x }}</td>
                                                                        <td><a href="{{ URL::to('project_details/realtimeboards/'.$item->id) }}" ><b> {{ $item->project_title }}</b></a></td>
                                                                     <td>
																	 <?php 
																	 
																 
																	   $project_employees = \App\ProjectEmployees::where("project_id",$item->id)->get();
																	   
																 
																	   $arr = array();
																	   foreach($project_employees as $e)
																	   {
																		  $user_id = $e->employee_id;
																		     @$name = @\App\User::where('id',$user_id)->first(["first_name"])->first_name;
																			 
																			 if($name != '' && $name !=null)
																			 {
																	 	       $arr[] = '<a href="#">'.$name.'</a> ';
																			 }
																			 
																	   }
																	   echo implode(",",$arr);
																	 ?>
																	 
																	 </td>
																	 
																	        <td>
																	 <?php 
																	 
																 
																	   $project_employees2 = \App\ProjectClients::where("project_id",$item->id)->get();
																	  $arr = array();
																	   foreach($project_employees2 as $e2)
																	   {
																		  $user_id = $e2->client_id;
																		     @$name = @\App\User::where('id',$user_id)->first(["first_name"])->first_name;
																			 
																			 if($name != '' && $name !=null)
																			 {
																	 	       $arr[] = '<a href="#">'.$name.'</a> ';
																			 }
																			 
																	   }
																	   echo implode(",",$arr);
																	 ?>
																	 
																	 </td>
																	 
																	 
																		<td style="background-color:{{$color_arr[$item->status]}};color:#000">{{ $status_name[$item->status] }}</td>
                                                                      
                                                                        @can('edit_project')
                                                                        <td class="text-center">
                                                                            @include('shared._actions', [ 'entity' => 'projects', 'id' => $item->id ])
                                                                        </td>
                                                                        @endcan
                                                                    </tr>
                                                                    @endforeach
                                                        </tbody>
                                                    </table>

                                                    <div class="text-right">
                                                        {{ $result->links() }}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <!------------- Add Model ----->
    <div id="myModal" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-center">Add New <span>Project</span></h4>
                </div>
                <div class="modal-body  ">
                    <div class="row">
                           <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                            {!! Form::open(['route' => ['projects.store'] ]) !!} @include('projects._form')
                            <!-- Submit Form Button -->
                            {!! Form::submit('Create', ['class' => 'btn btn-create']) !!} {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!---Add Model Ends--->

    @endsection