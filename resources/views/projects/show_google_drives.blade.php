@extends('layout.auth')
@section('title', 'Google Drives')
@section('content')
@section('header')
@include('includes.header')
@show

 
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
   
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    @include('flash::message')
                     <div class="row">
                        <div class="col-sm-10 text-center">
                           @include('projects._navigation')
						   <h4 style="color:#FF7979">Make sure you are logged in with <b><u>developers@goteso.com</u></b> in current browser</h4>
                        </div>
                        <div class="col-sm-2">
						 
                        </div>
                     </div>
                     <div class="tab-content" id="myDiv" >
					 
					 <button type='button' class="toggle-button" value='fullscreen'  ><i class="fa fa-arrow-left"></i></button>
                        @foreach( $result as $data )
						<?php if($auth_role == 'Admin') {?>
						        <iframe src="https://drive.google.com/embeddedfolderview?id={{ $data['drive_admin_folder'] }}" style="width:100%; height:600px; border:0;"></iframe>
						<?php } if($auth_role =='Employee') { ?>
						   <iframe src="https://drive.google.com/embeddedfolderview?id={{ $data['drive_development_folder'] }}" style="width:100%; height:600px; border:0;"></iframe>
						<?php } ?>
                        @endforeach
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>

  
@endsection