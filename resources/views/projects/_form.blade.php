<!-- First Name Form Input -->
 

<div class="form-group @if ($errors->has('project_title')) has-error @endif">
    {!! Form::label('project_title', 'Project Title') !!}
    {!! Form::text('project_title', @$project_title, ['class' => 'form-control ', 'placeholder' => 'Project Title' ] ) !!}
    @if ($errors->has('project_title')) <p class="help-block">{{ $errors->first('project_title') }}</p> @endif
</div>


<div class="form-group @if ($errors->has('project_start_date')) has-error @endif">
    {!! Form::label('project_start_date', 'Project Start Date') !!}
    {!! Form::text('project_start_date', @$project_start_date, ['class' => 'form-control datetimepicker', 'placeholder' => 'Project Start Date' ] ) !!}
    @if ($errors->has('project_start_date')) <p class="help-block">{{ $errors->first('project_start_date') }}</p> @endif
</div>


<div class="form-group @if ($errors->has('slack_channel_id')) has-error @endif">
    {!! Form::label('slack_channel_id', 'Slack Channel Id') !!}
    {!! Form::text('slack_channel_id', @$slack_channel_id, ['class' => 'form-control', 'placeholder' => 'Slack Channel Id' ] ) !!}
    @if ($errors->has('slack_channel_id')) <p class="help-block">{{ $errors->first('slack_channel_id') }}</p> @endif
</div>

<div class="form-group @if ($errors->has('drive_admin_folder')) has-error @endif">
    {!! Form::label('drive_admin_folder', 'Drive Admin Folder') !!}
    {!! Form::text('drive_admin_folder', @$drive_admin_folder, ['class' => 'form-control', 'placeholder' => 'Drive Admin Folder' ] ) !!}
    @if ($errors->has('drive_admin_folder')) <p class="help-block">{{ $errors->first('drive_admin_folder') }}</p> @endif
</div>

<div class="form-group @if ($errors->has('drive_development_folder')) has-error @endif">
    {!! Form::label('drive_development_folder', 'Drive Development Folder') !!}
    {!! Form::text('drive_development_folder', @$drive_development_folder, ['class' => 'form-control', 'placeholder' => 'Drive Development Folder' ] ) !!}
    @if ($errors->has('drive_development_folder')) <p class="help-block">{{ $errors->first('drive_development_folder') }}</p> @endif
</div>

 


<div class="form-group @if ($errors->has('status')) has-error @endif">
{!! Form::label('status', 'Project Status') !!}
 {{$errors->has('status')}}
<select class="form-control" name="status" id="status" required >
<option value="">Select Project Status</option>      
<option value="0" <?php if(isset($id) && \App\Project::where('id',@$id)->first(["status"])->status=='0'){ echo 'SELECTED';} ?> >Work Pending</option>
<option value="1" <?php if(isset($id) && \App\Project::where('id',@$id)->first(["status"])->status=='1'){ echo 'SELECTED';} ?>>Team Working</option>
<option value="2" <?php if( isset($id) &&\App\Project::where('id',@$id)->first(["status"])->status=='2'){ echo 'SELECTED';} ?>>Waiting For Client</option>
<option value="3" <?php if( isset($id) &&\App\Project::where('id',@$id)->first(["status"])->status=='3'){ echo 'SELECTED';} ?>>Completed</option>
<option value="4" <?php if( isset($id) &&\App\Project::where('id',@$id)->first(["status"])->status=='4'){ echo 'SELECTED';} ?>>Cancelled</option>
<option value="4" <?php if( isset($id) &&\App\Project::where('id',@$id)->first(["status"])->status=='5'){ echo 'SELECTED';} ?>>Payment Pending</option>
 </select>
   @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
</div>

 
 
 




 
<div class="form-group @if ($errors->has('employees_array')) has-error @endif">
{!! Form::label('employees_array', 'Employees') !!}

 
   <select   class="form-control" id="example-getting-started" name="employees_array[]" multiple="multiple" style="width:100%">
    
      @foreach($employees_array as $ct)
	  
	  	<?php   $count1 = \App\ProjectEmployees::where('project_id',@$id)->where('employee_id',@$ct->id)->get();
		if(isset($id) && $count1->count() > 0 ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
         <option value="{{ $ct->id }}" <?php echo $s1;?> >{{ $ct->first_name." ".$ct->last_name  }}</option>
     @endforeach
</select>
</div>




<div class="form-group @if ($errors->has('clients_array')) has-error @endif">
    {!! Form::label('employees', 'Clients') !!}
   <select  class="form-control" id="example-getting-started2" name="clients_array[]"  multiple="multiple">
     @foreach($clients_array as $ct2)
	<?php   $count2 = \App\ProjectClients::where('project_id',@$id)->where('client_id',$ct2->id)->get();
		if(isset($id) && $count2->count() > 0 ) { $s2 = 'SELECTED';} else { $s2 = '';}
		?>
	     <option value="{{ $ct2->id }}" <?php echo $s2;?>  >{{ $ct2->first_name }}</option>
     @endforeach
</select>
</div>
 

<script type="text/javascript">
   $(document).ready(function() {
       $('#example-getting-started').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
	   $('#example-getting-started2').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
 });
</script>

 

 
<script type='text/javascript'>
$(window).load(function(){
$('.datetimepicker').datetimepicker({  format:'DD/MM/YYYY hh:mm A', });
 
});
</script>