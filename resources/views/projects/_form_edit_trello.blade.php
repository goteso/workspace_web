<!-- First Name Form Input -->
 

<div class="form-group @if ($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Board Title') !!}
    {!! Form::text('title', $item['title'], ['class' => 'form-control ', 'placeholder' => 'Board Title' ] ) !!}
    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
</div>


 
    {!! Form::hidden('id', @$id, [] ) !!}
 


<div class="form-group @if ($errors->has('trello_board_id')) has-error @endif">
    {!! Form::label('trello_board_id', 'Trello Board Id') !!}
    {!! Form::text('trello_board_id', $item['trello_board_id'], ['class' => 'form-control', 'placeholder' => 'Trello Board Id' ] ) !!}
    @if ($errors->has('trello_board_id')) <p class="help-block">{{ $errors->first('trello_board_id') }}</p> @endif
</div>
