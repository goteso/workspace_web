<?php
use App\Role;
?>
@extends('layout.auth')
@section('title', 'Edit Project')
@section('content')

   @section('header')
         @include('includes.header')
      @show
         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    @section('sidebar')
         @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->	
            <div class="static-content-wrapper">
			
			<section id="main-header">
			<div class="container-fluid">
			  <div class="row">
			  <div class="col-sm-12">
			 	    @include('flash::message')

 
    <div class="row">
        <div class="col-md-5">
            <h3>Edit Project Details</h3>
			 
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('projects.index') }}" class="btn btn-default btn-back"> <i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>

    <div class="panel">
	
<div class="tab-content ">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
					
					 
                        {!! Form::model($project, ['method' => 'PUT', 'route' => ['projects.update',  $project->id ] ]) !!}
                            @include('projects._form')
                            <!-- Submit Form Button -->
                            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
 
 
</div>
			
              </div> 
            </div>
			</div>
			</section>
			</div>
         </div>
      </div>
 


 
 
   @endsection
