<!-- First Name Form Input -->
 

{!! Form::hidden('project_id', @$id, [] ) !!}
	
	
<div class="form-group @if ($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Type') !!}
	
	<?php $credentials_types = \App\CredentialsTypes::get(); ?>
	<select name="credentials_type_id" class="form-control">
	 <option value="">Select Type</option>
	@foreach($credentials_types as $ct)
       <option value="{{ $ct->id }}">{{ $ct->title }}</option>
    @endforeach
	</select>
 
</div>


<div class="form-group @if ($errors->has('host')) has-error @endif">
    {!! Form::label('host', 'Host') !!}
    {!! Form::text('host', @$host, ['class' => 'form-control', 'placeholder' => 'Host' ] ) !!}
    @if ($errors->has('host')) <p class="help-block">{{ $errors->first('host') }}</p> @endif
</div>

<div class="form-group @if ($errors->has('user')) has-error @endif">
    {!! Form::label('user', 'UserName') !!}
    {!! Form::text('user', @$user, ['class' => 'form-control', 'placeholder' => 'UserName' ] ) !!}
    @if ($errors->has('user')) <p class="help-block">{{ $errors->first('user') }}</p> @endif
</div>

<div class="form-group @if ($errors->has('password')) has-error @endif">
    {!! Form::label('password', 'Password') !!}
    {!! Form::text('password', @$password, ['class' => 'form-control', 'placeholder' => 'Password' ] ) !!}
    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
</div>

<div class="form-group @if ($errors->has('realtime_board_id')) has-error @endif">
    {!! Form::label('title', 'Sanbox/Live') !!}
       <select name="type" class="form-control">
	     <option value="">Select Type</option>
		 <option value="Live">Live</option>
		 <option value="Sandbox">Sandbox</option>
	</select>
</div>
