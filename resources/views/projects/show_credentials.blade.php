@extends('layout.auth')
@section('title', 'Credentials')
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------>			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------->
	  
      <?php $credentials = $result[0]['credentials']; $x1=0;$x2=0;?>	  
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    @include('flash::message')
                    
                        <div class="row">
                        <div class="col-sm-10">
                          @include('projects._navigation')
                        </div>
                        <div class="col-sm-2">
						
						@can('add_credentials')
                           <div class="project-add-button">
                              <button type="button" class="btn btn-add " data-toggle="modal" data-target="#add">Add New</button>
                           </div>
						@endcan
                        </div>
                     </div>
                     
                     <div class="tab-content">
                        <table class="table table-bordered table-striped table-hover" id="printTable">
                           <thead>
                              <tr>
                                 <th>Sr</th>
                             
                                 <td>Type</td>
                                 <td>Host</td>
                                 <td>UserName</td>
                                 <td>Password</td>
                                 <td>Live/Sandbox</td>
                               
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;?>
                              @foreach($credentials as $item)
                              <?php $x++; if($item['type'] =='Live') { $color='green';} else { $color='red';}?>
                              <tr>
                                 <td>{{ $x }}</td>
                   
                                 <td> <b><?php echo \App\CredentialsTypes::where('id',$item['credentials_type_id'])->first(["title"])->title;?></b></td>
                                 <td>{{ $item['host'] }}</td>
                                 <td>{{ $item['user'] }}</td>
                                 <td>{{ $item['password'] }}</td>
                                 <td  style="color:{{$color}}">{{ $item['type'] }}</td>
                             
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>


<!------------- Add Model ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Add New Credentials</h4>
         </div>
         <div class="modal-body "  >
        
            <div class="row">
                   <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  {!! Form::open(['route' => ['credentials.store'] ]) !!}
                  @include('projects._form_credentials')
                  <!-- Submit Form Button -->
                  {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                  {!! Form::close() !!}
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>


<!------------- View Model ----->
<div id="view" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Manage Credentials</h4>
         </div>
         <div class="modal-body "  >
         
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  <table class="table table-bordered table-striped table-hover" id="printTable">
                     <thead>
                        <tr>
                           <th>Sr</th>
                           <td>Project</td>
                           <td>Type</td>
                           <td>Host</td>
                           <td>UserName</td>
                           <td>Password</td>
                           <td>Live/Sandbox</td>
                           <th>Created At</th>
                           @can('edit_credentials', 'delete_credentials')
                           <th class="text-center">Actions</th>
                           @endcan
                        </tr>
                     </thead>
                     <tbody>
                        <?php $x=0;?>
                        @foreach($credentials as $item)
                        <?php $x++;?>
                        <tr>
                           <td>{{ $x }}</td>
                           <td> <?php echo \App\Project::where('id',$item['project_id'])->first(["project_title"])->project_title;?></td>
                           <td> <?php echo \App\CredentialsTypes::where('id',$item['credentials_type_id'])->first(["title"])->title;?></td>
                           <td>{{ $item['host'] }}</td>
                           <td>{{ $item['user'] }}</td>
                           <td>{{ $item['password'] }}</td>
                           <td>{{ $item['type'] }}</td>
                           <td>{{ $item['created_at'] }}</td>
                           @can('edit_credentials')
                           <td class="text-center">
                              @include('projects._actions_realtime', [
                              'entity' => 'credentials',
                              'id' => $item['id']
                              ])
                           </td>
                           @endcan
                        </tr>
                        <!-------inner model starts-------->		
                        <div id="edit{{$item['id']}}" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                           <div class="modal-dialog"  >
                              <!-- Modal content-->
                              <div class="modal-content"  >
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-center">Update Credentials</h4>
                                 </div>
                                 <div class="modal-body "  >
                                  
                                    <div class="row">
                                       <div class="col-lg-10">
                                          {!! Form::model($id, ['method' => 'PUT', 'route' => ['credentials.update',  $item['id'] ] ]) !!}
                                          @include('projects._form_edit_credentials')
                                          <!-- Submit Form Button -->
                                          {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                          {!! Form::close() !!}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
						<!---- Inner model ends---->
						
						
                        <script>
                           function close_inner<?php echo $item['id']; ?>()
                           {
                           	$('#edit<?php echo $item['id']; ?>').modal('hide');
                           }
                           
                        </script>
                        <!-------inner model ends-------->
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>


 
@endsection