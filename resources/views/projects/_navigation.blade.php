	<div class="row text-center">
					     <h2> <u><?php echo \App\Project::where('id',$id)->first(["project_title"])->project_title;?></u></h2>
					 </div>
<ul class="nav nav-tabs realTime">

@can('view_realtime_boards')
<li  class="<?php if(Request::path()=='project_details/realtimeboards/'.$id) { echo 'active';} ?>" ><a href="{{ URL::to('project_details/realtimeboards/'.$id) }}">RealTimeBoard </a><?php if(Request::path()=='project_details/realtimeboards/'.$id) {?><span>@can('edit_realtime_boards')<i class="fa fa-pencil" data-toggle="modal" data-target="#view"></i></span>@endcan<?php } ?></li>
@endcan


@can('view_trello_boards')
<li  class="<?php if(Request::path()=='project_details/trelloboards/'.$id) { echo 'active';} ?>" ><a href="{{ URL::to('project_details/trelloboards/'.$id) }}">Trello Boards </a><?php if(Request::path()=='project_details/trelloboards/'.$id) {?><span>@can('edit_trello_boards')<i class="fa fa-pencil" data-toggle="modal" data-target="#view"></i></span>@endcan<?php } ?></li>
@endcan


@can('view_credentials')
<li  class="<?php if(Request::path()=='project_details/credentials/'.$id) { echo 'active';} ?>" ><a href="{{ URL::to('project_details/credentials/'.$id) }}">Credentials </a><?php if(Request::path()=='project_details/credentials/'.$id) { ?><span>@can('edit_credentials')<i class="fa fa-pencil" data-toggle="modal" data-target="#view"></i></span>@endcan<?php } ?></li>
@endcan

@can('view_notes')
<li  class="<?php if(Request::path()=='project_details/notes/'.$id) { echo 'active';} ?>" ><a href="{{ URL::to('project_details/notes/'.$id) }}">Notes </a><?php if(Request::path()=='project_details/notes/'.$id) { ?><span>@can('edit_notes')<i class="fa fa-pencil" data-toggle="modal" data-target="#view"></i></span>@endcan<?php } ?></li>
@endcan



@can('view_notes')
<li  class="<?php if(Request::path()=='project_details/drives/'.$id) { echo 'active';} ?>" ><a href="{{ URL::to('project_details/drives/'.$id) }}">Google Drive </a></li>
@endcan



 
<li  class="<?php if(Request::path()=='board/project_details/cards/') { echo 'active';} ?>" ><a href="{{ URL::to('board/project_details/cards/') }}">Tasks</a></li>
 

 


</ul>	