<!-- First Name Form Input -->
 

 

<div class="form-group @if ($errors->has('contact_type_id')) has-error @endif">
{!! Form::label('contact_type_id', 'Contact Type') !!}
 
 {{$errors->has('contact_type_id')}}
 
<?php $contact_types = \App\ContactType::get();?>
<select class="form-control" name="contact_type_id" id="contact_type_id" >
        @foreach($contact_types as $cat)
        <option value="{{$cat->id}}" <?php if($cat->id == \App\ContactType::where('id',$contact->contact_type_id)->first(["id"])->id){ echo 'SELECTED';}?>   >{{ $cat->title }}</option>
        @endforeach
      </select>
</div>



<!-- Contact Value Form Input -->
<div class="form-group @if ($errors->has('contact_value')) has-error @endif">
    {!! Form::label('contact_value', 'Contact Value') !!}
    {!! Form::text('contact_value', null, ['class' => 'form-control', 'placeholder' => 'Contact Value']) !!}
    @if ($errors->has('contact_value')) <p class="help-block">{{ $errors->first('contact_value') }}</p> @endif
</div>




<!-- Contact Value Form Input -->
<div class="form-group @if ($errors->has('contact_title')) has-error @endif">
    {!! Form::label('contact_title', 'Contact Title (optional)') !!}
    {!! Form::text('contact_title', null, ['class' => 'form-control', 'placeholder' => 'Contact Title']) !!}
    @if ($errors->has('contact_title')) <p class="help-block">{{ $errors->first('contact_title') }}</p> @endif
</div>

  
 

<!-- Permissions -->
@if(isset($user))
    @include('shared._permissions', ['closed' => 'true', 'model' => $user ])
@endif