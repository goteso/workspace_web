<!-- First Name Form Input -->
 

{!! Form::hidden('linked_id', $id, [] ) !!}
{!! Form::hidden('notes_type', 'project_development', [] ) !!}
 


@can('notes_permission')
<label for="notes_type" >Notes Type</label>
 
<div class="row">

<div class="col-md-4 col-lg-4">
<h3 style="padding:0;margin:0">Project Development</h3>
</div>
<div class="col-md-8 col-lg-8">

<input type="radio" name="notes_type" class="form-control" style="width:20%" value="project_development" <?php if($item["notes_type"] =='project_development') { echo 'CHECKED';}?>>
</div>
</div>


<div class="row">
<div class="col-md-4 col-lg-4">
<h3 style="padding:0;margin:0">Project Admin</h3>

</div>
<div class="col-md-8 col-lg-8">
<input type="radio" name="notes_type" class="form-control"   style="width:20%" value="project_admin" <?php if($item["notes_type"] =='project_admin') { echo 'CHECKED';}?>>

</div>
</div>
@endcan
 

 
 <div class="row">

<div class="col-md-4 col-lg-4">
 <label for="notes_type" >Pinned ? </label>
</div>
<div class="col-md-8 col-lg-8">

<input type="checkbox" name="pinned" class="form-control" style="width:20%" value="1" <?php if($item["pinned"] =='1') { echo 'CHECKED';}?>>
</div>
</div>
 


 
<div class="form-group @if ($errors->has('host')) has-error @endif">
    {!! Form::label('notes', 'Note') !!}
    {!! Form::text('notes', $item["notes"], ['class' => 'form-control', 'placeholder' => 'Notes' ] ) !!}
    @if ($errors->has('notes')) <p class="help-block">{{ $errors->first('notes') }}</p> @endif
</div>