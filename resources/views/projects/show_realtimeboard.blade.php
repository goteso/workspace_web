<?php
   use App\Role;
?>
@extends('layout.auth')
@section('title', 'RealTimeBoard')
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <?php $realtimeboard = $result[0]['project_real_time_board']; $x1=0;$x2=0;?>	  
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    @include('flash::message')
                     <div class="row">
					 
                        <div class="col-sm-10">
					
					 
                          @include('projects._navigation')
                        </div>
                        <div class="col-sm-2">
						@can('add_realtime_boards')
                           <div class="project-add-button">
                              <button type="button" class="btn btn-add " data-toggle="modal" data-target="#add">Add New</button>
                           </div>
						@endcan
                        </div>
                     </div>
                     <div class="tab-content"  >
                        <ul class="nav nav-pills">
                           @foreach( $realtimeboard as $data1 )
                           <?php $x1++; if($x1==1) { $c1 = 'active';} else { $c1='';}
						    
							?>
							<?php if(sizeof($realtimeboard) > 1 )
							{
							?>
                           <li class="{{ $c1 }}" ><a  data-toggle="tab" href="#{{ $data1['id'] }}"  >{{ $data1['title'] }} </a></li>
							<?php } ?>
                           @endforeach
                        </ul>
                        <div class="tab-content" id="myDiv" >
                           @foreach( $realtimeboard as $data2 )
                           <?php $x2++; if($x2==1) { $c2 = 'in active';} else { $c2='';}
						  
						   ?>
                           <div id="{{ $data2['id'] }}" class="tab-pane fade {{ $c2 }}">
						   
						   <button type='button' class="toggle-button" value='fullscreen'  ><i class="fa fa-arrow-left"></i></button>
                              <iframe src="https://realtimeboard.com/app/embed/{{ $data2['realtime_board_id'] }}" style="width:100%; height:800px; border:0;"></iframe>
                           </div>
                           @endforeach
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------------- Add Modal Starts ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Add New <span>Board</span></h4>
         </div>
         <div class="modal-body "  >
            <br><br>
            <div class="row">
               <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  {!! Form::open(['route' => ['realtimeboards.store'] ]) !!}
                  @include('projects._form_realtime')
                  <!-- Submit Form Button -->
                  {!! Form::submit('Create', ['class' => 'btn btn-create']) !!}
                  {!! Form::close() !!}
               </div>
            </div>
            <br><br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>


<!------------- View Modal Starts ----->
<div id="view" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog text-center"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Manage RealTimeBoards</h4>
         </div>
         <div class="modal-body "  >
         
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  <table class="table table-bordered table-striped table-hover" id="printTable">
                     <thead>
                        <tr>
                           <th>Sr</th>
                           <th>Title</th>
                           <th>RealTimeBoard Id</th>
                           <th>Created At</th>
                           @can('edit_realtime_boards', 'delete_realtime_boards')
                           <th class="text-center">Actions</th>
                           @endcan
                        </tr>
                     </thead>
                     <tbody>
					     <?php $x=0;?>
                        @foreach($realtimeboard as $item)
						<?php $x++;?>
                        <tr>
                           <td>{{ $x }}</td>
                           <td>{{ $item['title'] }}</td>
                           <td>{{ $item['realtime_board_id'] }}</td>
                           <td>{{ $item['created_at'] }}</td>
                           @can('edit_realtime_boards')
                           <td class="text-center">
                              @include('projects._actions_realtime', [
                              'entity' => 'realtimeboards',
                              'id' => $item['id']
                              ])
                           </td>
                           @endcan
                        </tr>
                        <!-------inner model starts-------->		
                        <div id="edit{{$item['id']}}" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                           <div class="modal-dialog"  >
                              <!-- Modal content-->
                              <div class="modal-content"  >
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-center">Update Board</h4>
                                 </div>
                                 <div class="modal-body "  >
                                   
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                          {!! Form::model($id, ['method' => 'PUT', 'route' => ['realtimeboards.update',  $item['id'] ] ]) !!}
                                          @include('projects._form_edit_realtime')
                                          <!-- Submit Form Button -->
                                          {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                          {!! Form::close() !!}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <script>
                           function close_inner<?php echo $item['id']; ?>()
                           {
                           	$('#edit<?php echo $item['id']; ?>').modal('hide');
                           }
                           
                        </script>
                        <!-------inner model ends-------->
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>
@endsection