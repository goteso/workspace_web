@extends('layout.auth')
@section('title', 'Notes')
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
 <style>
 .profileImage {
  width: 100px;
  height: 100px;
  border-radius: 50%;
  background:  #339E2A;
  font-size: 35px;
  color: #fff;
  text-align: center;
  line-height: 100px;
  margin: 0px 25%;
   align:center;
}
 
 </style>
<script defer src="/static/fontawesome/fontawesome-all.js"></script>
      <!---------- Static Sidebar Starts------------>			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends-------------->
      <?php   $x1=0;$x2=0;?>	  
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    @include('flash::message')
                    
                        <div class="row">
                        <div class="col-sm-10">
                          @include('projects._navigation')
                        </div>
                        <div class="col-sm-2">
						
						@can('add_notes')
                           <div class="project-add-button">
                              <button type="button" class="btn btn-add " data-toggle="modal" data-target="#add">Add New</button>
                           </div>
						@endcan
                        </div>
                     </div>
                     
                     <div class="tab-content">
                        <table class="table table-bordered table-striped table-hover" id="printTable">
                           
                           <tbody>
                              <?php $x=0;?>
                              @foreach($notes as $item)
                              <?php $x++;?>
                              <tr>
                               
                                 <td style="padding-top:0.2%;padding-left:1%"><?php 
								 $first_name = @\App\User::where("id",$item['creator_id'])->first(["first_name"])->first_name;
								 $last_name = @\App\User::where("id",$item['creator_id'])->first(["last_name"])->last_name;
								 
								 $short_first_name = substr($first_name, 0, 1);
								 $short_last_name = substr($last_name, 0, 1);
								echo '<div class="row"   >';
								echo '<div class="col-md-2 col-lg-2 col-sm-3 "   >';
								
								echo '<div class="profileImage">';
								 echo $short_first_name;
								 echo $short_last_name;
								 echo '</div>';
								 
								 echo '</div>';
								 
								 echo '<div class="col-md-3 col-lg-3 col-sm-3" >';
									 echo '<h4>'.$first_name." ".$last_name."</h4>";
								 echo '<h5>'.@\Carbon\Carbon::parse($item['created_at'])->format("M j , h:i A")."</h5>";
								  echo '</div>';
								  
								  
								  

								  
								
							     echo '<div class="col-md-7 col-lg-7 col-sm-6 "   style="border-left:1px solid #f5f5f5;min-height:100">';
								 ?>
								 {{ $item['notes'] }}
								 <?php	
								 
								 echo '</div   >';
								  echo '</div>';
								 if($item['pinned'] == '1')
								 {
								 
									?>
									<img src="https://image.flaticon.com/icons/svg/230/230336.svg" style="max-height:20px;absolute:static;top:0;left:0">
									<?php
									
								 }?>
								 </td>
								 
							 
								 
                        
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<!------------- Add New User ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Add New Notes</h4>
         </div>
         <div class="modal-body "  >
        
            <div class="row">
                   <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  {!! Form::open(['route' => ['notes.store'] ]) !!}
                  @include('projects._form_notes')
                  <!-- Submit Form Button -->
                  {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                  {!! Form::close() !!}
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>


<!------------- View Model Starts ----->
<div id="view" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog"  >
      <!-- Modal content-->
      <div class="modal-content"  >
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Manage Notes</h4>
         </div>
         <div class="modal-body "  >
         
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                  <table class="table table-bordered table-striped table-hover" id="printTable">
                     <thead>
                        <tr>
                           <th>Sr</th>
                           <td>Type</td>
                           <td>Notes</td>
						   <td>Pinned ?</td>
                           <th>Created At</th>
                           @can('edit_notes', 'delete_notes')
                           <th class="text-center">Actions</th>
                           @endcan
                        </tr>
                     </thead>
                     <tbody>
                        <?php $x=0;?>
                        @foreach($notes as $item)
                        <?php $x++;?>
                        <tr>
                                 <td>{{ $x }}</td>
                                 <td>{{ $item['notes_type'] }}</td>
                                 <td>{{ $item['notes'] }}</td>
								 <td><?php
								 if($item['pinned'] == '1')
								 {
									 echo 'YES';
								 }
								 else
								 {
									 echo 'NO';
								 }
								 ?>
								 
								 </td>
                                 <td>{{ $item['created_at'] }}</td>
                           @can('edit_notes')
                           <td class="text-center">
                              @include('projects._actions_realtime', [
                              'entity' => 'notes',
                              'id' => $item['id']
                              ])
                           </td>
                           @endcan
                        </tr>
                        <!-------inner model starts-------->		
                        <div id="edit{{$item['id']}}" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                           <div class="modal-dialog"  >
                              <!-- Modal content-->
                              <div class="modal-content"  >
                                 <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-center">Update Notes</h4>
                                 </div>
                                 <div class="modal-body "  >
                                  
                                    <div class="row">
                                       <div class="col-lg-10">
                                          {!! Form::model($id, ['method' => 'PUT', 'route' => ['notes.update',  $item['id'] ] ]) !!}
                                          @include('projects._form_edit_notes')
                                          <!-- Submit Form Button -->
                                          {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                          {!! Form::close() !!}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <script>
                           function close_inner<?php echo $item['id']; ?>()
                           {
                           	$('#edit<?php echo $item['id']; ?>').modal('hide');
                           }
                           
                        </script>
                        <!-------inner model ends-------->
						
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>
@endsection