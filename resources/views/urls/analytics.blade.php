@extends('layout.auth')
@section('title', 'Analytics' )
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    @include('flash::message')
				  <div class="text-right">
                    
                     <!-- <button type="button" class="btn btn-success text-right"  data-toggle="modal" data-target="#add">Add New</button>-->
                 
					 </div>
                     <div class="tab-content">
					 <!-------- Graphs starts -------->
			<script>	


country_codes = {"AF":"AFGHANISTAN",
    "AL":"ALBANIA",
    "DZ":"ALGERIA",
    "AS":"AMERICAN SAMOA",
    "AD":"ANDORRA",
    "AO":"ANGOLA",
    "AI":"ANGUILLA",
    "AQ":"ANTARCTICA",
    "AG":"ANTIGUA AND BARBUDA",
    "AR":"ARGENTINA",
    "AM":"ARMENIA",
    "AW":"ARUBA",
    "AU":"AUSTRALIA",
    "AT":"AUSTRIA",
    "AZ":"AZERBAIJAN",
    "BS":"BAHAMAS",
    "BH":"BAHRAIN",
    "BD":"BANGLADESH",
    "BB":"BARBADOS",
    "BY":"BELARUS",
    "BE":"BELGIUM",
    "BZ":"BELIZE",
    "BJ":"BENIN",
    "BM":"BERMUDA",
    "BT":"BHUTAN",
    "BO":"BOLIVIA",
    "BA":"BOSNIA AND HERZEGOVINA",
    "BW":"BOTSWANA",
    "BV":"BOUVET ISLAND",
    "BR":"BRAZIL",
    "IO":"BRITISH INDIAN OCEAN TERRITORY",
    "BN":"BRUNEI DARUSSALAM",
    "BG":"BULGARIA",
    "BF":"BURKINA FASO",
    "BI":"BURUNDI",
    "KH":"CAMBODIA",
    "CM":"CAMEROON",
    "CA":"CANADA",
    "CV":"CAPE VERDE",
    "KY":"CAYMAN ISLANDS",
    "CF":"CENTRAL AFRICAN REPUBLIC",
    "TD":"CHAD",
    "CL":"CHILE",
    "CN":"CHINA",
    "CX":"CHRISTMAS ISLAND",
    "CC":"COCOS (KEELING) ISLANDS",
    "CO":"COLOMBIA",
    "KM":"COMOROS",
    "CG":"CONGO",
    "CD":"CONGO, THE DEMOCRATIC REPUBLIC OF THE",
    "CK":"COOK ISLANDS",
    "CR":"COSTA RICA",
    "CI":"COTE D IVOIRE",
    "HR":"CROATIA",
    "CU":"CUBA",
    "CY":"CYPRUS",
    "CZ":"CZECH REPUBLIC",
    "DK":"DENMARK",
    "DJ":"DJIBOUTI",
    "DM":"DOMINICA",
    "DO":"DOMINICAN REPUBLIC",
    "TP":"EAST TIMOR",
    "EC":"ECUADOR",
    "EG":"EGYPT",
    "SV":"EL SALVADOR",
    "GQ":"EQUATORIAL GUINEA",
    "ER":"ERITREA",
    "EE":"ESTONIA",
    "ET":"ETHIOPIA",
    "FK":"FALKLAND ISLANDS (MALVINAS)",
    "FO":"FAROE ISLANDS",
    "FJ":"FIJI",
    "FI":"FINLAND",
    "FR":"FRANCE",
    "GF":"FRENCH GUIANA",
    "PF":"FRENCH POLYNESIA",
    "TF":"FRENCH SOUTHERN TERRITORIES",
    "GA":"GABON",
    "GM":"GAMBIA",
    "GE":"GEORGIA",
    "DE":"GERMANY",
    "GH":"GHANA",
    "GI":"GIBRALTAR",
    "GR":"GREECE",
    "GL":"GREENLAND",
    "GD":"GRENADA",
    "GP":"GUADELOUPE",
    "GU":"GUAM",
    "GT":"GUATEMALA",
    "GN":"GUINEA",
    "GW":"GUINEA-BISSAU",
    "GY":"GUYANA",
    "HT":"HAITI",
    "HM":"HEARD ISLAND AND MCDONALD ISLANDS",
    "VA":"HOLY SEE (VATICAN CITY STATE)",
    "HN":"HONDURAS",
    "HK":"HONG KONG",
    "HU":"HUNGARY",
    "IS":"ICELAND",
    "IN":"INDIA",
    "ID":"INDONESIA",
    "IR":"IRAN, ISLAMIC REPUBLIC OF",
    "IQ":"IRAQ",
    "IE":"IRELAND",
    "IL":"ISRAEL",
    "IT":"ITALY",
    "JM":"JAMAICA",
    "JP":"JAPAN",
    "JO":"JORDAN",
    "KZ":"KAZAKSTAN",
    "KE":"KENYA",
    "KI":"KIRIBATI",
    "KP":"KOREA DEMOCRATIC PEOPLES REPUBLIC OF",
    "KR":"KOREA REPUBLIC OF",
    "KW":"KUWAIT",
    "KG":"KYRGYZSTAN",
    "LA":"LAO PEOPLES DEMOCRATIC REPUBLIC",
    "LV":"LATVIA",
    "LB":"LEBANON",
    "LS":"LESOTHO",
    "LR":"LIBERIA",
    "LY":"LIBYAN ARAB JAMAHIRIYA",
    "LI":"LIECHTENSTEIN",
    "LT":"LITHUANIA",
    "LU":"LUXEMBOURG",
    "MO":"MACAU",
    "MK":"MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF",
    "MG":"MADAGASCAR",
    "MW":"MALAWI",
    "MY":"MALAYSIA",
    "MV":"MALDIVES",
    "ML":"MALI",
    "MT":"MALTA",
    "MH":"MARSHALL ISLANDS",
    "MQ":"MARTINIQUE",
    "MR":"MAURITANIA",
    "MU":"MAURITIUS",
    "YT":"MAYOTTE",
    "MX":"MEXICO",
    "FM":"MICRONESIA, FEDERATED STATES OF",
    "MD":"MOLDOVA, REPUBLIC OF",
    "MC":"MONACO",
    "MN":"MONGOLIA",
    "MS":"MONTSERRAT",
    "MA":"MOROCCO",
    "MZ":"MOZAMBIQUE",
    "MM":"MYANMAR",
    "NA":"NAMIBIA",
    "NR":"NAURU",
    "NP":"NEPAL",
    "NL":"NETHERLANDS",
    "AN":"NETHERLANDS ANTILLES",
    "NC":"NEW CALEDONIA",
    "NZ":"NEW ZEALAND",
    "NI":"NICARAGUA",
    "NE":"NIGER",
    "NG":"NIGERIA",
    "NU":"NIUE",
    "NF":"NORFOLK ISLAND",
    "MP":"NORTHERN MARIANA ISLANDS",
    "NO":"NORWAY",
    "OM":"OMAN",
    "PK":"PAKISTAN",
    "PW":"PALAU",
    "PS":"PALESTINIAN TERRITORY, OCCUPIED",
    "PA":"PANAMA",
    "PG":"PAPUA NEW GUINEA",
    "PY":"PARAGUAY",
    "PE":"PERU",
    "PH":"PHILIPPINES",
    "PN":"PITCAIRN",
    "PL":"POLAND",
    "PT":"PORTUGAL",
    "PR":"PUERTO RICO",
    "QA":"QATAR",
    "RE":"REUNION",
    "RO":"ROMANIA",
    "RU":"RUSSIAN FEDERATION",
    "RW":"RWANDA",
    "SH":"SAINT HELENA",
    "KN":"SAINT KITTS AND NEVIS",
    "LC":"SAINT LUCIA",
    "PM":"SAINT PIERRE AND MIQUELON",
    "VC":"SAINT VINCENT AND THE GRENADINES",
    "WS":"SAMOA",
    "SM":"SAN MARINO",
    "ST":"SAO TOME AND PRINCIPE",
    "SA":"SAUDI ARABIA",
    "SN":"SENEGAL",
    "SC":"SEYCHELLES",
    "SL":"SIERRA LEONE",
    "SG":"SINGAPORE",
    "SK":"SLOVAKIA",
    "SI":"SLOVENIA",
    "SB":"SOLOMON ISLANDS",
    "SO":"SOMALIA",
    "ZA":"SOUTH AFRICA",
    "GS":"SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS",
    "ES":"SPAIN",
    "LK":"SRI LANKA",
    "SD":"SUDAN",
    "SR":"SURINAME",
    "SJ":"SVALBARD AND JAN MAYEN",
    "SZ":"SWAZILAND",
    "SE":"SWEDEN",
    "CH":"SWITZERLAND",
    "SY":"SYRIAN ARAB REPUBLIC",
    "TW":"TAIWAN, PROVINCE OF CHINA",
    "TJ":"TAJIKISTAN",
    "TZ":"TANZANIA, UNITED REPUBLIC OF",
    "TH":"THAILAND",
    "TG":"TOGO",
    "TK":"TOKELAU",
    "TO":"TONGA",
    "TT":"TRINIDAD AND TOBAGO",
    "TN":"TUNISIA",
    "TR":"TURKEY",
    "TM":"TURKMENISTAN",
    "TC":"TURKS AND CAICOS ISLANDS",
    "TV":"TUVALU",
    "UG":"UGANDA",
    "UA":"UKRAINE",
    "AE":"UNITED ARAB EMIRATES",
    "GB":"UNITED KINGDOM",
    "US":"UNITED STATES",
    "UM":"UNITED STATES MINOR OUTLYING ISLANDS",
    "UY":"URUGUAY",
    "UZ":"UZBEKISTAN",
    "VU":"VANUATU",
    "VE":"VENEZUELA",
    "VN":"VIET NAM",
    "VG":"VIRGIN ISLANDS, BRITISH",
    "VI":"VIRGIN ISLANDS, U.S.",
    "WF":"WALLIS AND FUTUNA",
    "EH":"WESTERN SAHARA",
    "YE":"YEMEN",
    "YU":"YUGOSLAVIA",
    "ZM":"ZAMBIA",
    "ZW":"ZIMBABWE"};
 		
			 function show_graphs(type) {
	
	 
	
analytic_data ='<?php echo json_encode($analytics);?>';
analytic_data = JSON.parse(analytic_data);

//countries
if(type=='month')
{
var countries_size = analytic_data["analytics"]["month"]["countries"].length;
var countries = analytic_data["analytics"]["month"]["countries"];
}
else if(type=='week'){
	var countries_size = analytic_data["analytics"]["week"]["countries"].length;
var countries = analytic_data["analytics"]["week"]["countries"];
}
else if(type=='day')
{
	var countries_size = analytic_data["analytics"]["day"]["countries"].length;
var countries = analytic_data["analytics"]["day"]["countries"];
}
else if(type=='twoHours')
{
var countries_size = analytic_data["analytics"]["twoHours"]["countries"].length;
var countries = analytic_data["analytics"]["twoHours"]["countries"];	
}
else
{
var countries_size = analytic_data["analytics"]["allTime"]["countries"].length;
var countries = analytic_data["analytics"]["allTime"]["countries"];
}

var d=[];
 
for(var x=0;x<countries_size;x++)
{
	
 var obj = new Object();
 obj.label = country_codes[countries[x]["id"]];
 obj.y = parseInt(countries[x]["count"]);
 d.push(obj);
}
//countries ends


//platforms
 
if(type=='month')
{
var platforms_size = analytic_data["analytics"]["month"]["platforms"].length;
var platforms = analytic_data["analytics"]["month"]["platforms"];
}
else if(type=='week'){
	var platforms_size = analytic_data["analytics"]["week"]["platforms"].length;
var platforms = analytic_data["analytics"]["week"]["platforms"];
}
else if(type=='day')
{
	var platforms_size = analytic_data["analytics"]["day"]["platforms"].length;
var platforms = analytic_data["analytics"]["day"]["platforms"];
}
else if(type=='twoHours')
{
var platforms_size = analytic_data["analytics"]["twoHours"]["platforms"].length;
var platforms = analytic_data["analytics"]["twoHours"]["platforms"];	
}
else
{
var platforms_size = analytic_data["analytics"]["allTime"]["platforms"].length;
var platforms = analytic_data["analytics"]["allTime"]["platforms"];
}

var p=[];
 
for(var x=0;x<platforms_size;x++)
{
	
	//alert(platforms[x]["count"]);
	
 var objp = new Object();
 objp.label = platforms[x]["id"] + '('+platforms[x]["count"]+')';
 objp.y = parseInt(platforms[x]["count"]);
 p.push(objp);
}

 
//platforms ends

 

/**
var d = [
			{ label: "apple",  y: 55  },
			{ label: "orange", y: 15  },
			{ label: "banana", y: 25  },
			{ label: "mango",  y: 30  },
			{ label: "grape",  y: 28  }
		];
**/

var chart = new CanvasJS.Chart("chartContainer", {
	theme: "light2", // "light2", "", "dark2"
	animationEnabled: false, // change to true		
	title:{
		text: "Clicks By Locations"
	},
	data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "pie",
		//indexLabel: "#percent%",
		  indexLabel: "{label}:{y} (#percent%)",
         indexLabelPlacement: "outside",  
		 
         indexLabelOrientation: "horizontal",
		dataPoints: d
	}
	]
});
chart.render();



var platform_chart = new CanvasJS.Chart("platformChartContainer", {
	theme: "light2", // "light2", "", "dark2"
	animationEnabled: false, // change to true		
	title:{
		text: "Analytics By Platform"
	},
	data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "pie",
		dataPoints: p,
		 indexLabel: "{label} (#percent%)",
         indexLabelPlacement: "outside",  
		 
         indexLabelOrientation: "horizontal"
	}
	]
});
platform_chart.render();

}
show_graphs("allTime");

</script>

<button onclick="show_graphs('allTime')" class="btn btn-info">All Times</button>
<button onclick="show_graphs('month')" class="btn btn-info">This Month</button>
<button onclick="show_graphs('week')" class="btn btn-info">This Week</button>
<button onclick="show_graphs('day')" class="btn btn-info">Today</button>
<button onclick="show_graphs('twoHours')" class="btn btn-info">Last 2 Hours</button>

	 <div id="chartContainer" style="height: 370px; width: 100%;"></div>
	 
	 <br>
	  <div id="platformChartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
					 
					 <!-------- Graphs ends here ----->
                 
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
 
 <script>show_graphs("allTime");</script>
 
 
@endsection