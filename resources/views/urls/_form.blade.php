<!-- First Name Form Input -->
 

<div class="form-group @if ($errors->has('title')) has-error @endif">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', @$item['title'], ['class' => 'form-control '  ] ) !!}
    @if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
</div>

<div class="form-group @if ($errors->has('url_type_id')) has-error @endif">
    {!! Form::label('url_type_id', 'Url Type') !!}
    <?php $urls_types = \App\UrlsTypes::get(); ?>
       <select name="url_type_id" class="form-control">
           <option value="">Select Type</option>
               @foreach($urls_types as $ct)
                   <option value="{{ $ct->id }}">{{ $ct->title }}</option>
               @endforeach
       </select>
      @if ($errors->has('url_type_id')) <p class="help-block">{{ $errors->first('url_type_id') }}</p> @endif
</div>

<div class="form-group @if ($errors->has('full_url')) has-error @endif">
    {!! Form::label('full_url', 'Full Url') !!}
    {!! Form::text('full_url', @$item['full_url'], ['class' => 'form-control '  ] ) !!}
    @if ($errors->has('full_url')) <p class="help-block">{{ $errors->first('full_url') }}</p> @endif
</div>


 