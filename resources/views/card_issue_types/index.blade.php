
<?php
   use App\Role;
   ?>
@extends('layout.auth')
@section('title', 'Card Issue Types' )
@section('content')
@section('header')
@include('includes.header')
@show
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      @section('sidebar')
      @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-12">
				  	    @include('flash::message')
				  <div class="text-right">
                     @can('add_designations')
                     <button type="button" class="btn btn-success text-right"  data-toggle="modal" data-target="#add">Add New</button>
                     @endcan
					 </div>
                     <div class="tab-content">
                        <table class="table table-bordered table-striped table-hover" id="printTable">
                           <thead>
                              <tr>
                                 <th>Sr</th>
                                 <th>Title</th>
								 <th>Image</th>
                                 <th>Created At</th>
                                 @can('edit_designations', 'delete_designations')
                                 <th class="text-center">Actions</th>
                                 @endcan
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;?>
                              @foreach($result as $item)
                              <?php $x++;?>
                              <tr>
                                 <td>{{ $x }}</td>
                                 <td>{{ $item['title'] }}</td>
								 <td><img src="{{url('/')}}/admin/issue_types/{{ $item['image'] }}" style="max-height:40px"></td>
                                 <td>{{ $item['created_at'] }}</td>
                                 @can('edit_designations')
                                 <td class="text-center">
                                    @include($model_name.'._actions_with_modals', [
                                    'entity' => $model_name,
                                    'id' => $item['id']
                                    ])
                                 </td>
							
                                 @endcan
                              </tr>
                              <div id="edit{{$item['id']}}" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
                                 <div class="modal-dialog"  >
                                    <!-- Modal content-->
                                    <div class="modal-content"  >
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Update {{ $display_modal_name }}</h4>
                                       </div>
                                       <div class="modal-body "  >
                                          <div class="row">
                                             <div class="col-lg-10">
                                           
                                                @include($model_name.'._form')
                                                <!-- Submit Form Button -->
                                            
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer">
                                          <button type="button" class="btn btn-default"  onclick="close_inner<?php echo $item['id'];?>()">Close</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <script>
                                 function close_inner<?php echo $item['id']; ?>()
                                 {
                                 	$('#edit<?php echo $item['id']; ?>').modal('hide');
                                 }
                                 
                              </script>
							  	 <?php $item=null;?>
                              <!-------inner model ends-------->
                              @endforeach
                           </tbody>
                        </table>
						  <div class="text-right">
                                 {{ $result->links() }}
                           </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<?php
   $roles = Role::pluck('name', 'id');
   ?>
<!--Add New Model Starts--> 
<style>

</style>
<!-- Modal -->
<!------------- Add New User ----->
<div id="add" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
   <div class="modal-dialog "  >
      <!-- Modal content-->
      <div class="modal-content "  >
         <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New {{ $display_modal_name }}</h4>
         </div>
         <div class="modal-body "  >
          
            <div class="row">
               <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
              
                  @include($model_name.'._form')
                  <!-- Submit Form Button -->
               
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!------>
@endsection