        <header id="topnav" class="navbar navbar-dark navbar-fixed-top clearfix" role="banner">

	 
	<span id="trigger-sidebar" class="toolbar-trigger" >
	                  	   <div class="navbar-header">
      <a class="navbar-brand" href="#">
	<img src="{{ env('APP_LOGO_WHITE')}}" class="" style="max-width:120px;"></a>
    </div>  
						 <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar"><span class="icon-bg"><i class="fa fa-fw fa-bars"></i></span></a>
					 
	                </span>
                                        
                    <ul class="nav navbar-nav toolbar pull-right">
                         @if (Auth::guest())
                        <li><a href="{{ url('/admin/login') }}">Login</a></li>
                        <li><a href="{{ url('/admin/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle name" data-toggle="dropdown" role="button" aria-expanded="false">
                                <span class="name">{{ Auth::user()->first_name }} </span><span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                
                   
								  <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
								
								
                            </ul>
                        </li>
                    @endif
					 
					 <!-- <li><span><i class="fa fa-plus-circle"></i></span></li>--->
					  
					  <li class="dropdown">
					         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               <span><i class="fa fa-cog"></i></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                               	   @can('view_contact_types')<li><a href="{{ URL::to('contact_types') }}"><i class="fa fa-phone"></i><span> Contact Types</span></a></li>@endcan
					           	   @can('view_credentials_types')<li><a href="{{ URL::to('credentials_types') }}"><i class="fa fa-key"></i><span> Credentials Types</span></a></li>@endcan
                               	   @can('view_payment_modes')<li><a href="{{ URL::to('payment_modes') }}"><i class="fa fa-key"></i><span> Payment Modes</span></a></li>@endcan
                               	   @can('view_payment_fee_types')<li><a href="{{ URL::to('payment_fee_types') }}"><i class="fa fa-key"></i><span> Payment Fee Types</span></a></li>@endcan
                               	   @can('view_designations')<li><a href="{{ URL::to('designations') }}"><i class="fa fa-key"></i><span> Designations</span></a></li>@endcan	

 <li><a href="{{ URL::to('urls_types') }}"><i class="fa fa-key"></i><span> Url Types</span></a></li> 	

  @can('view_payment_fee_types')<li><a href="{{ URL::to('card_issue_types') }}"><i class="fa fa-key"></i><span> Card Issue Types</span></a></li>@endcan
                            </ul>
                        </li> 
					  
					<!--- <li><span><i class="fa fa-question-circle"></i></span></li>-->
                    </ul>   
   </header>
  