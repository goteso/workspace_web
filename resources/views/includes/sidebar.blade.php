 <div class="static-sidebar-wrapper sidebar-default">
   <div class="static-sidebar">
      <div class="sidebar">
        
         <div class="widget stay-on-collapse" id="widget-sidebar">
            <nav role="navigation" class="widget-body">
               <ul class="acc-menu">
                    <li class="<?php if(Request::path()=='home'  ) { echo 'active';} ?>" ><a href="{{ URL::to('home') }}"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
					@can('view_users')<li class="<?php if(Request::path()=='users'  ) { echo 'active';} ?>" ><a href="{{ URL::to('users') }}"><i class="fa fa-user"></i><span>Members</span></a></li>@endcan
                    @can('view_roles')<li class="<?php if(Request::path()=='roles'  ) { echo 'active';} ?>" ><a href="{{ URL::to('roles') }}"><i class="fa fa-shield"></i><span>Roles & Permissions</span></a></li>@endcan
					
					@can('view_project')<li class="<?php if(Request::path()=='projects'  ) { echo 'active';} ?>" ><a href="{{ URL::to('projects') }}"><i class="fa fa-tasks"></i><span>Projects</span></a></li>@endcan
					@can('view_project')<li class="<?php if(Request::path()=='tutorials'  ) { echo 'active';} ?>" ><a href="{{ URL::to('tutorials') }}"><i class="fa fa-book"></i><span>Tutorials</span></a></li>@endcan
					
					@can('view_project')<li class="<?php if(Request::path()=='board'  ) { echo 'active';} ?>" ><a href="{{ URL::to('board') }}"><i class="fa fa-tasks"></i><span>Task Management</span></a></li>@endcan
					
					@can('view_urls')<li class="<?php if(Request::path()=='urls'  ) { echo 'active';} ?>" ><a href="{{ URL::to('urls') }}"><i class="fa fa-link"></i><span>Url's Management</span></a></li> @endcan
	 
				 
        
		 
        
	 
				  
 
				  
				  
              <!--    <li>
                     <a href="javascript:;"><i class="fa fa-male"></i>  <span>Personalisation</span></a>	
                     <ul class="acc-menu">
        
                        <li><a href="change_pwd.php">Password Reset</a></li>
                     </ul>
                  </li>--->
               </ul>
            </nav>
         </div>
      </div>
   </div>
</div>