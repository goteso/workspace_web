@extends('layout.auth')
@section('title', 'Goteso Workspace')
@section('content')

   @section('header')
         @include('includes.header')
      @show
         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    @section('sidebar')
         @include('includes.sidebar')
      @show
           







		   <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->	
            <div class="static-content-wrapper">
			
			<section id="main-header">
			<div class="container-fluid">
			  <div class="row">
			  <div class="col-sm-12">
 
 
  <div id="expenses" class="tab-pane fade in active">
   <div class="row">
     <div class="col-sm-7 header-left">
       <h3 class="text-left">Dashboard</h3>
	  </div>
	    <div class="col-sm-5 header-right">
 
     </div>
    </div>
	
	
	<div class="row">
     <div class="col-sm-12">
	   <div class="">
 
 
      <div class="row">
	  @can('view_users')
	           <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" >
			   <div class="panel" style="max-height:400px;overflow:auto;padding:2%">
			   <h3 class="text-center" ><b>Users</b> List</h3>
			       <table class="table" id="printTable">
                           <thead>
                              <tr>
                                 <th>Sr</th>
                                 <td>Name</td>
                                  
                              </tr>
                           </thead>
                           <tbody>
                              <?php $x=0;?>
                              @foreach($users as $u)
                              <?php $x++;?>
                              <tr>
                                 <td>{{ $x }}</td>
                                 <td>{{ $u['first_name'] }} {{ $u['last_name'] }}</td>
                   
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
			     </div>
			   </div>
		@endcan	  

@can('view_project')		
	           <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" >
			      <div class="panel" style="max-height:400px;overflow:auto;padding:2%;">
			   <h3 class="text-center" style="font-weight:bold;padding-bottom:10px;border-bottom:1px solid #ccc;">Projects</h3>
			   
			   <ol style="">
			   @foreach($projects as $u)
			   <li style="padding: 2px 5px;font-size: 16px;"><a href="{{ URL::to('project_details/realtimeboards/'.$u['id']) }}" target="_blank">{{ $u['project_title'] }}</a></li>
			   @endforeach
			   </ol>
 
			   </div>
			   </div>
			   @endcan	
			   
			   
			   
			   
	  </div>
		  
		   
		  
 

	   </div>
	  </div> 
    </div>
	
  </div>
  
 
			
              </div> 
            </div>
			</div>
			</section>
			</div>
			
			
			
			
       </div>
      </div>
	  
 
 
 
 
 
 <script>
    $('#page').ready( function(){
		$("#chooseFile").click(function(e){
			e.preventDefault();
			$("input[type=file]").trigger("click");
		});
		$("input[type=file]").change(function(){
			var file = $("input[type=file]")[0].files[0];            
			$("#preview").empty();
			displayAsImage3(file, "preview");
			
			$info = $("#info");
			$info.empty();
			if (file && file.name) {
				$info.append("<li>name:<span>" + file.name + "</span></li>");
			}
			if (file && file.type) {
				$info.append("<li>size:<span>" + file.type + " bytes</span></li>");
			}
			if (file && file.size) {
				$info.append("<li>size:<span>" + file.size + " bytes</span></li>");
			}
			if (file && file.lastModifiedDate) {
				$info.append("<li>lastModifiedDate:<span>" + file.lastModifiedDate + " bytes</span></li>");
			}
			$info.listview("refresh");
		});
    });

    function displayAsImage3(file, containerid) {
		if (typeof FileReader !== "undefined") {
			var container = document.getElementById(containerid),
			    img = document.createElement("img"),
			    reader;
			container.appendChild(img);
			reader = new FileReader();
			reader.onload = (function (theImg) {
				return function (evt) {
					theImg.src = evt.target.result;
				};
			}(img));
			reader.readAsDataURL(file);
		}
	}
	
    </script>
	
   @endsection