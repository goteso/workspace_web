
<div class="form-group @if ($errors->has('first_name')) has-error @endif">
			<?php if(@$user->photo == '')
			{
				echo '<h4 class="text-center" style="height:160px;width:150px;border:1px solid #ccc;border-radius:5px;padding-top:50px;">Upload Profile pic</h4>';
			}
			else
			{
?>
			<img src="<?php echo url('/').'/'.@$user->photo;?>" height="200px" >
			<?php }
			?>
			
				
</div>

   <a href="{{ url('/users/show_edit_user_profile/'.@$user->id ) }}"
                                         >
                                 Update Profile Picture
                                    </a>
<!-- First Name Form Input -->
<div class="form-group @if ($errors->has('first_name')) has-error @endif">
    {!! Form::label('first_name', 'First Name') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
    @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
</div>

<!-- Last Name Form Input -->
<div class="form-group @if ($errors->has('last_name')) has-error @endif">
    {!! Form::label('last_name', 'Last Name') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
    @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
</div>
<!-- email Form Input -->
<div class="form-group @if ($errors->has('slack_id')) has-error @endif">
    {!! Form::label('slack_id', 'Slack Id') !!}
    {!! Form::text('slack_id', null, ['class' => 'form-control', 'placeholder' => 'Slack Id']) !!}
    @if ($errors->has('slack_id')) <p class="help-block">{{ $errors->first('slack_id') }}</p> @endif
</div>
<!-- email Form Input -->
<div class="form-group @if ($errors->has('email')) has-error @endif">
    {!! Form::label('email', 'Email') !!}
    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
    @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
</div>





<!-- password Form Input -->
<div class="form-group @if ($errors->has('password')) has-error @endif">
    {!! Form::label('password', 'Password') !!}
    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
    @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
</div>

<!-- Roles Form Input -->
<div class="form-group @if ($errors->has('roles')) has-error @endif">
    {!! Form::label('roles[]', 'Roles') !!}
    {!! Form::select('roles[]', $roles, isset($user) ? $user->roles->pluck('id')->toArray() : null,  ['class' => 'form-control', 'multiple']) !!}
    @if ($errors->has('roles')) <p class="help-block">{{ $errors->first('roles') }}</p> @endif
</div>

 
					 
					 
<?php if(isset($designations_array) && sizeof($designations_array) > 0 )
{?>
<div class="form-group @if ($errors->has('designations_array')) has-error @endif">
{!! Form::label('designations_array', 'Designation') !!}<br>
   <select   class="form-control" id="example-getting-started" name="designations_array[]" multiple="multiple" style="width:100%;border:1px solid red">
      @foreach($designations_array as $ct)
	    <?php   $count1 = \App\EmployeeDesignations::where('user_id',@$user['id'])->where('designation_id',@$ct->id)->get();
		        $count1 = intval($count1->count());
		 		if(isset($user['id']) && $count1 > 0 ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
         <option value="{{ $ct->id }}" <?php echo $s1;?> >{{ $ct->title }}</option>
     @endforeach
</select>
</div>
<?php } ?>




<script type="text/javascript">
   $(document).ready(function() {
       $('#example-getting-started').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
 
 });
</script>



<!-- Permissions -->
@if(isset($user))
{{--  @include('shared._permissions', ['closed' => 'true', 'model' => $user ]) --}}
@endif
 