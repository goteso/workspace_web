<?php
use App\Role;
?>
@extends('layout.auth')
@section('title', 'Users')
@section('content')

   @section('header')
         @include('includes.header')
      @show
         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    @section('sidebar')
         @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->	
            <div class="static-content-wrapper">
			
			<section id="main-header">
			<div class="container-fluid">
			  <div class="row">
			  <div class="col-sm-12">
			  
			  	    @include('flash::message')
			    <ul class="nav nav-tabs">
			   <?php $records = App\Role::get();$x=0; //echo $records;?>
			  
			  
			   <?php    if(Request::path()=='users') { $class1 = 'active';} else { $class1='';} ?>
						    <li class="<?php echo $class1;?>"><a   href="{{ URL::to('users') }}">ALL</a></li>
			   @if (count($records))
					  @foreach ($records as $record)
					     <?php  $x++; if(Request::path()=='users/'.$record->name) { $class = 'active';} else { $class='';} ?>
						    <li class="<?php echo $class;?>"><a   href="{{ URL::to('users/'.$record->name) }}">{{ $record->name }}</a></li>
                        @endforeach
                        @else
               @endif


                      
                   
                </ul>

				
 


<div class="tab-content">
 
   <div class="row">
     <div class="col-sm-7 header-left">
       <h3 class="text-left">Users List</h3>
	  </div>
	    <div class="col-sm-5 header-right">
		
             <!-- <button class="btn btn-trans dropdown-toggle" type="button" data-toggle="dropdown">New Transaction
              <span class="fa fa-caret-down"></span></button>-->
			   @can('add_users')
               <button type="button" class="btn btn-trans"  data-toggle="modal" data-target="#myModal">Add New</button>
            @endcan
 

     </div>
    </div>
	
	
	<div class="row">
     <div class="col-sm-12">
	   <div class="panel">
	     
		 
		 	     <div class="row">
		 
		
 
 
        <div class="col-sm-7 ">
 
 </div> 
 
  
 
		
		
 
		
	      <div class="col-sm-5  actions-right">
	 
			@can('export_user')
			<?php if( !isset($role))
			{?>
			<a href="{{ URL::to('downloadExcel/xls') }}"><i class="fa fa-share-square-o" title="Export Excel"  ></i></a>
			<?php } else { ?>
			<a href="{{ URL::to('downloadExcel/'.@$role.'/xls') }}"><i class="fa fa-share-square-o" title="Export Excel"  ></i></a>
			<?php } ?>
			
			@endcan
		 
		  </div>
		    </div>
			
			<script>
 function printData()
{
   var divToPrint=document.getElementById("result-set");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}

$('#print_btn').on('click',function(){
printData();
})
			 
			
			</script>
			
		 
	 
		 <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">{{ $result->total() }} {{ str_plural('Users', $result->count()) }} </h3>
        </div>
        <div class="col-md-7 page-action text-right">
           
        </div>
    </div>

    <div class="result-set" id="result-set" >
        <table class="table table-bordered table-striped table-hover table-responsive" id="printTable">
            <thead>
            <tr>
                <th>Sr</th>
                <th>Name</th>
			 
                <th>Email</th>
                <th>Role</th>
				<th>Designations</th>
				 @can('view_contact')<th>Contacts</th>@endcan
                <th>Created At</th>
                @can('edit_users', 'delete_users')
                <th class="text-center">Actions</th>
                @endcan
            </tr>
            </thead>
         	<?php $x=0;?>
            @foreach($result as $item)
			      <?php $x++;
		             $employee_designation_id = @\App\EmployeeDesignations::where('user_id',$item->id)->get();
				  ?>
					  
		 
                <tr>
                    <td>{{ $x }}</td>
                    <td>							<?php if($item->photo == '')
			{
				echo '<h4 class="text-center" style="height:100px;width:100px;border:1px solid #ccc;border-radius:5px;padding-top:20px;">';
				?>
				
                         <a href="{{ url('/users/show_edit_user_profile/'.@$item->id ) }}">Upload Image</a>				
				<?php
				echo '</h4>';
			}
			else
			{
?>
			<img src="<?php echo url('/').'/'.$item->photo;?>" height="100px" >
			<?php }
			?>
					{{ $item->first_name }} {{ $item->last_name }}

					
					</td>
				 
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->roles->implode('name', ', ') }}</td>
					<td>
					 @foreach($employee_designation_id as $d)
					<?php $title = @\App\Designations::where('id',$d->designation_id)->first(["title"])->title; ?>
					{{ $title }}<br>
					 @endforeach
					</td>
					@can('view_contact')<td><a   href="{{ URL::to('contacts/'.$item->roles->implode('name', ', ').'/'.$item->id) }}">Contacts</a></td>@endcan
                    <td>{{ @$item->created_at->toFormattedDateString() }}</td>

                    @can('edit_users')
                    <td class="text-center">
                        @include('shared._actions', [
                            'entity' => 'users',
                            'id' => $item->id
                        ])
                    </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
        </table>
		
		
		

        <div class="text-right">
            {{ $result->links() }}
        </div>
    </div>
		  
		   
 

	   </div>
	  </div> 
    </div>
	
 
 
</div>
			
              </div> 
            </div>
			</div>
			</section>
			</div>
         </div>
      </div>
	  
<?php
      $roles = Role::pluck('name', 'id');
?>
	  
<!--Add New Model Starts-->
	  <style>
	  .modal-dialog {
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
}

.modal-content {
  height: auto;
  min-height: 100%;
  border-radius: 0;
}
</style>
<!-- Modal -->




<!------------- Add New User ----->
<div id="myModal" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
  <div class="modal-dialog"  >

    <!-- Modal content-->
    <div class="modal-content"  >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title  text-center">Add New <span>User</span></h3>
      </div>
      <div class="modal-body "  >
<br><br>

    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            {!! Form::open(['route' => ['users.store'] ]) !!}
                @include('user._form')
                <!-- Submit Form Button -->
                {!! Form::submit('Create', ['class' => 'btn btn-create']) !!}
            {!! Form::close() !!}
        </div>
    </div>
	<br><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!------>
	  
 
   @endsection
