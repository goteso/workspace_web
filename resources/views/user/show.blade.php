<?php
use App\Role;
?>
@extends('layout.auth')
@section('title', 'Edit User')
@section('content')

   @section('header')
         @include('includes.header')
      @show
         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    @section('sidebar')
         @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->	
            <div class="static-content-wrapper">
			
			<section id="main-header">
			<div class="container-fluid">
			  <div class="row">
			  <div class="col-sm-12">
			 

 	    @include('flash::message')
    <div class="row">
        <div class="col-md-5">
            <h3 class="edit-title"> <span>User</span> Profile</h3> 
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('users.index') }}" class="btn btn-back  "> <i class="fa fa-arrow-left"></i> &nbsp;&nbsp;Back</a>
        </div>
    </div>
	
	

  <div class="panel wrapper wrapper-content animated fadeInRight">
	
<div class="tab-content">
        <div class="row">
            <div class="col-md-12 col-lg-10">
                <div class="ibox float-e-margins">
                    <div class="ibox-content" >
                      
						
						        {!! Form::open(['route' => ['update_edit_user_profile'] ,'files'=>'true' , 'method'=>'POST']) !!}
			<?php if($user->photo == '')
			{
				echo '<h4 class="text-center" style="height:160px;width:150px;border:1px solid #ccc;border-radius:5px;padding-top:50px;">Upload Profile pic</h4>';
			}
			else
			{
?>
			<img src="<?php echo url('/').'/'.$user->photo;?>" height="200px" >
			<?php }
			?>
			
			       <?php  
  
         echo Form::file('image');
		 ?>
                            
						<br>	
		<input type="hidden" name="user_id" value="<?php echo $user->id; ?>">			
 

 




                            <!-- Submit Form Button -->
                            {!! Form::submit('UPDATE', ['class' => 'btn btn-save']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
 
 
 
			
              </div> 
            </div>
			</div>
			</section>
			</div>
         </div>
      </div>
 



 
	  
 
   @endsection
