@extends('layouts.app_task')
@section('content')

<div class="horizontal-container frame">

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

<?php $projects = \App\Project::get(); ?>
			<!------ filters menu starts----->
	<div class="row"  >
	
		 {{ Form::open(array('url' => 'board/cards-filter' , 'method'=>'POST')) }}
	     <!--- left filters--->
	      <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
		     <button class="btn btn-md btn-info" value="all" onclick="abc(this.value)">All</button>
			 <button class="btn btn-md btn-info" value="due_date" onclick="insert_quick_filters(this.value)" >Due Date</button>
			 <button class="btn btn-md btn-info" value="delayed" onclick="insert_quick_filters(this.value)">Delayed</button>
			 <button class="btn btn-md btn-info" value="added_yesterday" onclick="insert_quick_filters(this.value)">Added Yesterday</button>
			 <input type="text" id="quick_filters" name="filter" value="<?php echo @$filter;?>">
		  </div>
		  
		  
		  
		  
		  <!--- right filters--->
		  <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
		                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
				                 
							
                                   <select name="filter_project_id"  class="form-control" onchange="this.form.submit()" >
                                   
								      <option value="" <?php if(!isset($project_id) ) { echo 'SELECTED';}?> >Filter by Projects</option>
                                           @foreach($projects as $ct)
                                             <option value="{{ $ct->id }}" <?php if(isset($project_id) && $project_id == $ct->id ) { echo 'SELECTED';}?> >{{ $ct->project_title }}</option>
								           @endforeach
                                   </select>
								  
							</div>	   
						   
						    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
								  
                                    <select name="filter_issue" id="select-project" class="form-control"  >
                                    <option value="">Select Projects</option>
									      <option value="" <?php if(!isset($project_id) ) { echo 'SELECTED';}?> >ALL</option>
                                           @foreach($projects as $ct)
                                             <option value="{{ $ct->id }}" <?php if(isset($project_id) && $project_id == $ct->id ) { echo 'SELECTED';}?> >{{ $ct->project_title }}</option>
								           @endforeach
                                   </select>
							</div>	   
								   
							 <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
								      
                                    <select name="filter_sort" id="select-project" class="form-control"  >
                                    <option value="">Select Projects</option>
									      <option value="" <?php if(!isset($project_id) ) { echo 'SELECTED';}?> >ALL</option>
                                           @foreach($projects as $ct)
                                             <option value="{{ $ct->id }}" <?php if(isset($project_id) && $project_id == $ct->id ) { echo 'SELECTED';}?> >{{ $ct->project_title }}</option>
								           @endforeach
                                   </select>
							</div>
								   	 </div>	
		 
		  </div>
		  
		 {{ Form::close() }}  
		  
 
	
			 
    				   
	
	 
	<!------ filters menu ends----->
	
    <div class="row horizontal-row list-sortable" id="cards_container"  ng-app="myApp" ng-controller="namesCtrl" >

 
 
    </div>
	
	
	<script>
angular.module('myApp', []).controller('namesCtrl', function($scope) {
    $scope.names = [
        'Jani',
        'Carl',
        'Margareth',
        'Hege',
        'Joe',
        'Gustav',
        'Birgit',
        'Mary',
        'Kai'
    ];
});
</script>
</div>

 
 
 <script>
 function insert_quick_filters(v)
 {
	 document.getElementById("quick_filters").value=v;
 }
 </script>
 
 
 
<script>


 
var lists =  <?php echo json_encode($lists);?>;
var cards =  <?php echo json_encode($cards);?>;
var cardTaskCount = '<?php echo json_encode($cardTaskCount)?>';
cardTaskCount = JSON.parse(cardTaskCount);
   
function abc(filter,lists)
  {
	var final_data = '';
	
	for(var i=0; i<lists.length;i++)
	{
		var main='';
		var list_string_1 = '';
		var list_id = lists[i]["id"];
		var list_name = lists[i]["list_name"];
		
		
		list_string_1 += '<div class="bcategory-list" data-list-id="'+list_id+'">';
        list_string_1 += '<div class="panel panel-default">';
        list_string_1 += '<div class="panel-heading" style="border-bottom: 0px; ">';
        list_string_1 += '  <div class="row">';
        list_string_1 += '     <div class="col-lg-10">';
                             
		list_string_1 += '	  <h3 class="panel-title" >'+list_name+'</h3>';
        list_string_1 += '     </div>';
        list_string_1 += '   <div class="col-lg-2">';
        list_string_1 += '   </div>';
        list_string_1 += '</div>';
        list_string_1 += '</div>';
					 
        list_string_1 += '<div class="panel-body card-list-con frame" >';
        list_string_1 += ' <ul class="list-group">';
        list_string_1 += '<div class="card-con" data-listid="'+list_id+'">';
						
 					
					main += list_string_1;
					
					
					var list_string_2='';
					   for(var y=0; y<cards.length;y++)
	                        {
								var cid= cards[y]["id"];
								 
					           if(cards[y]["list_id"] == list_id )
							   {
								   var card_color = cards[y]["card_color"];
								   var card_id = cards[y]["id"];
								   var card_title = cards[y]["card_title"];
								   var card_description = cards[y]["card_description"];
								   var totalComments = cards[y]["totalComments"];								  
								   var project_title = cards[y]["project_title"];
								   var due_date = cards[y]["due_date"];
								   
list_string_2 += '<li   class="list-group-item board-list-items" id="card_'+card_id+'"   data-cardid ="'+card_id+'"  href="#card-detail"  style="border-left: 5px solid #'+card_color+';" >';
list_string_2 += project_title;
list_string_2 += '<div class="row">';
list_string_2 += '<div class="col-lg-12">';
list_string_2 += '<p style="margin-bottom: 0px;" class="pull-left">'+card_title+'</p>';    
list_string_2 += '<ul class="card-description-intro list-inline pull-right">';


if(card_description != '')
{
list_string_2 += '<li id="card_description">';
list_string_2 += '<a href="#" data-placement="bottom" data-toggle="tooltip" title="This card has a description." ><span class="glyphicon glyphicon-align-left" aria-hidden="true"></span></a>';
list_string_2 += '</li>';
}


 

for(var ct=0;ct<cardTaskCount.length;ct++)
{
	var totalTasks = cardTaskCount[ct]["totalTasks"];
	if( cardTaskCount[ct]["id"] == card_id )
	{
		if(totalTasks > 0)
		{
			list_string_2 += '<li id="totalTasks" >';
list_string_2 += '<a  href="#" data-placement="bottom" data-toggle="tooltip" title="This card have '+totalTasks+' tasks." data-totaltask="'+totalTasks+'"><span class="glyphicon glyphicon-check" aria-hidden="true"></span></a>';
            list_string_2 += '</li>';
			
		}
	}
}
 

                                                         
                                                      
if(totalComments > 0)
{	
list_string_2 += '<li id="totalComments">';
list_string_2 += '<a href="#" data-placement="bottom" data-toggle="tooltip" title="This card have '+totalComments+' comments." data-totalComments="'+totalComments+'"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span></a>'+totalComments;
list_string_2 += '</li>';
}


                                                      
														list_string_2 += due_date;
														 
                                                    list_string_2 += '</ul>';
                                                list_string_2 += '</div>';
                                            list_string_2 += '</div>';                                           
                                        list_string_2 += '</li>';
							   }
		  
							}
						 
							main += list_string_2;
							
							
							
							
	

var list_string_3 = '';
list_string_3 +='</div>';	
list_string_3 +='</ul>';



list_string_3 +='<input type="hidden" value="" name="abc" id="abc">';
list_string_3 +='<a href="#" class="show-input-field">Add a card...</a>';
list_string_3 +='<form action="/postCard" method="POST" role="form" style="display: none;">';
list_string_3 +='<input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >';
list_string_3 +='<div class="form-group" id="dynamic-board-input-con" style="margin-bottom: 8px;">';
list_string_3 +='<textarea name="card-title" class="form-control" rows="3"></textarea>';


list_string_3 +='<select name="project_id" class="form-control">';
list_string_3 +='<option value="">Select Projects</option>';
                                           @foreach($projects as $ct)
                                             list_string_3 +='<option value="{{ $ct->id }}">{{ $ct->project_title }}</option>';
                                           @endforeach
list_string_3 +='</select>';
								   
								   
list_string_3 +='<select   class="form-control" id="example-getting-started'+list_id+'" name="employees_array[]" multiple="multiple" style="width:100%">';
    
	<?php if(isset($employees_array) && sizeof($employees_array)>0) {?>
      @foreach($employees_array as $ct)
	  
	  	<?php   $count1 = \App\ProjectEmployees::where('project_id',@$id)->where('employee_id',@$ct->id)->get();
		if(isset($id) && $count1->count() > 0 ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
list_string_3 +='<option value="{{ $ct->id }}" <?php echo $s1;?> >{{ $ct->first_name." ".$ct->last_name  }}</option>';
     @endforeach
	<?php } ?>
list_string_3 +='</select>';

  
  $(document).ready(function() {
	  
$('#example-getting-started'+list_id).multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,numberDisplayed: 100,filterBehavior: 'text'});
  });



list_string_3 +='<input type="hidden" name="list_id" value="'+list_id+'">';
list_string_3 +='</div>';
list_string_3 +='<div class="form-group" style="margin-bottom: 0px;">';
list_string_3 +='<button type="submit" class="btn btn-primary" id="saveCard">Save It here</button> <span class="glyphicon glyphicon-remove close-input-field" aria-hidden="true"></span>';
list_string_3 +='</div>';
list_string_3 +='</form>';
list_string_3 +='</div>';
list_string_3 +='</div>';
list_string_3 +='</div>';
 
			
			
	main += list_string_3;		
	// main += list_string_1+list_string_2+list_string_3;
	 //alert(main);
			 
		  				
 						
		final_data += main;				
	}
	
			 document.getElementById("cards_container").innerHTML = final_data;
    		 
		   
		   
		   
  }
 

 
 
 abc('due_date',lists);


   
 
 </script>
 
 
 <!---
<select   class="form-control" id="example1" name="emp[]" multiple="multiple" style="width:100%">
 <option value="1">1</option>
 <option value="2">2</option>
 <option value="3">3</option>
 <option value="4">4</option>
 <option value="5">5</option>
 </select>--->
<script type="text/javascript">
   $(document).ready(function() {
	   
	    $('#example1').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
	   
	   
       $('#example-getting-started').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
	   
	   $('#example-getting-started2').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
 });
</script>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
 
@endsection