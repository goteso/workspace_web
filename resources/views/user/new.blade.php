@extends('layout.auth')

@section('title', 'Create')

@section('content')
   @section('header')
         @include('includes.header')
      @show
         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    @section('sidebar')
         @include('includes.sidebar')
      @show

    <div class="row">
         
        <div class="col-md-6 page-action text-right">
            <a href="{{ route('users.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>

    <div class="row">dfdfdf
        <div class="col-lg-10">
         <?php  
			echo Form::open(array('route' => '/users.store','files'=>'true'));
         echo 'Select the file to upload.';
         echo Form::file('image');
		 ?>
                @include('user._form')
                <!-- Submit Form Button -->
                {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
	
	       </div>
    </div>
@endsection