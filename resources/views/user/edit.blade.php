<?php
use App\Role;
?>
@extends('layout.auth')
@section('title', 'Edit User')
@section('content')

   @section('header')
         @include('includes.header')
      @show
         <div id="wrapper"  >
         <div id="layout-static">
            <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
    @section('sidebar')
         @include('includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------------------------------------------------------------------------------------------------------------->	
            <div class="static-content-wrapper">
			
			<section id="main-header">
			<div class="container-fluid">
			  <div class="row">
			  <div class="col-sm-12">
			 

 	    @include('flash::message')
    <div class="row">
        <div class="col-md-5">
            <h3 class="edit-title">Edit <span>User</span> Details</h3> 
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('users.index') }}" class="btn btn-back  "> <i class="fa fa-arrow-left"></i> &nbsp;&nbsp;Back</a>
        </div>
    </div>
	
	 @csrf

  <div class="panel wrapper wrapper-content animated fadeInRight">

<div class="tab-content">
        <div class="row">
            <div class="col-md-12 col-lg-10">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        {!! Form::model($user, ['method' => 'PUT', 'route' => ['users.update',  $user->id ] ]) !!}
                            @include('user._form')
                            <!-- Submit Form Button -->
                            {!! Form::submit('Save Changes', ['class' => 'btn btn-save']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
 
 
 
			
              </div> 
            </div>
			</div>
			</section>
			</div>
         </div>
      </div>
	  
<?php
      $roles = Role::pluck('name', 'id');
?>
	  
<!--Add New Model Starts-- 
	  <style>
	  .modal-dialog {
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
}

.modal-content {
  height: auto;
  min-height: 100%;
  border-radius: 0;
}
</style>
<!-- Modal -->




<!------------- Add New User ----->
<div id="myModal" class="modal fade" data-backdrop="false" style="z-index:99999" role="dialog">
  <div class="modal-dialog"  >

    <!-- Modal content-->
    <div class="modal-content"  >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New User</h4>
      </div>
      <div class="modal-body "  >

		    <div class="row">
        <div class="col-lg-10">
		        <p>Enter Below from to Register a New User.</p>
            <h3>Create</h3>
        </div>
 
    </div>

    <div class="row">
        <div class="col-lg-10">
            {!! Form::open(['route' => ['users.store'] ]) !!}
                @include('user._form')
                <!-- Submit Form Button -->
                {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!------>
	  
 
   @endsection
