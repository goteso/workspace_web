
        @foreach($lists as $list)
            <div class="bcategory-list" data-list-id="{{ $list['id'] }}">
                <div class="panel panel-default">
                    <div class="panel-heading" style="border-bottom: 0px; ">
                        <div class="row">
                            <div class="col-lg-10">
                              <!---  <h3 class="panel-title board-panel-title" data-pk="{{ $list['id'] }}">{{ $list['list_name'] }}</h3>-->
							  <h3 class="panel-title" >{{ $list['list_name'] }} - ajax</h3>
                            </div>
                            <div class="col-lg-2">
                                <!---<span data-listid="{{ $list['id'] }}" class="glyphicon glyphicon-trash delete-list" aria-hidden="true" style="cursor: pointer; top: 3px;"></span>--->
                            </div>
                        </div>
                    </div>
					 
                    <div class="panel-body card-list-con frame">
                        <ul class="list-group">
                            <div class="card-con" data-listid="{{ $list->id }}">
							
							
                                @foreach($cards as $card)
		 
                                    @if($card['list_id'] === $list['id']) 
									
                                        <li class="list-group-item board-list-items" id="card_{{ $card['id'] }}"   data-cardid ="{{ $card['id'] }}" data-toggle="modal" href="#card-detail" @if(!empty($card["card_color"])) style="border-top: 5px solid #{{ $card['card_color'] }};" @endif>
											<?php $project_title= \App\Project::where('id',$card['project_id'])->first(["project_title"])->project_title; echo $project_title;?>
                                            <div class="row">
                                                <div class="col-lg-12">
											 
                                                    <p style="margin-bottom: 0px;" class="pull-left">{{ $card['card_title'] }}</p>    
                                                    <ul class="card-description-intro list-inline pull-right">
													  
                                                        @if($card["card_description"] !== '')
                                                            <li id="card_description">
                                                                <a href="#" data-placement="bottom" data-toggle="tooltip" title="This card has a description." ><span class="fa fa-align-left" aria-hidden="true"></span></a>
                                                            </li>
                                                        @endif
                                                        @foreach($cardTaskCount as $x) 
                                                            @if($x["id"] === $card['id'])
                                                                @if($x['totalTasks'] > 0)
                                                                    <li id="totalTasks" >
                                                                        <a  href="#" data-placement="bottom" data-toggle="tooltip" title="This card have {{ $x['totalTasks'] }} tasks." data-totaltask="{{ $x['totalTasks'] }}"><span class="fa fa-check" aria-hidden="true"></span></a>
                                                                    </li>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                        @if($card['totalComments'] > 0) 
                                                            <li id="totalComments">
                                                                <a href="#" data-placement="bottom" data-toggle="tooltip" title="This card have {{ $card['totalComments'] }} comments." data-totalComments="{{ $card['totalComments'] }}"><span class="fa fa-comment" aria-hidden="true"></span></a>{{ $card['totalComments'] }}
                                                            </li>
                                                        @endif
														<?php $createdAt = \Carbon\Carbon::parse($card['due_date']); echo $createdAt->format("j M");?>
														 
                                                    </ul>
                                                </div>
                                            </div>                                            
                                        </li>
                                    @endif
                                @endforeach
                            </div>
                        </ul>
						
						
						 <input type="hidden" value="" name="abc" id="abc">
				 
                                              
										 

                                          
													
													
													
                        <a href="#" class="show-input-field">Add a card...</a>
                        <form action="/postCard" method="POST" role="form" style="display: none;">
						 
						           <input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
                            <div class="form-group" id="dynamic-board-input-con" style="margin-bottom: 8px;">
                                <textarea name="card-title" class="form-control" rows="3"></textarea>
								
								   <?php $projects = \App\Project::get(); ?>
                                    <select name="project_id" class="form-control">
                                    <option value="">Select Projects</option>
                                           @foreach($projects as $ct)
                                             <option value="{{ $ct->id }}">{{ $ct->project_title }}</option>
                                           @endforeach
                                   </select>
								   
                                <input type="hidden" name="list_id" value="{{ $list->id }}">
                                
                            </div>
                            <div class="form-group" style="margin-bottom: 0px;">
                                <button type="submit" class="btn btn-primary" id="saveCard">Save It here</button> <span class="fa fa-remove close-input-field" aria-hidden="true"></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
