@extends('layouts.app_task')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<div class="horizontal-container frame">

		 

 <?php $projects = \App\Project::get(); ?>

    <div ng-app = "myApp" >
        
	
           <div ng-controller="myCtrl">
                
	 		<?php //echo $projects; ?>
				 
								   <div class="row">
								    <div class="col-md-6">
								     <button class="btn btn-md btn-info" >All</button>
			 <button class="btn btn-md btn-info" ng-model="date" ng-click="checkDueDate()" >Due Date  </button>
			 <button class="btn btn-md btn-info"  >Delayed</button>
			 <button class="btn btn-md btn-info"  >Added Yesterday</button> 
						    	
						 </div>
						 
								   <div class="col-md-6">
								   
								   <div class="row">
								   
								   <div class="col-sm-4">
						     
						 
						   <select  class="form-control"  ng-model="projectFilter" ng-options="project as project.project_title for project in projects">
						<option value="">Select Project</option> 
                           </select>
						 
						 
						 </div>

						  <div class="col-sm-4">
                          			  <div class="dropdown" >
                                              <button class="btn btn-defalut dropdown-toggle" type="button" data-toggle="dropdown">Sort By
                                               <span class="caret"></span></button>
                                                   <ul class="dropdown-menu">
                                                  <li><a href=""ng-click="sortBy='card_title'; reverse=!reverse;">Alphabetically Method1</a></li>
                                                  <li><a href=""ng-click="sort('id')">Alphabetically Method2</a></li> 
                                                </ul>
                                             </div>



			   
						 </div>
                        </div>	
</div>

<input ng-model="searchName">
</div>						
				 
				 ghg
				 
				 <br><br>
				 
				 
				 
                <div ng-repeat="list in lists ">
				
				
				<div class="bcategory-list" data-list-id="@{{list.id}}">
				<div class="panel panel-default">
				<div class="panel-heading" style="border-bottom: 0px; "> 
				<div class="row">
				<div class="col-lg-10">
                        <h3 class="panel-title" >@{{list.list_name}}</h3>
	    	    </div>
		        <div class="col-lg-2">
				</div> 
				</div>
				</div>
				<div class="panel-body card-list-con frame" >
				
				
				<ul class="list-group" >
				<div class="card-con" data-listid="@{{list.id}}" > 
				  
				  
                 <li  ng-repeat="card in cards | filter:{project_id:projectFilter.id, due_date:date, searchName} | orderBy : sortBy : reverse | orderBy : sortKey : reverse  "  ng-if="list.id == card.list_id"  class="list-group-item board-list-items" id="card_@{{card.id}}"   data-cardid="@{{card.id}}"  href="#card-detail"  style="border-left: 5px solid #@{{card.card_color}};" >
				 
				 
				 @{{card.project_title}}
                    <div class="row">  
					<div class="col-lg-12"> 
					   <p style="margin-bottom: 0px;" class="pull-left">@{{card.card_title}} </p>  
                      <ul class="card-description-intro list-inline pull-right">


					 
					  <li id="card_description"  ng-hide="card.card_description == ''">
					   <a href="#" data-placement="bottom" data-toggle="tooltip" title="This card has a description." ><span class="glyphicon glyphicon-align-left" aria-hidden="true"></span></a>
					  </li>
					  
					  <li id="totalTasks" ng-show="card.totalTasks > 0">
					  <a  href="#" data-placement="bottom" data-toggle="tooltip" title="This card have @{{totalTasks}} tasks." data-totaltask="@{{totalTasks}}"><span class="glyphicon glyphicon-check" aria-hidden="true"></span></a>@{{totalTasks}}
					  </li>
					  
					  <li id="totalComments" ng-show="card.totalComments > 0">
					  <a href="#" data-placement="bottom" data-toggle="tooltip" title="This card have @{{totalComments}} comments." data-totalComments="@{{totalComments}}"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span></a>@{{totalComments}}
					  </li>
					  
					  @{{card.due_date}}
</ul></div>
            </div>
			</li>
 
			</div>
			</ul>
      
      </div>
	    </div>
      </div>
	    </div>
		
		
		
		<textarea id="res"></textarea>
		
		
      </div>
 </div>
 
 
  </div>

<script>
         var app = angular.module('myApp', []);
		 
        app.controller('myCtrl',function($scope, $http) { 
           
 
var cardTaskCount = '<?php echo json_encode($cardTaskCount)?>';
          var data = {
"lists":<?php echo json_encode($lists);?>,
"cards":<?php echo json_encode($cards);?>
};

 
            $scope.lists = data.lists;//the data is what you got from api;
			$scope.cards = data.cards;
            console.log(JSON.stringify($scope.lists));
			 console.log(JSON.stringify($scope.cards));
			 
			 $scope.sortAlphabetically = function (card) {
				 
				$scope.sortAlphabet = card.id; 
            }
			
			
var project = <?php echo $projects ?>;
		  $scope.projects = project;
		  
		  $scope.checkDueDate = function () {
                // $scope.date = new Date(); 
				$scope.date = "3 Feb"; 
            }
			
			
			$scope.sort = function(id){
        $scope.sortKey = id;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
			
		
        });
              </script>
              
   </body>
@endsection