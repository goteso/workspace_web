@extends('layouts.app_task')

@section('content')


<div class="horizontal-container frame">

 <?php $projects = \App\Project::get(); ?>
			<!------ filters menu starts----->
	<div class="row"  >
	
		 {{ Form::open(array('url' => 'board/cards-filter' , 'method'=>'POST')) }}
	     <!--- left filters--->
	      <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
		     <button class="btn btn-md btn-info" value="all" onclick="insert_quick_filters(this.value)">All</button>
			 <button class="btn btn-md btn-info" value="due_date" onclick="insert_quick_filters(this.value)" >Due Date</button>
			 <button class="btn btn-md btn-info" value="delayed" onclick="insert_quick_filters(this.value)">Delayed</button>
			 <button class="btn btn-md btn-info" value="added_yesterday" onclick="insert_quick_filters(this.value)">Added Yesterday</button>
			 <input type="text" id="quick_filters" name="filter" value="<?php echo @$filter;?>">
		  </div>
		  
		  
		  
		  
		  <!--- right filters--->
		  <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
		                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
				                 
							
                                    <select name="filter_project_id"  class="form-control" onchange="this.form.submit()" >
                                
									      <option value="" <?php if(!isset($project_id) ) { echo 'SELECTED';}?> >Filter by Projects</option>
                                           @foreach($projects as $ct)
                                             <option value="{{ $ct->id }}" <?php if(isset($project_id) && $project_id == $ct->id ) { echo 'SELECTED';}?> >{{ $ct->project_title }}</option>
								           @endforeach
                                   </select>
								  
							</div>	   
						   
						    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
								  
                                    <select name="filter_issue" id="select-project" class="form-control"  >
                                    <option value="">Select Projects</option>
									      <option value="" <?php if(!isset($project_id) ) { echo 'SELECTED';}?> >ALL</option>
                                           @foreach($projects as $ct)
                                             <option value="{{ $ct->id }}" <?php if(isset($project_id) && $project_id == $ct->id ) { echo 'SELECTED';}?> >{{ $ct->project_title }}</option>
								           @endforeach
                                   </select>
							</div>	   
								   
							 <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
								      
                                    <select name="filter_sort" id="select-project" class="form-control"  >
                                    <option value="">Select Projects</option>
									      <option value="" <?php if(!isset($project_id) ) { echo 'SELECTED';}?> >ALL</option>
                                           @foreach($projects as $ct)
                                             <option value="{{ $ct->id }}" <?php if(isset($project_id) && $project_id == $ct->id ) { echo 'SELECTED';}?> >{{ $ct->project_title }}</option>
								           @endforeach
                                   </select>
							</div>
								   	 </div>	
		 
		  </div>
		  
		 {{ Form::close() }}  
		  
 
	
			 
    				   
	
	 
	<!------ filters menu ends----->
	
    <div class="row horizontal-row list-sortable" id="cards_container">


	
	<?php echo json_encode($lists);?>
	
        @foreach($lists as $list)
            <div class="bcategory-list" data-list-id="{{ $list['id'] }}">
                <div class="panel panel-default">
                    <div class="panel-heading" style="border-bottom: 0px; ">
                        <div class="row">
                            <div class="col-lg-10">
                              <!---  <h3 class="panel-title board-panel-title" data-pk="{{ $list['id'] }}">{{ $list['list_name'] }}</h3>-->
							  <h3 class="panel-title" >{{ $list['list_name'] }}</h3>
                            </div>
                            <div class="col-lg-2">
                                <!---<span data-listid="{{ $list['id'] }}" class="glyphicon glyphicon-trash delete-list" aria-hidden="true" style="cursor: pointer; top: 3px;"></span>--->
                            </div>
                        </div>
                    </div>
					 
                    <div class="panel-body card-list-con frame" >
                        <ul class="list-group">
                            <div class="card-con" data-listid="{{ $list->id }}">
							
							 
                                @foreach($cards as $card)
		 
                                    @if($card['list_id'] === $list['id']) 
									
                                        <li class="list-group-item board-list-items" id="card_{{ $card['id'] }}"   data-cardid ="{{ $card['id'] }}" data-toggle="modal" href="#card-detail" @if(!empty($card["card_color"])) style="border-top: 5px solid #{{ $card['card_color'] }};" @endif>
											<?php $project_title= \App\Project::where('id',$card['project_id'])->first(["project_title"])->project_title; echo $project_title;?>
                                            <div class="row">
                                                <div class="col-lg-12">
											 
                                                    <p style="margin-bottom: 0px;" class="pull-left">{{ $card['card_title'] }}</p>    
                                                    <ul class="card-description-intro list-inline pull-right">
													  
                                                        @if($card["card_description"] !== '')
                                                            <li id="card_description">
                                                                <a href="#" data-placement="bottom" data-toggle="tooltip" title="This card has a description." ><span class="glyphicon glyphicon-align-left" aria-hidden="true"></span></a>
                                                            </li>
                                                        @endif
                                                        @foreach($cardTaskCount as $x) 
                                                            @if($x["id"] === $card['id'])
                                                                @if($x['totalTasks'] > 0)
                                                                    <li id="totalTasks" >
                                                                        <a  href="#" data-placement="bottom" data-toggle="tooltip" title="This card have {{ $x['totalTasks'] }} tasks." data-totaltask="{{ $x['totalTasks'] }}"><span class="glyphicon glyphicon-check" aria-hidden="true"></span></a>
                                                                    </li>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                        @if($card['totalComments'] > 0) 
                                                            <li id="totalComments">
                                                                <a href="#" data-placement="bottom" data-toggle="tooltip" title="This card have {{ $card['totalComments'] }} comments." data-totalComments="{{ $card['totalComments'] }}"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span></a>{{ $card['totalComments'] }}
                                                            </li>
                                                        @endif
														<?php $createdAt = \Carbon\Carbon::parse($card['due_date']); echo $createdAt->format("j M");?>
														 
                                                    </ul>
                                                </div>
                                            </div>                                            
                                        </li>
                                    @endif
                                @endforeach
                            </div>
                        </ul>
						
						
						 <input type="hidden" value="" name="abc" id="abc">
				 
                                              
										 

                                          
													
													
													
                        <a href="#" class="show-input-field">Add a card...</a>
                        <form action="/postCard" method="POST" role="form" style="display: none;">
						 
						           <input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
                            <div class="form-group" id="dynamic-board-input-con" style="margin-bottom: 8px;">
                                <textarea name="card-title" class="form-control" rows="3"></textarea>
								
			 
                                    <select name="project_id" class="form-control">
                                    <option value="">Select Projects</option>
                                           @foreach($projects as $ct)
                                             <option value="{{ $ct->id }}">{{ $ct->project_title }}</option>
                                           @endforeach
                                   </select>
								   
								   
								      <select   class="form-control" id="example-getting-started" name="employees_array[]" multiple="multiple" style="width:100%">
    
	<?php if(isset($employees_array) && sizeof($employees_array)>0) {?>
      @foreach($employees_array as $ct)
	  
	  	<?php   $count1 = \App\ProjectEmployees::where('project_id',@$id)->where('employee_id',@$ct->id)->get();
		if(isset($id) && $count1->count() > 0 ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
         <option value="{{ $ct->id }}" <?php echo $s1;?> >{{ $ct->first_name." ".$ct->last_name  }}</option>
     @endforeach
	<?php } ?>
</select>


								   
                                <input type="hidden" name="list_id" value="{{ $list->id }}">
                                
                            </div>
                            <div class="form-group" style="margin-bottom: 0px;">
                                <button type="submit" class="btn btn-primary" id="saveCard">Save It here</button> <span class="glyphicon glyphicon-remove close-input-field" aria-hidden="true"></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
		
		
    </div>
</div>

 
<textarea id="res" name="res"></textarea>
 <script>
 function insert_quick_filters(v)
 {
	 document.getElementById("quick_filters").value=v;
 }
 </script>
 
 
<script type="text/javascript">
   $(document).ready(function() {
       $('#example-getting-started').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
	   $('#example-getting-started2').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
 });
</script>
 
@endsection