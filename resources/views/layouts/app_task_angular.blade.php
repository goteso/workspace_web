<html>
   
   <head>
   
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>@yield('title')</title>
	        <!-- Font Awesome -->
	   <link href='http://fonts.googleapis.com/css?family=RobotoDraft:300,400,400italic,500,700' rel='stylesheet' type='text/css'>
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700' rel='stylesheet' type='text/css'>
      <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	  <link rel="stylesheet" href="{{ URL::asset('admin/css/bootstrap.min.css')}}"> 
       <link href="{{ URL::asset('admin/css/styles.css')}}" type="text/css" rel="stylesheet">
	   <link href="{{ URL::asset('admin/css/main-style.css')}}" type="text/css" rel="stylesheet">
	   <link href="{{ URL::asset('admin/css/custom_task.css')}}" type="text/css" rel="stylesheet">
	   
	   
	   <!--------Assets For Image Uploading------------------->
	   <link href="{{ URL::asset('admin/img-uploads/css/style.css')}}" rel="stylesheet">
       <script src="{{ URL::asset('admin/img-uploads/js/jquery.min.js')}}"></script>
     	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
		
		
		
		<!-------------------- uploadeer assets--------->
		 <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
		 <link href="{{ URL::asset('upload/css/jquery.fileuploader.min.css')}}" media="all" rel="stylesheet">
		 <link href="{{ URL::asset('upload/css/jquery.fileuploader-theme-thumbnails.css')}}" media="all" rel="stylesheet">
	   
		 <script src="{{ URL::asset('upload/js/jquery.fileuploader.min.js')}}" type="text/javascript"></script>
		 <script src="{{ URL::asset('upload/js/custom.js')}}" type="text/javascript"></script>
	<!----------------uploader ends ----------->
	
			
 
 
 
 
	  <style>
	  
	  multiselect {
    display:block;
}
multiselect .btn {
    width: 100%;
    background-color: #FFF;
}
multiselect .btn.error{
    border: 1px solid #da4f49 !important;
}
multiselect .dropdown-menu {
    max-height: 300px;
    overflow-y: auto;
}
multiselect .dropdown-menu {
    width: 100%;
    box-sizing: border-box;
    padding: 2px;
}
multiselect .dropdown-menu > li > a {
    padding: 3px 10px;
    cursor:pointer;
}

</style>
	      
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-editable.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/magic-check.min.css') }}" rel="stylesheet"/>
 
    <link href="{{ asset('css/selectize.css') }}" rel="stylesheet"/>
 
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>
 
    <link href="{{ asset('css/animation.css') }}" rel="stylesheet"/>
 
   
    <script src="{{ URL::asset('admin/js/bootstrap-multiselect.js')}}"   ></script>
	  <link href="{{ URL::asset('admin/css/bootstrap-multiselect.css')}}" type="text/css" rel="stylesheet">

 
	       <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
   </head>
   
   <body onload='hashchange()'; onhashchange="hashchange()">
 
	      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			

	
  
      
 
         @yield('content')
       

	  
	  

 							<!-- Load Bootstrap -->  
      <script src="{{ URL::asset('admin/js/enquire.min.js')}}"></script> 									<!-- Enquire for Responsiveness -->
      <script src="{{ URL::asset('admin/plugins/nanoScroller/js/jquery.nanoscroller.min.js')}}"></script> <!-- nano scroller --> 
      <script src="{{ URL::asset('admin/js/application.js')}}"></script> 
	  
	  
	<script src="{{ asset('js/jquery-ui.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/bootstrap-editable.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
   <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <!--<script src="{{ asset('js/ajax-loading.js') }}"></script>-->
    <script src="{{ asset('js/selectize.js') }}"></script>
    <script src="{{ asset('js/board.js') }}"></script>
 
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    
    <script src="{{ asset('js/typed.min.js') }}"></script>
	  
	   
	   <script>
  $('.toggle-button').click(function(e){
    $('#myDiv').toggleClass('fullscreen');  
});
 </script>
	  <!---------script for checkbox table------------->
	  <script type='text/javascript'> 
        $(window).load(function(){
           $('#selectAll').click(function (e) {
               $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
            });
        }); 
     </script>
   	 
	 <script>
	 $(document).ready(function () {
        $('.checkAll').on('click', function () {
           $(this).closest('table').find('tbody :checkbox')
             .prop('checked', this.checked)
             .closest('tr').toggleClass('selected', this.checked);
        });

        $('tbody :checkbox').on('click', function () {
            $(this).closest('tr').toggleClass('selected', this.checked); //Classe de seleção na row
            $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
        });
      });  
	  </script>
	  
 
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>


<script>
function hashchange(event)
{
	
	
	var hash =window.location.hash;
	 hash = hash.replace("#", "");
	  if(hash!='')
	  {
    document.getElementById('card_detail_'+hash).click();
	  }

 
 
}
hashchange();
</script>
   </body>
</html>