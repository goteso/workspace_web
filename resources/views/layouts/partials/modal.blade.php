
<div class="modal fade" id="see-closed-board">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><span class="fa fa-user" aria-hidden="true"></span> Closed Boards</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default panel-custom">
                    <div class="panel-body">
                        <p>No boards have been closed.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="create-team">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><span class="fa fa-user" aria-hidden="true"></span> Closed Boards</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="" method="POST" role="form">
						 {{ csrf_field() }}
						           <input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name">
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" class="form-control" rows="3" required="required"></textarea>
                            </div>
                            <hr>
                            <div class="form-group">
                                <p>
                                    A teams is a group of boards and people. It helps keep your company, team, or family organized.
                                </p>
                                <p>
                                    <b>Business Class </b>gives your team more security, administrative controls and superpowers.
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-profile-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center">Change profile info</h4>
            </div>
            <div class="modal-body">
                <form action="" method="POST" role="form">
				 {{ csrf_field() }}
						           <input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
                    <div class="form-group">
                        <label for="title">Full Name</label>
                        <input type="text" class="form-control" id="title">
                    </div>
                    <div class="form-group">
                        <label for="title">User Name</label>
                        <input type="text" class="form-control" id="title">
                    </div>
                    <div class="form-group">
                        <label for="title">Initials</label>
                        <input type="text" class="form-control" id="title">
                    </div>
                    <div class="form-group">
                        <label for="title">Bio</label>
                        <textarea name="" id="input" class="form-control" rows="3" required="required"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="save-change">Save changes</button>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="card-detail" style="top:50px;">
 
    <div class="modal-dialog" style="width: 720px;">
        <div class="modal-content">
            <div role="tabpanel">
		
                <div class="modal-header" style="border-bottom: none; padding-bottom: 0px !important;">
					<h3 class="text-center" id="model_card_title"></h3>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#general" aria-controls="tab" role="tab" data-toggle="tab">General</a>
                        </li>
                       <!-- <li role="presentation">
                            <a href="#date" aria-controls="tab" role="tab" data-toggle="tab">Date</a>
                        </li>-->
                        <li role="presentation">
                            <a href="#subtasks" aria-controls="tab" role="tab" data-toggle="tab">Subtasks</a>
                        </li>
						
						 <li role="presentation">
                            <a href="#subCards" aria-controls="tab" role="tab" data-toggle="tab">SubCards</a>
                        </li>
						
							 <li role="presentation">
                            <a href="#attachments" aria-controls="tab" role="tab" data-toggle="tab">Attachments</a>
                        </li>
						
					
                       <!-- <li role="presentation">
					   
					      <input type="file" name="files">
                            <a href="#comments" aria-controls="tab" role="tab" data-toggle="tab">Comments</a>
                        </li>-->
                    </ul>
                </div>
                <div class="modal-body" style="padding-top: 10px; padding-left: 35px; padding-right: 35px;">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="general">
						 <form action="" method="POST" role="form">
						<div class="row">
						<div class="col-sm-8">
                           
							 {{ csrf_field() }}
						           <input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <a href="#" data-type="text" class="input-editable editable-click" title="Enter Card Title" id="card_title_editable">Empty</a>
                                </div>           
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <a href="#" data-type="textarea" class="input-editable editable-click" title="Enter Card desc" id="card_description_editable">Empty</a>
                                </div>
                                
                              
								
								
								
							<?php $projects = \App\Project::get();?>	
								
								
								
								
					 
								   
					

<div class="form-group" id="sele">
<label for="">Assigned Employees</label>					
<select   class="form-control" id="assigned_users" name="employees_array[]" multiple="multiple" style="width:100%!important"> 
    
	<?php if(isset($employees_array) && sizeof($employees_array)>0) {?>
      @foreach($employees_array as $ct)
	  
	  	<?php   $count1 = \App\ProjectEmployees::where('project_id',@$id)->where('employee_id',@$ct->id)->get();
		if(isset($id) && $count1->count() > 0 ) { $s1 = 'SELECTED';} else { $s1 = '';}
		?>
<option value="{{ $ct->id }}" <?php echo $s1;?> >{{ $ct->first_name." ".$ct->last_name  }}</option>
     @endforeach
	<?php } ?>
</select> 

</div>
<!-- <div class="form-group">
                                    <label for="">Labels</label>
                                    <input type="text" id="card-tags-input">
                                </div>-->
 
<label for="">		 Post a Comment: </label>
                                  <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <textarea name="adasd" id="comment-input" class="form-control" rows="3" required="required"></textarea>
                                                </div>  
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <button class="btn btn-default" id="submit-comment">Submit</button>
                                                </div>  
                                            </div>
                                        </div>
										
										 <div class="detailBox">
                                        <div class="actionBox">
                                            <ul class="commentList frame">
                                            </ul>
                                        </div>
                                    </div>   

</div>
                     <div class="col-sm-4">
					   <div class="form-group">
                                    <label for="">Working Hours</label>
                                   <input type="text" class="form-control" name="working_hours" id="working_hours">
                                </div>
					  <div class="form-group"> <label for="">Due Date:</label> 
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                                            <input type='text' class="form-control" data-format="dd-MM-yyyy" name="due_date" id='due-date' aria-describedby="basic-addon1"/>
                                        </div>
										</div>
										
                        <div class="form-group">
                                    <label for="">Priority</label>
                                    <select id="card_color">
                                        <option value="">Choose Priority...</option>
										<option value="3">High</option>
										<option value="2">Medium</option>
                                        <option value="1">Low</option>
                                        <!--<option value="F2D600">Yellow</option>-->
                                        
                                        
                                        <!--<option value="C377E0">Purple</option>-->
                                        <!---<option value="0079BF">Blue</option>-->
                                    </select>
                                </div>
								
								
								<div class="form-group" >
<label for="">Issue Type</label>					
<select   class="form-control" id="card_issue_type_id" name="card_issue_type_id"  style="width:100%"> 
  <option value="">Select Issue Type</option>
	<?php if(isset($card_issue_types) && sizeof($card_issue_types)>0) {?>
      @foreach($card_issue_types as $ct)
	  
	   
<option value="{{ $ct->id }}"  >{{ $ct->title  }}</option>
     @endforeach
	<?php } ?>
</select> 

</div>
							</div>
</div>
                            </form>
                       <!-- </div>
                        <div role="tabpanel" class="tab-pane" id="date">
                            <h1>Add due date and Time</h1>
                            <hr>
                            <form action="" method="POST" role="form" style="height: 65px;">
							 {{ csrf_field() }}
						           <input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
                              <div class="row">
                                    <div class="col-lg-6">
                                        <h1 class="label" style="color: #333333; padding-left: 0px; font-size: 16px;">Created at: </h1>
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1"><span class="fa fa-calendar" aria-hidden="true"></span></span>
                                            <input type='text' class="form-control" id='created-at' aria-describedby="basic-addon1" disabled />
                                        </div>
                                    </div> 
                                    <div class="col-lg-6">
                                        
                                    </div>
                                </div>
                            </form>--->
                        </div>
                        <div role="tabpanel" class="tab-pane" id="subtasks">
                            <div class="addSubTaskCon">
                                <h2>Add subtask</h2>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="task-description-input" required="required">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" id="submit-task">Add</button>
                                    </span>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 19px; margin-bottom: 19px;">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped per-tasks-completed" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                          <span class="show"></span>
                                        </div>
                                      </div>
                                </div>
                            </div>
                            <div class="task-list-con frame" style="margin-top: 12px; max-height: 235px; overflow: scroll;"></div>
                        </div>
						
						
						
						<div role="tabpanel" class="tab-pane" id="subCards">
                          
                                <h2>Add SubCards</h2>
                             
                        
                         
						 
						  <input type="hidden" value="" name="abc" id="abc">
<a href="#" class="show-input-field-subcard">Add a card...</a>
<form action="/postCard" method="POST" role="form" style="display: none;">
<input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
<div class="form-group" id="dynamic-board-input-con" style="margin-bottom: 8px;">
<textarea name="card-title" class="form-control" rows="3" placeholder="Card Title Here"></textarea>



<input type="hidden" name="parent_card_id" id="parent_card_id" >
 

<input type="hidden" name="project_id" id="project_id_subcards">
								   
<input type="hidden" name="list_id" id="list_id_subcards" >
</div>
<div class="form-group" style="margin-bottom: 0px;">
<button type="submit" class="btn btn-primary" id="saveCardSubCard">Save</button> <span class="fa fa-remove close-input-field-subcard" aria-hidden="true"></span>
</div>
</form>


                            <div class="task-list-con-subcards frame" id="task-list-con-subcards" style="margin-top: 12px; max-height: 235px; overflow: scroll;"></div>
                        </div>
						
						
						
						
						<div role="tabpanel" class="tab-pane" id="attachments">
                             
							 
<textarea id="stored_images" >[{"id":22,"card_id":"225","name":"1491976695951.jpg","file":"http://192.168.0.106/workspace_web/public/upload/uploads/1491976695951.jpg","type":"image/jpeg","created_at":"2018-03-16 13:38:45","updated_at":"2018-03-16 13:38:45","size":"1273"},{"id":23,"card_id":"225","name":"1491976695951.jpg","file":"http://192.168.0.106/workspace_web/public/upload/uploads/1491976695951.jpg","type":"image/jpeg","created_at":"2018-03-16 13:38:45","updated_at":"2018-03-16 13:38:45","size":"1273"}]</textarea>


                                <h2>Add Attachments</h2>
                                 
                            <div class="row" style="margin-top: 19px; margin-bottom: 19px;">
                                <div class="col-lg-8 col-lg-offset-2" id="ffff">
									 
                                    <input type="file" name="files" id="nnn">
								 
                                </div>
                            </div> 
                        </div>
						
						
                     <!--   <div role="tabpanel" class="tab-pane" id="comments">
                            <div class="row" style="margin-top: 13px;">
                                <div class="col-lg-12">
                                    <h1 style="font-family: monospace; font-size: 23px; font-weight: 700; margin: 0;">Post a Comment: </h1>
                                    <hr style="margin-top: 5px;">
                                    <form  method="POST" role="form" role="form">
									 {{ csrf_field() }}
						           <input type="hidden"   value="{{ Session::token() }}" name="_token" id="token" >
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <div class="form-group">
                                                    <textarea name="adasd" id="comment-input" class="form-control" rows="3" required="required"></textarea>
                                                </div>  
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <button class="btn btn-default" id="submit-comment">Submit</button>
                                                </div>  
                                            </div>
                                        </div>
                                    </form>
                                    <div class="detailBox">
                                        <div class="actionBox">
                                            <ul class="commentList frame">
                                            </ul>
                                        </div>
                                    </div>       
                                </div>
                            </div>
                        </div> --->
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal-footer">                    
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save-change">Save Changes</button>
                    <button type="button" class="btn btn-danger" id="delete-card"><span class="fa fa-trash" aria-hidden="true"></span> Delete</button>
                </div>            
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
   $(document).ready(function() {
       $('#assigned_users').multiselect({enableFiltering: true,enableFullValueFiltering:true,enableCaseInsensitiveFiltering:true,selectAllNumber:true,
	   numberDisplayed: 100,filterBehavior: 'text'});
 
 });
</script>
<script>
  $(function () {
                $('#due-date').datetimepicker({
				 format:'DD MMM YYYY ',
				});
            });
</script>