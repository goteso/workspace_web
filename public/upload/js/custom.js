$(document).ready(function() {
	
	
		 var base_path = 'http://192.168.0.106/workspace_web/public/upload/';
		 
		 
	/*	var saveEditedImage = function(image, item) {
		// set new image
		item.editor._blob = image;
		
		// if still uploading
		// pend and exit
		if (item.upload && item.upload.status == 'loading')
			return item.editor.isUploadPending = true;
		
		// if not uploaded
		if (item.upload && item.upload.send && !item.upload.status) {
			item.editor._namee = item.name;
			return item.upload.send();
		}

		// if not appended or not uploaded
		if (!item.appended && !item.uploaded)
			return;

		// if no editor
		if (!item.editor || !item.reader.width)
			return;
		
		// if uploaded
		// resend upload
		if (item.upload && item.upload.resend) {
			item.editor._namee = item.name;
			item.editor._editingg = true;
			item.upload.resend();
		}
		
		// if appended
		// send request
		if (item.appended) {
			// hide current thumbnail (this is only animation)
			item.imageIsUploading = true;
			item.editor._editingg = true;
			
			var form = new FormData();
			form.append('files[]', item.editor._blob);
			form.append('fileuploader', 1);
			form.append('_namee', item.name);
			form.append('_editingg', true);
			
			$.ajax({
				url: 'php/ajax_upload_file.php',
				data: form,
		  		type: 'POST',
				processData: false,
		  		contentType: false
			});
		}
	};*/
	
 
	var stored_images  = document.getElementById('stored_images').value;
 
 
	
	// enable fileuploader plugin
	$('input[name="files"]').fileuploader({
        extensions: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
		changeInput: ' ',
		theme: 'thumbnails',
        enableApi: true,
		addMore: true,
		 files: JSON.parse(stored_images),
		thumbnails: {
			box: '<textarea name="file" id="fileValue"> </textarea>'+
			         ' <div class="fileuploader-items">' +
                      '<ul class="fileuploader-items-list">' +
					      '<li class="fileuploader-thumbnails-input"><div class="fileuploader-thumbnails-input-inner">+</div></li>' +
						  '<span class="clear">clear</span>'+
                      '</ul>' +
                  '</div>',
			item: '<li class="fileuploader-item">' +
				       '<div class="fileuploader-item-inner">' +
                           '<div class="thumbnail-holder">${image}</div>' +
                           '<div class="actions-holder">' +
						   	   '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
							   '<span class="fileuploader-action-popup"></span>' +
                           '</div>' +
                       	   '<div class="progress-holder">${progressBar}</div>' +
                       '</div>' +
                   '</li>',
			item2: '<li class="fileuploader-item">' +
				       '<div class="fileuploader-item-inner">' +
                           '<div class="thumbnail-holder">${image}</div>' +
						   '<a href="${file}" target="_blank">' +
                                '<div class="column-title">' +
                                    '<div title="${name}">${name}</div>' +
                                    '<span>${size2}</span>' +
                                '</div>' +
                            '</a>' +
                           '<div class="actions-holder">' + 
                                '<a href="${file}" class="fileuploader-action fileuploader-action-download" title="${captions.download}" download><i></i></a>' +
                               '<a class="fileuploader-action fileuploader-action-sort" title="${captions.sort}"><i></i></a>' +
                               '<a class="fileuploader-action fileuploader-action-remove" title="${captions.remove}"><i class="remove"></i></a>' +
							   '<span class="fileuploader-action-popup"></span>' +
                           '</div>' +
                       '</div>' +
                   '</li>',
			startImageRenderer: false,
			canvasImage: false,
			_selectors: {
				list: '.fileuploader-items-list',
				item: '.fileuploader-item',
				start: '.fileuploader-action-start',
				retry: '.fileuploader-action-retry',
				remove: '.fileuploader-action-remove',
                sorter: '.fileuploader-action-sort',
				download: '.fileuploader-action-download'
			},
			
			popup: {
				onShow: function(item) {
                    item.popup.html.on('click', '[data-action="crop"]', function(e) {
						if (item.editor)
                        	item.editor.cropper();
                    }).on('click', '[data-action="rotate-cw"]', function(e) {
						if (item.editor)
                        	item.editor.rotate();
                    }).on('click', '[data-action="remove"]', function(e) {
                        item.popup.close();
                        item.remove();
                    }).on('click', '[data-action="cancel"]', function(e) {
                        item.popup.close();
                    }).on('click', '[data-action="save"]', function(e) {
						if (item.editor)
                        	item.editor.save(function(blob, item) {
								saveEditedImage(blob, item);
							}, true, null, false);
						
						if (item.popup.close)
							item.popup.close();
                    });
                },	
			},
			 
			onImageLoaded: function(item) {
				if (item.appended)
					return;
				
				// hide current thumbnail (this is only animation)
				if (item.imageIsUploading) {
					item.image.addClass('fileuploader-loading').html('');
				}
				
				if (!item.imageLoaded)
					item.editor.save(function(blob, item) {
						saveEditedImage(blob, item);
					}, true, null, true);
				
				item.imageLoaded = true;
			},
			onItemShow: function(item, listEl) {
				var plusInput = listEl.find('.fileuploader-thumbnails-input');
				
				plusInput.insertAfter(item.html);
				
				if(item.format == 'image') {
					item.html.find('.fileuploader-item-icon').hide();
				}
			}
		},
		
		afterRender: function(listEl, parentEl, newInputEl, inputEl) {
			var plusInput = listEl.find('.fileuploader-thumbnails-input'),
				api = $.fileuploader.getInstance(inputEl.get(0));
		
			plusInput.on('click', function() {
				api.open();
			});
		},
		upload: {
	 
			
			                            url: base_path+'php/ajax_upload_file.php',
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            start: true,
            synchron: true,
            //beforeSend: null,
			beforeSend: function(item, listEl, parentEl, newInputEl, inputEl) {
				// add image to formData
				if (item.editor && item.editor._blob) {
					item.upload.data.fileuploader = 1;
					item.upload.formData.delete(inputEl.attr('name'));
					item.upload.formData.append(inputEl.attr('name'), item.editor._blob);
					
					// add name to data
					if (item.editor._namee) {
						item.upload.data._namee = item.name;
						delete item.editor._namee;
					}

					// add is after editing to data
					if (item.editor._editingg) {
						item.upload.data._editingg = true;
					}
				}
			},
            onSuccess: function(data, item , listEl, parentEl, newInputEl, inputEl) {
				
		var filename = 'uploads/'+item.name; 
			 
				/* var data = {};
				
				try {
					data = JSON.parse(result);
				} catch (e) {
					data.hasWarnings = true;
				}
                
				// if success
                if (data.isSuccess && data.files[0]) {
                    item.name = data.files[0].name;
					item.html.find('.column-title > div:first-child').text(data.files[0].name).attr('title', data.files[0].name);
					
					// send pending editor
					if (item.editor && item.editor.isUploadPending) {
						delete item.editor.isUploadPending;
						
						saveEditedImage(item.editor._blob, item);
					}
                }
				
				// if warnings
				if (data.hasWarnings) {
					for (var warning in data.warnings) {
						alert(data.warnings);
					}
					
					item.html.removeClass('upload-successful').addClass('upload-failed');
					// go out from success function by calling onError function
					// in this case we have a animation there
					// you can also response in PHP with 404
					return this.onError ? this.onError(item) : null;
				}
                
                item.html.find('.column-actions').append('<a class="fileuploader-action fileuploader-action-download" title="download"><i></i></a><a class="fileuploader-action fileuploader-action-sort" title="Sort"><i></i></a><a class="fileuploader-action fileuploader-action-remove fileuploader-action-success" title="Remove"><i></i></a>');
                setTimeout(function() {
					item.renderThumbnail();
                    item.html.find('.progress-holder').fadeOut(400, function() {
						$(this).find('.fileuploader-progressbar .bar').width(0 + "%");
					});
                }, 400);*/
				 setTimeout(function() {
                      item.html.find('.actions-holder').append('<a href="'+base_path+'/'+filename+'" download class="fileuploader-action fileuploader-action-download" title="download"><i></i></a>');
					 item.html.find('.actions-holder').append('<a class="fileuploader-action fileuploader-action-sort" title="Sort"><i></i></a>');
					item.html.find('.progress-holder').hide();
					item.renderThumbnail();
				}, 400); 
				
				
				
				var clearInput = listEl.find('.clear')
				var plusInput1 = listEl.find('.fileuploader-thumbnails-input'),
				api = $.fileuploader.getInstance(inputEl.get(0));
				// api11 = $.fileuploader.getInstance(file_input_element);
		//alert(JSON.stringify(api1));
		
		
		//plusInput1.on('click', function() { 
		 api1 = api.getUploadedFiles(data);
		var api2 = api.getFiles();
		//var api2 = (JSON.stringify(api2));
		//alert( api1[0][name]);
		alert( api2.length);
		alert(typeof(api2));
		
		var myarray = [];
		$.each(api2, function(){
           alert("name"+this.name);
		    myarray.push( this.name);
			
		   alert(myarray);
		   $('#fileValue').val(myarray); 
       // });
		
				
		});
		
		
			
			
		 clearInput.on('click', function() {
				api.reset();
				$('#fileValue').val("");
			}); 	 
			 
			 
			 
            },
            onError: function(item) {
				item.html.find('.progress-holder').hide();
				item.html.find('.fileuploader-item-icon i').text('Failed!');
            },
            onProgress: function(data, item) {
                var progressBar = item.html.find('.progress-holder');
				
                if(progressBar.length > 0) {
                    progressBar.show();
                    progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                }
            }
        },
		dragDrop: {
			container: '.fileuploader-thumbnails-input'
		},
		onRemove: function(item) {
			$.post('php/ajax_remove_file.php', {
				file: item.name
			});
		},
		editor: {
			cropper: {
			showGrid: true
			},
			maxWidth: 800,
			maxHeight: 600,
			quality: 98
		},
        sorter: {
			selectorExclude: null,
			placeholder: '<li class="fileuploader-item fileuploader-sorter-placeholder"><div class="fileuploader-item-inner"></div></li>',
			scrollContainer: window,
			onSort: function(list, listEl, parentEl, newInputEl, inputEl) {
                var api = $.fileuploader.getInstance(inputEl.get(0)),
                    fileList = api.getFileList(),
                    _list = [];
                
                $.each(fileList, function(i, item) {
                    _list.push({
                        name: item.name,
                        index: item.index
                    });
					
					 $('#fileValue').val(_list); 
                });
                
				
				var myarray = [];
		        $.each(fileList, function(){ 
		              myarray.push( this.name); 
		             $('#fileValue').val(myarray); 
       	        });
		
		
                $.post('php/ajax_sort_files.php', {
                    _list: JSON.stringify(_list)
                });
			}
		},
		
		 captions: {
            button: function(options) { return 'Choose ' + (options.limit == 1 ? 'File' : 'Files'); },
            feedback: function(options) { return 'Choose ' + (options.limit == 1 ? 'file' : 'files') + ' to upload'; },
            feedback2: function(options) { return options.length + ' ' + (options.length > 1 ? ' files were' : ' file was') + ' chosen'; },
		 },
	});
	
});